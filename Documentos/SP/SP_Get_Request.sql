USE [SeccionProjecto]
GO
/****** Object:  StoredProcedure [dbo].[SP_Get_Request]    Script Date: 01/07/2020 07:01:54 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rodolfo Cruz
-- Create date: 03-05-2020
-- Description:	Devuelve un listado de todas las solicitudes
-- =============================================
ALTER PROCEDURE [dbo].[SP_Get_Request]
	@DisplayLength INT,
	@DisplayStart INT,
	@SortCol INT,
	@SortDir NVARCHAR(10),
	@Search NVARCHAR(255) = NULL
AS BEGIN

	DECLARE @FirtsRecord INT, @LastRecord INT, @TotalRequest INT;
	
	SET @FirtsRecord = @DisplayStart;
	SET @LastRecord = @DisplayStart + @DisplayLength;

	SET @TotalRequest = (Select COUNT(*) FROM TbRequest r
		INNER JOIN TbEmployee e ON r.TbEmployeeId = e.TbEmployeeId);
	SELECT 
		ROW_NUMBER() OVER (ORDER BY 
								CASE WHEN (@SortCol = 1 AND @SortDir = 'asc') THEN r.RequestNumber END ASC,
								CASE WHEN (@SortCol = 1 AND @SortDir = 'desc') THEN r.RequestNumber END DESC,
								CASE WHEN (@SortCol = 2 AND @SortDir = 'asc') THEN r.DateCreateRequest END ASC,
								CASE WHEN (@SortCol = 2 AND @SortDir = 'desc') THEN r.DateCreateRequest END DESC,
								CASE WHEN (@SortCol = 3 AND @SortDir = 'asc') THEN CONCAT(e.[Name],' ',e.FirstName,' ', e.LastName) END ASC,
								CASE WHEN (@SortCol = 3 AND @SortDir = 'desc') THEN CONCAT(e.[Name],' ',e.FirstName,' ', e.LastName) END DESC) AS RowNum,
		COUNT(*) OVER() AS TotalFilter,
		@TotalRequest AS TotalRequest,
		r.TbRequestId
		,e.TbEmployeeId
		,r.RequestNumber
		,r.DateCreateRequest
		,CONCAT(e.[Name],' ',e.FirstName,' ', e.LastName) AS Solicitante
		,r.Amount
		,r.PaymentsPeriod
		,r.DiscountRate
		,r.AdministrationExpenses
		,r.AdministrationExpensesResult
		,r.AdditionalMonths
		,r.AbonoCatorcenalMensual
		,r.TotalPay
		,r.AvalName
		,r.DebitAval 
	FROM TbRequest r
	INNER JOIN TbEmployee e ON r.TbEmployeeId = e.TbEmployeeId
	WHERE (@Search IS NULL 
			OR RequestNumber LIKE '%' + @Search + '%'  
			OR E.[Name] like '%' + @Search + '%')
	ORDER BY 
	CASE WHEN (@SortCol = 1 AND @SortDir = 'asc') THEN r.RequestNumber END ASC,
	CASE WHEN (@SortCol = 1 AND @SortDir = 'desc') THEN r.RequestNumber END DESC,
	CASE WHEN (@SortCol = 2 AND @SortDir = 'asc') THEN r.DateCreateRequest END ASC,
	CASE WHEN (@SortCol = 2 AND @SortDir = 'desc') THEN r.DateCreateRequest END DESC,
	CASE WHEN (@SortCol = 3 AND @SortDir = 'asc') THEN CONCAT(e.[Name],' ',e.FirstName,' ', e.LastName) END ASC,
	CASE WHEN (@SortCol = 3 AND @SortDir = 'desc') THEN CONCAT(e.[Name],' ',e.FirstName,' ', e.LastName) END DESC
	OFFSET @FirtsRecord ROWS FETCH NEXT @LastRecord ROWS ONLY
END
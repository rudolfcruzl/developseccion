USE [SeccionProjecto]
GO
/****** Object:  StoredProcedure [dbo].[spGetEmployees]    Script Date: 01/07/2020 07:01:58 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetEmployees]
	-- Add the parameters for the stored procedure here
	@DisplayLength int,
	@DisplayStart int,
	@SortCol int,
	@SortDir nvarchar(10),
	@Search nvarchar(255) = NULL
AS
BEGIN
	DECLARE @FirtsRecord INT, @LastRecord INT, @TotalRequest INT;

	SET @FirtsRecord = @DisplayStart;
	SET @LastRecord = @DisplayStart + @DisplayLength;

	SET @TotalRequest = (SELECT COUNT(*) FROM TbEmployee);
	
	SELECT
		ROW_NUMBER() OVER(ORDER BY 
							CASE WHEN (@SortCol = 1 AND @SortDir = 'asc') THEN NumberFicha END ASC,
							CASE WHEN (@SortCol = 1 AND @SortDir = 'desc') THEN NumberFicha END DESC,
							CASE WHEN (@SortCol = 2 AND @SortDir = 'asc') THEN [Name] END ASC,
							CASE WHEN (@SortCol = 2 AND @SortDir = 'desc') THEN [Name] END DESC,
							CASE WHEN (@SortCol = 3 AND @SortDir = 'asc') THEN FirstName END ASC,
							CASE WHEN (@SortCol = 3 AND @SortDir = 'desc') THEN FirstName END DESC,
							CASE WHEN (@SortCol = 4 AND @SortDir = 'asc') THEN LastName END ASC,
							CASE WHEN (@SortCol = 4 AND @SortDir = 'desc') THEN LastName END DESC,
							CASE WHEN (@SortCol = 5 AND @SortDir = 'asc') THEN Rol END ASC,
							CASE WHEN (@SortCol = 5 AND @SortDir = 'desc') THEN Rol END DESC,
							CASE WHEN (@SortCol = 7 AND @SortDir = 'asc') THEN Rol END ASC,
							CASE WHEN (@SortCol = 7 AND @SortDir = 'desc') THEN Rol END DESC,
							CASE WHEN (@SortCol = 8 AND @SortDir = 'asc') THEN Region END ASC,
							CASE WHEN (@SortCol = 8 AND @SortDir = 'desc') THEN Region END DESC,
							CASE WHEN (@SortCol = 9 AND @SortDir = 'asc') THEN [Level] END ASC,
							CASE WHEN (@SortCol = 9 AND @SortDir = 'desc') THEN [Level] END DESC) AS RowNum,
		COUNT(*) OVER() AS TotalFilter,
		@TotalRequest AS TotalFilter,
		TbEmployeeId, 
		NumberFicha, 
		[Name], 
		FirstName, 
		LastName, 
		ContractualSituation, 
		WorkLocation, 
		Rol, 
		Region, 
		[Level], 
		DepartmentKey
	FROM TbEmployee 
	WHERE (@Search IS NULL
			OR NumberFicha LIKE '%' + @Search + '%'
			OR [Name] LIKE '%' + @Search + '%'
			OR FirstName LIKE '%' + @Search + '%'
			OR LastName LIKE '%' + @Search + '%'
			OR Rol LIKE '%' + @Search + '%'
			OR Region LIKE '%' + @Search + '%'
			OR [Level] LIKE '%' + @Search + '%')
	ORDER BY 
	CASE WHEN (@SortCol = 1 AND @SortDir = 'asc') THEN NumberFicha END ASC,
	CASE WHEN (@SortCol = 1 AND @SortDir = 'desc') THEN NumberFicha END DESC,
	CASE WHEN (@SortCol = 2 AND @SortDir = 'asc') THEN [Name] END ASC,
	CASE WHEN (@SortCol = 2 AND @SortDir = 'desc') THEN [Name] END DESC,
	CASE WHEN (@SortCol = 3 AND @SortDir = 'asc') THEN FirstName END ASC,
	CASE WHEN (@SortCol = 3 AND @SortDir = 'desc') THEN FirstName END DESC,
	CASE WHEN (@SortCol = 4 AND @SortDir = 'asc') THEN LastName END ASC,
	CASE WHEN (@SortCol = 4 AND @SortDir = 'desc') THEN LastName END DESC,
	CASE WHEN (@SortCol = 5 AND @SortDir = 'asc') THEN Rol END ASC,
	CASE WHEN (@SortCol = 5 AND @SortDir = 'desc') THEN Rol END DESC,
	CASE WHEN (@SortCol = 7 AND @SortDir = 'asc') THEN Rol END ASC,
	CASE WHEN (@SortCol = 7 AND @SortDir = 'desc') THEN Rol END DESC,
	CASE WHEN (@SortCol = 8 AND @SortDir = 'asc') THEN Region END ASC,
	CASE WHEN (@SortCol = 8 AND @SortDir = 'desc') THEN Region END DESC,
	CASE WHEN (@SortCol = 9 AND @SortDir = 'asc') THEN [Level] END ASC,
	CASE WHEN (@SortCol = 9 AND @SortDir = 'desc') THEN [Level] END DESC
	OFFSET @FirtsRecord ROWS FETCH NEXT @LastRecord ROWS ONLY
END

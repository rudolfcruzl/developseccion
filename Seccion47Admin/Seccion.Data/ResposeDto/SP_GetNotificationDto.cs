﻿using Seccion.Model.Enums;
using System;

namespace Seccion.Data.ResposeDto
{
    public class SP_GetNotificationDto
    {
        public Int64 RowNum { get; set; }
        public int TotalFilter { get; set; }
        public int TotalRequest { get; set; }
        public int TbNotificationId { get; set; }
        public string Title { get; set; }
        public string Status { get; set; }
        public string Email { get; set; }
        public DateTime DateInsert { get; set; }
    }
}

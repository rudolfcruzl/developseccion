﻿namespace Seccion.Data.ResposeDto
{
    public class SP_GetMenuByUserIdDto
    {
        public int MenuItemId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int? ParentId { get; set; }
        public string Icon { get; set; }
        public string Url { get; set; }
        public string RolesId { get; set; }
        public string Permission { get; set; }

    }
}

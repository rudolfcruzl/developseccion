﻿using System;

namespace Seccion.Data.ResposeDto
{
    public class SP_GetInfoBoxDto
    {
        public Int64 RowNum { get; set; }
        public int TotalFilter { get; set; }
        public int TotalData { get; set; }
        public int TbBoxesId { get; set; }
        public string NumberFicha { get; set; }
        public string UserName { get; set; }
        public decimal Quantity { get; set; }
        public decimal TotalQuantity { get; set; }
        public decimal Discount { get; set; }
        public decimal NewDiscount { get; set; }
        public decimal Rate { get; set; }
        public decimal GA { get; set; }
        public int? Period { get; set; }
        public int Month { get; set; }
        public int Fourteen { get; set; }
        public string TypeLoan { get; set; }
    }
}

﻿

using Microsoft.AspNet.Identity.EntityFramework;
using Seccion.Data.Configuration;
using Seccion.Model.IdentityModel;
using Seccion.Model.Models;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using System.Threading.Tasks;
using System.Web.ModelBinding;
using SqlProviderServices = System.Data.Entity.SqlServer.SqlProviderServices;

namespace Seccion.Data
{
    [DbConfigurationType(typeof(DbConfiguration))]
    public sealed class StoreEntities : IdentityDbContext<ApplicationUser, ApplicationRole, int, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>
    {
        public StoreEntities(string connectionString)
            : base(connectionString)
        {

        }

        public StoreEntities()
            : base("StoreEntities")
        {
        }
        public DbSet<TbEmployee> Employee { get; set; }
        public DbSet<TbRequest> Request { get; set; }
        public DbSet<TbStates> States { get; set; }
        public DbSet<TbMunicipalities> Municipalities { get; set; }

        public DbSet<MenuItem> MenuItems { get; set; }
        public DbSet<Permission> Permission { get; set; }
        public DbSet<MenuPermission> MenuPermission { get; set; }

        public DbSet<TbNotification> Notifications { get; set; }
        public DbSet<TbCard> Cards { get; set; }
        public DbSet<TbFile> Files { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null)
            {
                throw new ArgumentNullException("modelBuilder error");
            }

            // Needed to ensure subclasses share the same table
            var user = modelBuilder.Entity<ApplicationUser>().ToTable("AspNetUsers");
            user.HasMany(u => u.Roles).WithRequired().HasForeignKey(ur => ur.UserId);
            user.HasMany(u => u.Claims).WithRequired().HasForeignKey(uc => uc.UserId);
            user.HasMany(u => u.Logins).WithRequired().HasForeignKey(ul => ul.UserId);
            user.Property(u => u.UserName).IsRequired().HasMaxLength(100).HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("UserNameIndex") { IsUnique = true }));

            //// CONSIDER: u.Email is Required if set on options?
            user.Property(u => u.Email).HasMaxLength(256);

            modelBuilder.Entity<ApplicationUserRole>().ToTable("AspNetUserRoles").HasKey(r => new { r.UserId, r.RoleId });

            modelBuilder.Entity<ApplicationUserLogin>().ToTable("AspNetUserLogins").HasKey(l => new { l.LoginProvider, l.ProviderKey, l.UserId });

            var role = modelBuilder.Entity<ApplicationRole>().ToTable("AspNetRoles");
            role.Property(r => r.Name).IsRequired().HasMaxLength(100).HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("RoleNameIndex") { IsUnique = true }));
            role.HasMany(r => r.Users).WithRequired().HasForeignKey(ur => ur.RoleId);

            ///* Role-Menu Definitions */
            modelBuilder.Configurations.Add(new AspNetMenuConfiguration());

            modelBuilder.Entity<ApplicationRoleMenu>().ToTable("AspNetRoleMenu");
            modelBuilder.Entity<ApplicationRoleMenu>().HasMany(e => e.Permissions).WithRequired(e => e.RoleMenu).HasForeignKey(e => e.RoleMenuId).WillCascadeOnDelete(false);

            modelBuilder.Configurations.Add(new TbEmployeeConfiguration());
            modelBuilder.Configurations.Add(new TbStatesConfiguration());
            modelBuilder.Configurations.Add(new TbMunicipalitiesConfiguration());
            modelBuilder.Configurations.Add(new PermissionConfiguration());
            modelBuilder.Configurations.Add(new MenuPermissionConfiguration());
            modelBuilder.Configurations.Add(new TbRequestConfiguration());
            modelBuilder.Configurations.Add(new TbNotificationConfiguration());
            modelBuilder.Configurations.Add(new TbCardConfiguration());
            modelBuilder.Configurations.Add(new TbFileConfiguration());
        }
        public async Task Commit()
        {
            await SaveChangesAsync();
        }
        public static StoreEntities Create()
        {
            return new StoreEntities();
        }
    }
}

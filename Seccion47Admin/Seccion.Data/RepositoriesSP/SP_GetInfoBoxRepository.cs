﻿using Seccion.Data.Infrastructure;
using Seccion.Data.ResposeDto;

namespace Seccion.Data.RepositoriesSP
{
    public interface ISP_GetInfoBoxRepository: IRepository<SP_GetInfoBoxDto>
    {

    }

    public class SP_GetInfoBoxRepository : RepositoryBase<SP_GetInfoBoxDto>, ISP_GetInfoBoxRepository
    {
        public SP_GetInfoBoxRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}

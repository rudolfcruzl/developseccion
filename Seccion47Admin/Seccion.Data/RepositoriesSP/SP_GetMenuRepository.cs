﻿using Seccion.Data.Infrastructure;
using Seccion.Data.ResposeDto;

namespace Seccion.Data.RepositoriesSP
{
    public interface ISP_GetMenuRepository : IRepository<SP_GetMenuByUserIdDto>
    {

    }

    public class SP_GetMenuRepository : RepositoryBase<SP_GetMenuByUserIdDto>, ISP_GetMenuRepository
    {
        public SP_GetMenuRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}

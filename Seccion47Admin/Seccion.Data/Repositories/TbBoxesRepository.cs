﻿using Seccion.Data.Infrastructure;
using Seccion.Data.ResposeDto;
using Seccion.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Data.Repositories
{ 
    public interface ITbBoxesRepository : IRepository<TbBoxes>
    {

    }

    public class TbBoxesRepository : RepositoryBase<TbBoxes>, ITbBoxesRepository
    {

        public TbBoxesRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}

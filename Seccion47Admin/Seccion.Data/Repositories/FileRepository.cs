﻿using Seccion.Data.Infrastructure;
using Seccion.Data.InterfazRepositories;
using Seccion.Model.Models;

namespace Seccion.Data.Repositories
{
    public class FileRepository : RepositoryBase<TbFile>, IFileRepository
    {
        public FileRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}

﻿using Seccion.Data.Infrastructure;
using Seccion.Data.Enum;
using Seccion.Model.IdentityModel;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using System.Security.Claims;
using System.Collections.Generic;
using System;
using System.Linq;
using Microsoft.AspNet.Identity;
using Seccion.Data.InterfazRepositories;

namespace Seccion.Data.Repositories
{
	public class AccountUserRepository : RepositoryBase<ApplicationUser>, IAccountUserRepository
	{
		public AccountUserRepository(IDbFactory dbFactory) : base(dbFactory)
		{

		}

		public ApplicationUserManager UserManager { get; set; } = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
		public ApplicationSignInManager SignInManager { get; set; } = HttpContext.Current.GetOwinContext().Get<ApplicationSignInManager>();
		public ApplicationRoleManager AppRoleManager { get; set; } = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationRoleManager>();


		public async Task<IdentityResult> AccountUserCreate(ApplicationUser user, string pass)
		{
			var result = await UserManager.CreateAsync(user, pass);

			return result;
		}
		public async Task<ApplicationUser> GetUserByIdForLogin(string userNameOrEmail)
        {
			return await DbContext.Users.FirstOrDefaultAsync(x => x.UserName == userNameOrEmail || x.Email == userNameOrEmail);
		}
		public async Task<ApplicationUser> GetUserByIdAsync(int id)
		{
			DbContext.Configuration.LazyLoadingEnabled = false;
			DbContext.Configuration.ProxyCreationEnabled = false;
			return await DbContext.Users.Include("Roles.Role").FirstOrDefaultAsync(x => x.Id == id);
		}

		public ApplicationUser GetUserById(int id)
		{
			DbContext.Configuration.LazyLoadingEnabled = false;
			DbContext.Configuration.ProxyCreationEnabled = false;
			return DbContext.Users.Include("Roles.Role").FirstOrDefault(x => x.Id == id);
		}


		public async Task< IEnumerable<ApplicationUser>> GetUserList()
		{
			return await UserManager.Users.Where(x => x.Active).ToListAsync();
		}
		public async Task<SignInStatusUser> SignInUser(string userName, string password)
		{
			var result = await SignInManager.PasswordSignInAsync(userName, password, isPersistent: false, shouldLockout: false);
			return result == SignInStatus.Success ? SignInStatusUser.Success : result == SignInStatus.LockedOut ? SignInStatusUser.LockedOut : result == SignInStatus.RequiresVerification ? SignInStatusUser.RequiresVerification : SignInStatusUser.Failure;
		}
		public async Task<ClaimsIdentity> CreateUserIdentity(ApplicationUser appUser)
		{
			return await SignInManager.CreateUserIdentityAsync(appUser);
		}
		public async Task<IdentityResult> ChangePasswordUser(int userId, string currentPassword, string newPassword)
		{
			var result = await UserManager.ChangePasswordAsync(userId, currentPassword, newPassword);

			return result;
		}
		/// <summary>
		/// Obtiene
		/// </summary>
		/// <returns>Lisatdo de roles</returns>
		public List<Tuple<int, string>> GetRoles()
		{

			var listRoles = AppRoleManager.Roles.ToList().Select(x => new Tuple<int, string>(x.Id, x.Name)).ToList();
			return listRoles;
		}
		public async Task<IdentityResult> AddRole(int userId, string role)
		{
			return await UserManager.AddToRoleAsync(userId, role);
		}
		public async Task<string> GetRoleById(int roleId)
		{
			var result = await AppRoleManager.FindByIdAsync(roleId);
			return result.Name;
		}
		public async Task<IList<string>> GetRoleByUserId(int userId)
		{
			//var result = await AppRoleManager.Roles.Where(e => e.Users.Any(y => y.UserId == userId)).ToListAsync();
			var result = await UserManager.GetRolesAsync(userId);
			return result;
		}
		public async Task<IdentityResult> DeleteRoleByUserId(int userId, string role)
		{
			var result = await UserManager.RemoveFromRoleAsync(userId, role);
			return result;
		}
		public async Task<string> GeneratePasswordResetToken(int userId)
		{
			return await UserManager.GeneratePasswordResetTokenAsync(userId);
		}
		public async Task<bool> VerifyUserToken(int userId, string token)
		{
			return await UserManager.VerifyUserTokenAsync(userId, "ResetPassword", token);
		}
		public async Task<IdentityResult> ResetPassword(int userId, string token, string newPassword)
		{
			return await UserManager.ResetPasswordAsync(userId, token, newPassword);
		}
		public async Task<string> EmailConfirmation(int userId)
		{
			return await UserManager.GenerateEmailConfirmationTokenAsync(userId);
		}
		public async Task<IdentityResult> EmailConfirmn(int userId, string token)
		{
			return await UserManager.ConfirmEmailAsync(userId, token);
		}
	}
}

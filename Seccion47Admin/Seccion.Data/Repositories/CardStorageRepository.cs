﻿using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Blob;
using Seccion.Data.InterfazRepositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Seccion.Data.Repositories
{
    public class CardStorageRepository : ICardStorageRepository
    {
        private readonly string _containerName = "card";
        private readonly string _containerNameVideosArchive = "coffeevideos-archive";
        private readonly string _connectionString;
        private readonly string _metadataKeyTitle = "title";
        private readonly string _metadataKeyDescription = "description";

        public CardStorageRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        /// Metodo que carga de manera asincrona un archivo al blob storage de azure
        /// </summary>
        /// <param name="videoByteArray">byte[] file</param>
        /// <param name="blobName">Nombre del archivo</param>
        /// <param name="title">Titulo</param>
        /// <param name="description">Descripcion</param>
        /// <returns></returns>
        public async Task<CloudBlockBlob> UploadFileAsync(byte[] videoByteArray, string blobName, string title, string description, string contentType)
        {
            var cloudBlobContainer = await GetCardContainerAsync();

            var cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(blobName);

            cloudBlockBlob.Properties.ContentType = contentType;

            SetMetadata(cloudBlockBlob, _metadataKeyTitle, title);
            SetMetadata(cloudBlockBlob, _metadataKeyDescription, description);

            await cloudBlockBlob.UploadFromByteArrayAsync(videoByteArray, 0, videoByteArray.Length);

            return cloudBlockBlob;
        }

        /// <summary>
        /// Verifica si el contendor existe
        /// </summary>
        /// <param name="blobName">Nombre del contenedor</param>
        /// <returns>true or false</returns>
        public async Task<bool> CheckIfBlobExistsAsync(string blobName)
        {
            var cloudBlobContainer = await GetCardContainerAsync();

            var cloudBlockBlob = cloudBlobContainer.GetBlobReference(blobName);

            return await cloudBlockBlob.ExistsAsync();
        }

        public async Task<IEnumerable<CloudBlockBlob>> ListFileBlobsAsync(string prefix = null, bool includeSnapshots = false)
        {
            var cloudBlockBlobs = new List<CloudBlockBlob>();
            var cloudBlobContainer = await GetCardContainerAsync();

            BlobContinuationToken token = null;
            do
            {
                var blobListingDetails = BlobListingDetails.Metadata;

                if (includeSnapshots)
                {
                    blobListingDetails |= BlobListingDetails.Snapshots;
                }

                var blobResultSegment =
                    await cloudBlobContainer.ListBlobsSegmentedAsync(prefix, true,
                        blobListingDetails, null, token, null, null);
                token = blobResultSegment.ContinuationToken;
                cloudBlockBlobs.AddRange(blobResultSegment.Results.OfType<CloudBlockBlob>());
            }
            while (token != null);

            return cloudBlockBlobs;
        }

        public async Task DownloadFileAsync(MemoryStream targetStream, string blobName)
        {
            var cloudBlobContainer = await GetCardContainerAsync();

            var cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(blobName);

            await cloudBlockBlob.DownloadToStreamAsync(targetStream);
        }

        public async Task OverwriteFileAsync(CloudBlockBlob cloudBlockBlob, byte[] videoByteArray, string leaseId)
        {
            var accessCondition = new AccessCondition
            {
                IfMatchETag = cloudBlockBlob.Properties.ETag,
                LeaseId = leaseId
            };

            await cloudBlockBlob.UploadFromByteArrayAsync(videoByteArray, 0, videoByteArray.Length,
                accessCondition, null, null);
        }

        public async Task DeleteFileAsync(CloudBlockBlob cloudBlockBlob, string leaseId)
        {
            var accessCondition = new AccessCondition
            {
                IfMatchETag = cloudBlockBlob.Properties.ETag,
                LeaseId = leaseId
            };

            var deleteSnapshotsOption = cloudBlockBlob.IsSnapshot
                ? DeleteSnapshotsOption.None
                : DeleteSnapshotsOption.IncludeSnapshots;

            await cloudBlockBlob.DeleteAsync(deleteSnapshotsOption, accessCondition,
                null, null);
        }

        public async Task UpdateMetadataAsync(CloudBlockBlob cloudBlockBlob, string title, string description, string leaseId)
        {
            SetMetadata(cloudBlockBlob, _metadataKeyTitle, title);
            SetMetadata(cloudBlockBlob, _metadataKeyDescription, description);

            var accessCondition = new AccessCondition
            {
                IfMatchETag = cloudBlockBlob.Properties.ETag,
                LeaseId = leaseId
            };

            await cloudBlockBlob.SetMetadataAsync(accessCondition, null, null);
        }

        public async Task ReloadMetadataAsync(CloudBlockBlob cloudBlockBlob)
        {
            await cloudBlockBlob.FetchAttributesAsync();
        }

        public (string title, string description) GetBlobMetadata(CloudBlockBlob cloudBlockBlob)
        {
            return (cloudBlockBlob.Metadata.ContainsKey(_metadataKeyTitle)
                    ? cloudBlockBlob.Metadata[_metadataKeyTitle]
                    : ""
                , cloudBlockBlob.Metadata.ContainsKey(_metadataKeyDescription)
                    ? cloudBlockBlob.Metadata[_metadataKeyDescription]
                    : "");
        }

        public string GetBlobUriWithSasToken(CloudBlockBlob cloudBlockBlob)
        {
            var sharedAccessBlobPolicy = new SharedAccessBlobPolicy
            {
                Permissions = SharedAccessBlobPermissions.Read,
                SharedAccessExpiryTime = DateTime.Now.AddDays(1)
            };

            var sasToken = cloudBlockBlob.GetSharedAccessSignature(sharedAccessBlobPolicy);

            if (cloudBlockBlob.IsSnapshot)
            {
                sasToken = sasToken.Replace('?', '&');
            }

            return cloudBlockBlob.SnapshotQualifiedUri + sasToken;
        }

        public async Task<string> AcquireOneMinuteLeaseAsync(CloudBlockBlob cloudBlockBlob)
        {
            var accessCondition = new AccessCondition
            {
                IfMatchETag = cloudBlockBlob.Properties.ETag
            };

            return await cloudBlockBlob.AcquireLeaseAsync(TimeSpan.FromMinutes(1), null,
                accessCondition, null, null);
        }

        public async Task RenewLeaseAsync(CloudBlockBlob cloudBlockBlob, string leaseId)
        {
            var accessCondition = new AccessCondition
            {
                LeaseId = leaseId
            };

            await cloudBlockBlob.RenewLeaseAsync(accessCondition);
        }

        public async Task ReleaseLeaseAsync(CloudBlockBlob cloudBlockBlob, string leaseId)
        {
            var accessCondition = new AccessCondition
            {
                LeaseId = leaseId
            };

            await cloudBlockBlob.ReleaseLeaseAsync(accessCondition);
        }

        public async Task<string> LoadLeaseInfoAsync(CloudBlockBlob cloudBlockBlob)
        {
            await cloudBlockBlob.FetchAttributesAsync();

            return $"Lease state: {cloudBlockBlob.Properties.LeaseState}\n" +
                   $"Lease status: {cloudBlockBlob.Properties.LeaseStatus}\n" +
                   $"Lease duration: {cloudBlockBlob.Properties.LeaseDuration}";
        }

        public async Task CreateSnapshotAsync(CloudBlockBlob cloudBlockBlob)
        {
            await cloudBlockBlob.CreateSnapshotAsync();
        }

        public async Task PromoteSnapshotAsync(CloudBlockBlob snapshotCloudBlockBlob)
        {
            if (!snapshotCloudBlockBlob.IsSnapshot)
            {
                throw new ArgumentException("CloudBlockBlob must be a snapshot",
                    nameof(snapshotCloudBlockBlob));
            }

            var cloudBlobContainer = await GetCardContainerAsync();

            var baseCloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(
                snapshotCloudBlockBlob.Name);

            await baseCloudBlockBlob.StartCopyAsync(snapshotCloudBlockBlob);

            await WaitForCopyToCompleteAsync(baseCloudBlockBlob);
        }

        public async Task ArchiveVideoAsync(CloudBlockBlob cloudBlockBlob)
        {
            var archiveCloubBlobContainer = await GetCardContainerAsync();

            var archiveCloudBlockBlob = archiveCloubBlobContainer.GetBlockBlobReference(
                $"{DateTime.Now:yyyy/MM/dd}/{cloudBlockBlob.Name}");

            await archiveCloudBlockBlob.StartCopyAsync(cloudBlockBlob);

            await WaitForCopyToCompleteAsync(archiveCloudBlockBlob);

            await archiveCloudBlockBlob.SetStandardBlobTierAsync(StandardBlobTier.Cool);
        }

        /// <summary>
        /// Metodo que manda a llamar el contenedor
        /// </summary>
        /// <returns></returns>
        private async Task<CloudBlobContainer> GetCardContainerAsync()
        {
            return await GetContainerAsync(_containerName);
        }

        /// <summary>
        /// Obtiene el contenedor, si no existe lo crea.
        /// </summary>
        /// <param name="containerName">Nombre del contenedor</param>
        /// <returns>Blob Container</returns>
        private async Task<CloudBlobContainer> GetContainerAsync(string containerName)
        {
            var cloudStorageAccount = CloudStorageAccount.Parse(_connectionString);

            var cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();

            var cloudBlobContainer = cloudBlobClient.GetContainerReference(containerName);
            await cloudBlobContainer.CreateIfNotExistsAsync();
            return cloudBlobContainer;
        }

        /// <summary>
        /// Establece los metada de un archivo
        /// </summary>
        /// <param name="cloudBlockBlob">Contenedor</param>
        /// <param name="key">Identificador</param>
        /// <param name="value">Valor</param>
        private static void SetMetadata(CloudBlockBlob cloudBlockBlob, string key, string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                if (cloudBlockBlob.Metadata.ContainsKey(key))
                {
                    cloudBlockBlob.Metadata.Remove(key);
                }
            }
            else
            {
                cloudBlockBlob.Metadata[key] = value;
            }
        }

        private static async Task WaitForCopyToCompleteAsync(CloudBlockBlob cloudBlockBlob)
        {
            var copyInProgress = true;

            while (copyInProgress)
            {
                await Task.Delay(500);
                await cloudBlockBlob.FetchAttributesAsync();
                copyInProgress = cloudBlockBlob.CopyState.Status == CopyStatus.Pending;
            }
        }
    }
}

﻿using Seccion.Data.Infrastructure;
using Seccion.Data.InterfazRepositories;
using Seccion.Model.Models;

namespace Seccion.Data.Repositories
{
	public class ExpensesRepository : RepositoryBase<TbFuneralExpenses>, IExpensesRepository
	{
		public ExpensesRepository(IDbFactory dbFactory) : base(dbFactory)
		{
		}
	}
}

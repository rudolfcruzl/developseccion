﻿using Seccion.Data.Infrastructure;
using Seccion.Data.InterfazRepositories;
using Seccion.Model.Models;
using System;
using System.Threading.Tasks;

namespace Seccion.Data.Repositories
{
    public class CardRepository : RepositoryBase<TbCard>, ICardRepository
    {
        public CardRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}

﻿using Seccion.Data.Infrastructure;
using Seccion.Model.IdentityModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Seccion.Data.Repositories
{
    public interface IMenuRepository : IRepository<MenuItem>
    {
        ICollection<MenuItem> GetMenuByUser(int UserId, Func<MenuPermission, bool> filterFunc = null);
    }
    public class MenuRepository : RepositoryBase<MenuItem>, IMenuRepository
    {
        private readonly IMenuPermissiomRepository _IMenuPermissiomRepository;
        public MenuRepository(IDbFactory dbFactory, IMenuPermissiomRepository IMenuPermissiomRepository) : base(dbFactory)
        {
            _IMenuPermissiomRepository = IMenuPermissiomRepository;
        }


        public ICollection<MenuItem> GetMenuByUser(int UserId, Func<MenuPermission, bool> filterFunc = null)
        {
            var lstRoleIds = _IMenuPermissiomRepository.GetMenuPermissionsByUser(UserId);

            var records = filterFunc == null
               ? lstRoleIds.ToList()
               : lstRoleIds.Where(filterFunc).ToList();

            return records
                .GroupBy(menuPermission => menuPermission.RoleMenu.MenuItem)
                .Select(grouping => grouping.Key)
                .ToList();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using Seccion.Data.Infrastructure;
using Seccion.Data.InterfazRepositories;
using Seccion.Data.ResposeDto;
using Seccion.Model.Models;

namespace Seccion.Data.Repositories
{
    public class NotificationRepository : RepositoryBase<TbNotification>, INotificationRepository
    {
        public NotificationRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public async Task<IEnumerable<SP_GetNotificationDto>> GetNotifications(params object[] parameters)
        {
            try
            {
                using (var connection = DbContext.Database.Connection)
                {
                    connection.Open();
                    var command = connection.CreateCommand();
                    command.CommandText = "[dbo].[spGetNotifications]";
                    command.Parameters.AddRange(parameters);
                    command.CommandType = CommandType.StoredProcedure;
                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        var result = ((IObjectContextAdapter) DbContext).ObjectContext
                            .Translate<SP_GetNotificationDto>(reader).ToList();

                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

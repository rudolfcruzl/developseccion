﻿using Seccion.Data.Infrastructure;
using Seccion.Model.IdentityModel;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Seccion.Data.Repositories
{
    public interface IMenuPermissiomRepository : IRepository<MenuPermission>
    {
        ICollection<MenuPermission> GetMenuPermissionsByUser(int UserId);
    }

    public class MenuPermissionRepository : RepositoryBase<MenuPermission>, IMenuPermissiomRepository
    {
        public MenuPermissionRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public ICollection<MenuPermission> GetMenuPermissionsByUser(int UserId)
        {
            var lstRoles = DbContext.Roles
                .Include(role => role.MenuItems.Select(menu => menu.Permissions))
                .Where(role => role.Users.Any(tableUser => tableUser.UserId == UserId))
                .SelectMany(role => role.MenuItems.SelectMany(roleMenu => roleMenu.Permissions))
                .ToList();
            return lstRoles;
        }
    }
}

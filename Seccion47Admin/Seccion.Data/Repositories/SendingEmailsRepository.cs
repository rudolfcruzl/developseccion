﻿using Seccion.Data.Enum;
using Seccion.Data.InterfazRepositories;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Collections.Generic;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Seccion.Data.Repositories
{
	public class SendingEmailsRepository : ISendingEmails
	{
		private readonly SendGridClient sendGridClient;
		private readonly EmailAddress emailAddressFrom;
		private SendGridMessage SendGridMessage;

		//Constructor con la configuración personalizada
		public SendingEmailsRepository()
		{
			emailAddressFrom = new EmailAddress(ConfigurationManager.AppSettings["Sengrid_From"].ToString(), "H. Sección 47");
			sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridAPIKey"].ToString());

		}

		public async Task ForgotPasswordEmail(string ToEmail, string userName, string url, EmailType emailType)
		{

			var to = new EmailAddress(ToEmail, userName);
			//ID del Template en Sengrid
			string templateId = string.Empty;

			//parametros para el Template
			var dinamycTemplateData = new Dictionary<string, object>
			{
				{ "Sender_Name", userName },
                { "Url_UpdatePassword", url }
			};
			templateId = EmailType.ForgotPassword == emailType ? 
				ConfigurationManager.AppSettings["ForgotPassword_Id"].ToString(): ConfigurationManager.AppSettings["ConfirmEmail_Id"].ToString();
			switch (emailType)
			{
				case EmailType.AccoutConfirmEmail:
				case EmailType.ForgotPassword:
					
					SendGridMessage = MailHelper.CreateSingleTemplateEmail(emailAddressFrom, to, templateId, dinamycTemplateData);
					break;
				default:
					break;
			}
			var response = await sendGridClient.SendEmailAsync(SendGridMessage);

		}
		private bool validarEmail(string email)
		{
			string expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";

			if (Regex.IsMatch(email, expresion))
			{
				if (Regex.Replace(email, expresion, string.Empty).Length == 0)
				{ return true; }
				else
				{ return false; }
			}
			else
			{ return false; }
		}
	}
}

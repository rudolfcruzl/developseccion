﻿using System.Security.Claims;
using System.Security.Principal;

namespace Seccion.Data.ExtensionsIIdentity
{
	public static class Extensions
	{
		public static string GetName(this IIdentity identity)
		{
			var claim = ((ClaimsIdentity)identity).FindFirst("Nombre");
			return (claim != null) ? claim.Value : string.Empty;
		}
		public static string GetEmail(this IIdentity identity)
		{
			var claim = ((ClaimsIdentity)identity).FindFirst("Email");
			return (claim != null) ? claim.Value : string.Empty;
		}

		public static string Roles(this IIdentity identity)
        {
			var claim = ((ClaimsIdentity)identity).FindFirst("Roles");
			return (claim != null) ? claim.Value : string.Empty;
		}
	}
}

﻿namespace Seccion.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateAspNetUsers : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "UpdatePasswordDate", c => c.DateTime());
            AddColumn("dbo.AspNetUsers", "UpdateDate", c => c.DateTime());
            AddColumn("dbo.AspNetUsers", "UserInsert", c => c.String());
            AddColumn("dbo.AspNetUsers", "UserUpdate", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "UserUpdate");
            DropColumn("dbo.AspNetUsers", "UserInsert");
            DropColumn("dbo.AspNetUsers", "UpdateDate");
            DropColumn("dbo.AspNetUsers", "UpdatePasswordDate");
        }
    }
}

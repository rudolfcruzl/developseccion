﻿namespace Seccion.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateTableTbBoxes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TbBoxes",
                c => new
                    {
                        TbBoxesId = c.Int(nullable: false, identity: true),
                        TbEmployeeId = c.Int(nullable: false),
                        Folio547 = c.String(nullable: true),
                        FolioNP = c.String(nullable: true),
                        Quantity = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Date = c.DateTime(nullable: false),
                        Period = c.Int(nullable: false),
                        Rate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        GA = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Month = c.Int(nullable: false),
                        Fourteen = c.Int(nullable: false),
                        AdditionalMonths = c.Int(nullable: true),
                        TotalQuantity = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Discount = c.Decimal(precision: 18, scale: 2, nullable: true),
                        Balance = c.Decimal(precision: 18, scale: 2, nullable: true),
                        CurrentBalance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        FourteenRemaining = c.DateTime(nullable: false),
                        LiquidTwoCAT = c.Boolean(nullable: false),
                        TypeLoan = c.String(),
                        CreditInsurance = c.Decimal(precision: 18, scale: 2, nullable: true),
                        DateInsert = c.DateTime(),
                        DateUpdate = c.DateTime(),
                        UserInsert = c.String(),
                        UserUpdate = c.String(),
                    })
                .PrimaryKey(t => t.TbBoxesId)
                .ForeignKey("dbo.TbEmployee", t => t.TbEmployeeId, cascadeDelete: true)
                .Index(t => t.TbEmployeeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TbBoxes", "TbEmployeeId", "dbo.TbEmployee");
            DropIndex("dbo.TbBoxes", new[] { "TbEmployeeId" });
            DropTable("dbo.TbBoxes");
        }
    }
}

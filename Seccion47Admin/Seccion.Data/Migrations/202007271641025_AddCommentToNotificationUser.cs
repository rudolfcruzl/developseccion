﻿namespace Seccion.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCommentToNotificationUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TbCards", "Description", c => c.String());
            AddColumn("dbo.TbNotifications", "Comment", c => c.String());
            DropColumn("dbo.TbCards", "Comment");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TbCards", "Comment", c => c.String());
            DropColumn("dbo.TbNotifications", "Comment");
            DropColumn("dbo.TbCards", "Description");
        }
    }
}

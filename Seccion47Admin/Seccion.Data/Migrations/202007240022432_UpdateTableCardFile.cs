﻿namespace Seccion.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class UpdateTableCardFile : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.TbFileCards", newName: "TbFileCards");
            DropForeignKey("dbo.TbNotifications", "TbCardId", "dbo.TbCards");
            DropForeignKey("dbo.TbNotifications", "UserId", "dbo.AspNetUsers");
            RenameColumn(table: "dbo.TbFileCards", name: "TbFile_TbFileId", newName: "TbCardId");
            RenameColumn(table: "dbo.TbFileCards", name: "TbCard_TbCardId", newName: "TbFileId");
            RenameIndex(table: "dbo.TbFileCards", name: "IX_TbCard_TbCardId", newName: "IX_TbFileId");
            RenameIndex(table: "dbo.TbFileCards", name: "IX_TbFile_TbFileId", newName: "IX_TbCardId");
            DropPrimaryKey("dbo.TbFileCards");
            AlterColumn("dbo.TbCards", "Title", c => c.String(nullable: false, maxLength: 80));
            AlterColumn("dbo.TbCards", "UserInsert", c => c.String(maxLength: 50));
            AlterColumn("dbo.TbCards", "UserUpdate", c => c.String(maxLength: 50));
            AlterColumn("dbo.TbFiles", "FileName", c => c.String(nullable: false));
            AlterColumn("dbo.TbFiles", "UserInsert", c => c.String(maxLength: 50));
            AlterColumn("dbo.TbFiles", "UserUpdate", c => c.String(maxLength: 50));
            AlterColumn("dbo.TbNotifications", "UserInsert", c => c.String(maxLength: 50));
            AlterColumn("dbo.TbNotifications", "UserUpdate", c => c.String(maxLength: 50));
            AddPrimaryKey("dbo.TbFileCards", new[] { "TbFileId", "TbCardId" });
            AddForeignKey("dbo.TbNotifications", "TbCardId", "dbo.TbCards", "TbCardId");
            AddForeignKey("dbo.TbNotifications", "UserId", "dbo.AspNetUsers", "Id");
        }

        public override void Down()
        {
            DropForeignKey("dbo.TbNotifications", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.TbNotifications", "TbCardId", "dbo.TbCards");
            DropPrimaryKey("dbo.TbFileCards");
            AlterColumn("dbo.TbNotifications", "UserUpdate", c => c.String());
            AlterColumn("dbo.TbNotifications", "UserInsert", c => c.String());
            AlterColumn("dbo.TbFiles", "UserUpdate", c => c.String());
            AlterColumn("dbo.TbFiles", "UserInsert", c => c.String());
            AlterColumn("dbo.TbFiles", "FileName", c => c.String());
            AlterColumn("dbo.TbCards", "UserUpdate", c => c.String());
            AlterColumn("dbo.TbCards", "UserInsert", c => c.String());
            AlterColumn("dbo.TbCards", "Title", c => c.String());
            AddPrimaryKey("dbo.TbFileCards", new[] { "TbFile_TbFileId", "TbCard_TbCardId" });
            RenameIndex(table: "dbo.TbFileCards", name: "IX_TbCardId", newName: "IX_TbFile_TbFileId");
            RenameIndex(table: "dbo.TbFileCards", name: "IX_TbFileId", newName: "IX_TbCard_TbCardId");
            RenameColumn(table: "dbo.TbFileCards", name: "TbFileId", newName: "TbCard_TbCardId");
            RenameColumn(table: "dbo.TbFileCards", name: "TbCardId", newName: "TbFile_TbFileId");
            AddForeignKey("dbo.TbNotifications", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.TbNotifications", "TbCardId", "dbo.TbCards", "TbCardId", cascadeDelete: true);
            RenameTable(name: "dbo.TbFileCards", newName: "TbFileTbCards");
        }
    }
}

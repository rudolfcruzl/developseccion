﻿// <auto-generated />
namespace Seccion.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.0")]
    public sealed partial class AddColumIntoTbEmployee : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddColumIntoTbEmployee));
        
        string IMigrationMetadata.Id
        {
            get { return "202007080011181_AddColumIntoTbEmployee"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

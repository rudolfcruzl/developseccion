﻿namespace Seccion.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class contentNameToTableFile : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TbFiles", "ContainerName", c => c.String());
        }

        public override void Down()
        {
            DropColumn("dbo.TbFiles", "ContainerName");
        }
    }
}

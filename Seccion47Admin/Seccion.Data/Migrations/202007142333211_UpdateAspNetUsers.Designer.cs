﻿// <auto-generated />
namespace Seccion.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.0")]
    public sealed partial class UpdateAspNetUsers : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(UpdateAspNetUsers));
        
        string IMigrationMetadata.Id
        {
            get { return "202007142333211_UpdateAspNetUsers"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

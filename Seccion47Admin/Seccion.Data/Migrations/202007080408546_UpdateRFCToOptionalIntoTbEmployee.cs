﻿namespace Seccion.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateRFCToOptionalIntoTbEmployee : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TbEmployee", "RFC", c => c.String(nullable: true, maxLength: 20));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TbEmployee", "RFC", c => c.String(nullable: false, maxLength: 20));
        }
    }
}

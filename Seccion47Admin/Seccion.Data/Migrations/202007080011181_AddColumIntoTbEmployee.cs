﻿namespace Seccion.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColumIntoTbEmployee : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TbEmployee", "CURP", c => c.String(nullable:true));
            AddColumn("dbo.TbEmployee", "INE", c => c.String(nullable:true));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TbEmployee", "INE");
            DropColumn("dbo.TbEmployee", "CURP");
        }
    }
}

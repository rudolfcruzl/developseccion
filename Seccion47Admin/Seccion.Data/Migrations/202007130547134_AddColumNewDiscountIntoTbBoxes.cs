﻿namespace Seccion.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColumNewDiscountIntoTbBoxes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TbBoxes", "NewDiscount", c => c.Decimal(precision: 18, scale: 2, nullable:true));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TbBoxes", "NewDiscount");
        }
    }
}

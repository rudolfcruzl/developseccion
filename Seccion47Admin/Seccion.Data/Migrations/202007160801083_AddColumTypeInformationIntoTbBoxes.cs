﻿namespace Seccion.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColumTypeInformationIntoTbBoxes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TbBoxes", "TypeInformation", c => c.Int(nullable: false));
            AlterColumn("dbo.TbBoxes", "FourteenRemaining", c => c.Int(nullable: true));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TbBoxes", "FourteenRemaining", c => c.DateTime());
            DropColumn("dbo.TbBoxes", "TypeInformation");
        }
    }
}

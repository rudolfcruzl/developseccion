﻿namespace Seccion.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class CreateTableNotification : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TbCards",
                c => new
                    {
                        TbCardId = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Comment = c.String(),
                        DateInsert = c.DateTime(),
                        DateUpdate = c.DateTime(),
                        UserInsert = c.String(),
                        UserUpdate = c.String(),
                    })
                .PrimaryKey(t => t.TbCardId);

            CreateTable(
                "dbo.TbFiles",
                c => new
                    {
                        TbFileId = c.Int(nullable: false, identity: true),
                        FileName = c.String(),
                        DateInsert = c.DateTime(),
                        DateUpdate = c.DateTime(),
                        UserInsert = c.String(),
                        UserUpdate = c.String(),
                    })
                .PrimaryKey(t => t.TbFileId);

            CreateTable(
                "dbo.TbNotifications",
                c => new
                    {
                        TbNotificationId = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        TbCardId = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        DateInsert = c.DateTime(),
                        DateUpdate = c.DateTime(),
                        UserInsert = c.String(),
                        UserUpdate = c.String(),
                    })
                .PrimaryKey(t => t.TbNotificationId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.TbCards", t => t.TbCardId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.TbCardId);

            CreateTable(
                "dbo.TbFileCards",
                c => new
                    {
                        TbFile_TbFileId = c.Int(nullable: false),
                        TbCard_TbCardId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.TbFile_TbFileId, t.TbCard_TbCardId })
                .ForeignKey("dbo.TbFiles", t => t.TbFile_TbFileId, cascadeDelete: true)
                .ForeignKey("dbo.TbCards", t => t.TbCard_TbCardId, cascadeDelete: true)
                .Index(t => t.TbFile_TbFileId)
                .Index(t => t.TbCard_TbCardId);

        }

        public override void Down()
        {
            DropForeignKey("dbo.TbNotifications", "TbCardId", "dbo.TbCards");
            DropForeignKey("dbo.TbNotifications", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.TbFileCards", "TbCard_TbCardId", "dbo.TbCards");
            DropForeignKey("dbo.TbFileCards", "TbFile_TbFileId", "dbo.TbFiles");
            DropIndex("dbo.TbFileCards", new[] { "TbCard_TbCardId" });
            DropIndex("dbo.TbFileCards", new[] { "TbFile_TbFileId" });
            DropIndex("dbo.TbNotifications", new[] { "TbCardId" });
            DropIndex("dbo.TbNotifications", new[] { "UserId" });
            DropTable("dbo.TbFileCards");
            DropTable("dbo.TbNotifications");
            DropTable("dbo.TbFiles");
            DropTable("dbo.TbCards");
        }
    }
}

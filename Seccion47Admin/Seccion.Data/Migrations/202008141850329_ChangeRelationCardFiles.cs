﻿namespace Seccion.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeRelationCardFiles : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.TbFileCards", name: "TbFileId", newName: "__mig_tmp__0");
            RenameColumn(table: "dbo.TbFileCards", name: "TbCardId", newName: "TbFileId");
            RenameColumn(table: "dbo.TbFileCards", name: "__mig_tmp__0", newName: "TbCardId");
            RenameIndex(table: "dbo.TbFileCards", name: "IX_TbFileId", newName: "__mig_tmp__0");
            RenameIndex(table: "dbo.TbFileCards", name: "IX_TbCardId", newName: "IX_TbFileId");
            RenameIndex(table: "dbo.TbFileCards", name: "__mig_tmp__0", newName: "IX_TbCardId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.TbFileCards", name: "IX_TbCardId", newName: "__mig_tmp__0");
            RenameIndex(table: "dbo.TbFileCards", name: "IX_TbFileId", newName: "IX_TbCardId");
            RenameIndex(table: "dbo.TbFileCards", name: "__mig_tmp__0", newName: "IX_TbFileId");
            RenameColumn(table: "dbo.TbFileCards", name: "TbCardId", newName: "__mig_tmp__0");
            RenameColumn(table: "dbo.TbFileCards", name: "TbFileId", newName: "TbCardId");
            RenameColumn(table: "dbo.TbFileCards", name: "__mig_tmp__0", newName: "TbFileId");
        }
    }
}

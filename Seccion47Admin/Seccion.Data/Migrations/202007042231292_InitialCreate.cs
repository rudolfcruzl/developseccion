﻿namespace Seccion.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TbEmployee",
                c => new
                    {
                        TbEmployeeId = c.Int(nullable: false, identity: true),
                        TbStatesId = c.Int(nullable: false),
                        TbMunicipalitiesId = c.Int(nullable: false),
                        NumberFicha = c.String(nullable: false, maxLength: 20),
                        Name = c.String(nullable: false, maxLength: 50),
                        FirstName = c.String(nullable: false, maxLength: 50),
                        LastName = c.String(nullable: false, maxLength: 50),
                        RFC = c.String(nullable: false, maxLength: 20),
                        ContractualSituation = c.String(maxLength: 50),
                        WorkLocation = c.String(maxLength: 50),
                        Rol = c.String(maxLength: 50),
                        Region = c.String(maxLength: 50),
                        DepartmentKey = c.String(maxLength: 50),
                        Level = c.Int(nullable: false),
                        AsisteKey = c.String(maxLength: 100),
                        Address = c.String(nullable: false, maxLength: 200),
                        State = c.String(maxLength: 50),
                        City = c.String(maxLength: 50),
                        PhoneHouse = c.String(maxLength: 50),
                        CelularPhone = c.String(maxLength: 50),
                        Email = c.String(maxLength: 50),
                        BirthDate = c.DateTime(),
                        Bank = c.String(maxLength: 50),
                        BranchOffice = c.String(maxLength: 50),
                        AccountNumber = c.String(maxLength: 50),
                        ClaveInterbancaria = c.String(maxLength: 50),
                        Comments = c.String(maxLength: 400),
                        ActiveEmployee = c.Boolean(nullable: false),
                        DateInsert = c.DateTime(),
                        DateUpdate = c.DateTime(),
                        UserInsert = c.String(maxLength: 50),
                        UserUpdate = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.TbEmployeeId)
                .ForeignKey("dbo.TbMunicipalities", t => t.TbMunicipalitiesId)
                .ForeignKey("dbo.TbStates", t => t.TbStatesId)
                .Index(t => t.TbStatesId)
                .Index(t => t.TbMunicipalitiesId)
                .Index(t => t.NumberFicha, unique: true, name: "NumberFichaIndex");
            
            CreateTable(
                "dbo.TbFuneralExpenses",
                c => new
                    {
                        TbFuneralExpensesId = c.Int(nullable: false, identity: true),
                        TbEmployeeId = c.Int(nullable: false),
                        OrderNumberGF = c.String(),
                        DateCreate = c.DateTime(nullable: false),
                        ServiceConcept = c.String(),
                        PaymentType = c.String(),
                        VoucherKey = c.String(),
                        AvalName = c.String(),
                        PaymentAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PeriodsPayment = c.String(),
                        AbonoCatorcenalMensual = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Comments = c.String(),
                        DateInsert = c.DateTime(),
                        DateUpdate = c.DateTime(),
                        UserInsert = c.String(),
                        UserUpdate = c.String(),
                    })
                .PrimaryKey(t => t.TbFuneralExpensesId)
                .ForeignKey("dbo.TbEmployee", t => t.TbEmployeeId, cascadeDelete: true)
                .Index(t => t.TbEmployeeId);
            
            CreateTable(
                "dbo.TbVouchers",
                c => new
                    {
                        TbVoucherId = c.Int(nullable: false, identity: true),
                        TbFuneralExpensesId = c.Int(nullable: false),
                        NameVoucher = c.String(),
                        DateInsert = c.DateTime(),
                        DateUpdate = c.DateTime(),
                        UserInsert = c.String(),
                        UserUpdate = c.String(),
                    })
                .PrimaryKey(t => t.TbVoucherId)
                .ForeignKey("dbo.TbFuneralExpenses", t => t.TbFuneralExpensesId, cascadeDelete: true)
                .Index(t => t.TbFuneralExpensesId);
            
            CreateTable(
                "dbo.TbMunicipalities",
                c => new
                    {
                        TbMunicipalitiesId = c.Int(nullable: false, identity: true),
                        TbStatesId = c.Int(nullable: false),
                        NameMunicipalities = c.String(maxLength: 100),
                        DateInsert = c.DateTime(),
                        DateUpdate = c.DateTime(),
                        UserInsert = c.String(maxLength: 50),
                        UserUpdate = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.TbMunicipalitiesId)
                .ForeignKey("dbo.TbStates", t => t.TbStatesId)
                .Index(t => t.TbStatesId);
            
            CreateTable(
                "dbo.TbStates",
                c => new
                    {
                        TbStatesId = c.Int(nullable: false, identity: true),
                        NameSate = c.String(maxLength: 100),
                        DateInsert = c.DateTime(),
                        DateUpdate = c.DateTime(),
                        UserInsert = c.String(maxLength: 50),
                        UserUpdate = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.TbStatesId);
            
            CreateTable(
                "dbo.TbRequest",
                c => new
                    {
                        TbRequestId = c.Int(nullable: false, identity: true),
                        TbEmployeeId = c.Int(nullable: false),
                        DateCreateRequest = c.DateTime(nullable: false),
                        RequestNumber = c.String(maxLength: 10),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PaymentsPeriod = c.String(maxLength: 20),
                        DiscountRate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AdministrationExpenses = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AdministrationExpensesResult = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AdditionalMonths = c.String(maxLength: 20),
                        TotalPay = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AbonoCatorcenalMensual = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CatorcenaLiquidate = c.String(maxLength: 50),
                        BelongingSection = c.String(maxLength: 50),
                        NextRise = c.DateTime(),
                        AvalName = c.String(maxLength: 100),
                        AvalKey = c.String(maxLength: 20),
                        DebitAval = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Comments = c.String(maxLength: 400),
                        Status = c.Int(nullable: false),
                        DateInsert = c.DateTime(),
                        DateUpdate = c.DateTime(),
                        UserInsert = c.String(maxLength: 50),
                        UserUpdate = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.TbRequestId)
                .ForeignKey("dbo.TbEmployee", t => t.TbEmployeeId)
                .Index(t => t.TbEmployeeId);
            
            CreateTable(
                "dbo.AspNetMenu",
                c => new
                    {
                        MenuItemId = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 50),
                        Description = c.String(maxLength: 100),
                        ParentId = c.Int(),
                        Icon = c.String(maxLength: 50),
                        Url = c.String(maxLength: 500),
                        DateInsert = c.DateTime(),
                        DateUpdate = c.DateTime(),
                        UserInsert = c.String(maxLength: 50),
                        UserUpdate = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.MenuItemId)
                .ForeignKey("dbo.AspNetMenu", t => t.ParentId)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.AspNetRoleMenu",
                c => new
                    {
                        RoleMenuId = c.Int(nullable: false, identity: true),
                        RoleId = c.Int(nullable: false),
                        MenuId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.RoleMenuId)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetMenu", t => t.MenuId)
                .Index(t => t.RoleId)
                .Index(t => t.MenuId);
            
            CreateTable(
                "dbo.MenuPermission",
                c => new
                    {
                        RoleMenuId = c.Int(nullable: false),
                        PermissionId = c.Int(nullable: false),
                        DateInsert = c.DateTime(),
                        DateUpdate = c.DateTime(),
                        UserInsert = c.String(maxLength: 50),
                        UserUpdate = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => new { t.RoleMenuId, t.PermissionId })
                .ForeignKey("dbo.Permission", t => t.PermissionId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoleMenu", t => t.RoleMenuId)
                .Index(t => t.RoleMenuId)
                .Index(t => t.PermissionId);
            
            CreateTable(
                "dbo.Permission",
                c => new
                    {
                        PermissionId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        DateInsert = c.DateTime(),
                        DateUpdate = c.DateTime(),
                        UserInsert = c.String(maxLength: 50),
                        UserUpdate = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.PermissionId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        RoleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        UserPhoto = c.String(),
                        LastLogin = c.DateTime(),
                        Active = c.Boolean(nullable: false),
                        HasChangedPassword = c.Boolean(nullable: false),
                        JoinDate = c.DateTime(),
                        BirthDate = c.DateTime(),
                        Gender = c.String(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.ApplicationUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UsedPasswords",
                c => new
                    {
                        HashPassword = c.String(nullable: false, maxLength: 128),
                        Id = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.HashPassword, t.Id })
                .ForeignKey("dbo.AspNetUsers", t => t.Id, cascadeDelete: true)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetRoleMenu", "MenuId", "dbo.AspNetMenu");
            DropForeignKey("dbo.UsedPasswords", "Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ApplicationUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.AspNetRoleMenu", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.MenuPermission", "RoleMenuId", "dbo.AspNetRoleMenu");
            DropForeignKey("dbo.MenuPermission", "PermissionId", "dbo.Permission");
            DropForeignKey("dbo.AspNetMenu", "ParentId", "dbo.AspNetMenu");
            DropForeignKey("dbo.TbEmployee", "TbStatesId", "dbo.TbStates");
            DropForeignKey("dbo.TbRequest", "TbEmployeeId", "dbo.TbEmployee");
            DropForeignKey("dbo.TbEmployee", "TbMunicipalitiesId", "dbo.TbMunicipalities");
            DropForeignKey("dbo.TbMunicipalities", "TbStatesId", "dbo.TbStates");
            DropForeignKey("dbo.TbVouchers", "TbFuneralExpensesId", "dbo.TbFuneralExpenses");
            DropForeignKey("dbo.TbFuneralExpenses", "TbEmployeeId", "dbo.TbEmployee");
            DropIndex("dbo.UsedPasswords", new[] { "Id" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.ApplicationUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.MenuPermission", new[] { "PermissionId" });
            DropIndex("dbo.MenuPermission", new[] { "RoleMenuId" });
            DropIndex("dbo.AspNetRoleMenu", new[] { "MenuId" });
            DropIndex("dbo.AspNetRoleMenu", new[] { "RoleId" });
            DropIndex("dbo.AspNetMenu", new[] { "ParentId" });
            DropIndex("dbo.TbRequest", new[] { "TbEmployeeId" });
            DropIndex("dbo.TbMunicipalities", new[] { "TbStatesId" });
            DropIndex("dbo.TbVouchers", new[] { "TbFuneralExpensesId" });
            DropIndex("dbo.TbFuneralExpenses", new[] { "TbEmployeeId" });
            DropIndex("dbo.TbEmployee", "NumberFichaIndex");
            DropIndex("dbo.TbEmployee", new[] { "TbMunicipalitiesId" });
            DropIndex("dbo.TbEmployee", new[] { "TbStatesId" });
            DropTable("dbo.UsedPasswords");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.ApplicationUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Permission");
            DropTable("dbo.MenuPermission");
            DropTable("dbo.AspNetRoleMenu");
            DropTable("dbo.AspNetMenu");
            DropTable("dbo.TbRequest");
            DropTable("dbo.TbStates");
            DropTable("dbo.TbMunicipalities");
            DropTable("dbo.TbVouchers");
            DropTable("dbo.TbFuneralExpenses");
            DropTable("dbo.TbEmployee");
        }
    }
}

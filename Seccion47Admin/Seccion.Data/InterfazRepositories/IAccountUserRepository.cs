﻿using Microsoft.AspNet.Identity;
using Seccion.Data.Enum;
using Seccion.Data.Infrastructure;
using Seccion.Model.IdentityModel;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Seccion.Data.InterfazRepositories
{
	/// <summary>
	/// Interfaz que implementa las funciones o metodos para las cuenta de usuario
	/// </summary>
	public interface IAccountUserRepository : IRepository<ApplicationUser>
	{
		Task<IdentityResult> AccountUserCreate(ApplicationUser user, string pass);
		Task<IEnumerable<ApplicationUser>> GetUserList();
		Task<ClaimsIdentity> CreateUserIdentity(ApplicationUser appUser);

		Task<IdentityResult> ChangePasswordUser(int userId, string currentPassword, string newPassword);
		Task<ApplicationUser> GetUserByIdForLogin(string userNameOrEmail);
		Task<ApplicationUser> GetUserByIdAsync(int id);
		ApplicationUser GetUserById(int id);
		Task<SignInStatusUser> SignInUser(string userName, string password);
		/// <summary>
		/// Obtiene
		/// </summary>
		/// <returns>Lisatdo de roles</returns>
		List<Tuple<int, string>> GetRoles();
		Task<IdentityResult> AddRole(int userId, string role);
		Task<string> GetRoleById(int roleId);
		Task<IList<string>> GetRoleByUserId(int userId);
		Task<IdentityResult> DeleteRoleByUserId(int userId, string role);
		/// <summary>
		/// Email Token
		/// </summary>
		/// <param name="userId"></param>
		/// <returns>Retorna un token para le cambio o recuperación de contraseña</returns>
		Task<string> GeneratePasswordResetToken(int userId);
		/// <summary>
		/// Valida en el token para recuperar la contraseña
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="token"></param>
		/// <returns>true o false</returns>
		Task<bool> VerifyUserToken(int userId, string token);
		Task<IdentityResult> ResetPassword(int userId, string token, string newPassword);
		/// <summary>
		/// Confirmar la cuenta de usuario
		/// </summary>
		/// <param name="userId"></param>
		/// <returns>Retorna un token para validar y confirmar la cuenta de usuario</returns>
		Task<string> EmailConfirmation(int userId);
		/// <summary>
		/// Verifica la confirmacion de la cuenta
		/// </summary>
		/// <param name="userId"></param>
		/// <returns>Retonar el resultado de la verificacion de la cuenta</returns>
		Task<IdentityResult> EmailConfirmn(int userId, string token);
	}
}
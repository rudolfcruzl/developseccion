﻿using Seccion.Data.Enum;
using System.Threading.Tasks;

namespace Seccion.Data.InterfazRepositories
{
	public interface ISendingEmails
	{
		Task ForgotPasswordEmail(string ToEmail, string userName, string url, EmailType emailType);
	}
}

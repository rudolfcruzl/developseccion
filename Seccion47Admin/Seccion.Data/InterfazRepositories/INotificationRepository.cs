﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Seccion.Data.Infrastructure;
using Seccion.Data.ResposeDto;
using Seccion.Model.Models;

namespace Seccion.Data.InterfazRepositories
{
    public interface INotificationRepository : IRepository<TbNotification>
    {
        Task<IEnumerable<SP_GetNotificationDto>> GetNotifications(params object[] parameters);
    }
}

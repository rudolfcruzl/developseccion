﻿using Microsoft.Azure.Storage.Blob;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Seccion.Data.InterfazRepositories
{
    public interface ICardStorageRepository
    {
        Task<CloudBlockBlob> UploadFileAsync(byte[] videoByteArray, string blobName,
            string title, string description, string contentType);
        Task<bool> CheckIfBlobExistsAsync(string blobName);

        Task<IEnumerable<CloudBlockBlob>> ListFileBlobsAsync(string prefix = null, bool includeSnapshots = false);

        Task DownloadFileAsync(MemoryStream targetStream, string blobName);
        Task OverwriteFileAsync(CloudBlockBlob cloudBlockBlob, byte[] videoByteArray, string leaseId);
        Task DeleteFileAsync(CloudBlockBlob cloudBlockBlob, string leaseId);

        Task UpdateMetadataAsync(CloudBlockBlob cloudBlockBlob, string title, string description, string leaseId);
        Task ReloadMetadataAsync(CloudBlockBlob cloudBlockBlob);
        (string title, string description) GetBlobMetadata(CloudBlockBlob cloudBlockBlob);

        string GetBlobUriWithSasToken(CloudBlockBlob cloudBlockBlob);

        Task<string> AcquireOneMinuteLeaseAsync(CloudBlockBlob cloudBlockBlob);
        Task RenewLeaseAsync(CloudBlockBlob cloudBlockBlob, string leaseId);
        Task ReleaseLeaseAsync(CloudBlockBlob cloudBlockBlob, string leaseId);

        Task<string> LoadLeaseInfoAsync(CloudBlockBlob cloudBlockBlob);

        Task CreateSnapshotAsync(CloudBlockBlob cloudBlockBlob);
        Task PromoteSnapshotAsync(CloudBlockBlob cloudBlockBlob);

        Task ArchiveVideoAsync(CloudBlockBlob cloudBlockBlob);
    }
}

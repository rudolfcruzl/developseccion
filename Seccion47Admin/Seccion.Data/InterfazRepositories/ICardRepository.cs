﻿using Seccion.Data.Infrastructure;
using Seccion.Model.Models;

namespace Seccion.Data.InterfazRepositories
{
    public interface ICardRepository : IRepository<TbCard>
    {
    }
}

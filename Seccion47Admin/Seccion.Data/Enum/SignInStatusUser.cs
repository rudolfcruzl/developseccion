﻿using Microsoft.AspNet.Identity.Owin;
namespace Seccion.Data.Enum
{
	public enum SignInStatusUser
	{
		//
		// Summary:
		//     Sign in was successful
		Success = SignInStatus.Success,
		//
		// Summary:
		//     User is locked out
		LockedOut = SignInStatus.LockedOut,
		//
		// Summary:
		//     Sign in requires addition verification (i.e. two factor)
		RequiresVerification = SignInStatus.RequiresVerification,
		//
		// Summary:
		//     Sign in failed
		Failure = SignInStatus.Failure
	}
}

﻿using Seccion.Model.Models;

namespace Seccion.Data.Configuration
{
    public class TbNotificationConfiguration : BaseEntityTypeConfiguration<TbNotification>
    {
        public TbNotificationConfiguration()
        {
            ToTable("TbNotifications");
            HasKey(t => t.TbNotificationId);
            HasRequired(x => x.ApplicationUser).WithMany(x => x.Notifications).HasForeignKey(x => x.UserId).WillCascadeOnDelete(false);
            HasRequired(x => x.Card).WithMany(x => x.Notifications).HasForeignKey(x => x.TbCardId).WillCascadeOnDelete(false);
            Property(x => x.Status).IsRequired();
        }
    }
}

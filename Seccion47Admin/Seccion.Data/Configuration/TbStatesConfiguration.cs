﻿using Seccion.Model.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace Seccion.Data.Configuration
{
    public class TbStatesConfiguration : BaseEntityTypeConfiguration<TbStates>
    {
        public TbStatesConfiguration()
        {
            ToTable(nameof(TbStates));
            HasKey(x => x.TbStatesId);
            Property(x => x.TbStatesId).HasDatabaseGeneratedOption(databaseGeneratedOption: DatabaseGeneratedOption.Identity);
            Property(x => x.NameSate).HasMaxLength(100);
            //base.Configure();
        }
    }
}

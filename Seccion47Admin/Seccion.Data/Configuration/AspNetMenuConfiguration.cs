﻿using Seccion.Model.IdentityModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Seccion.Data.Configuration
{
    public class AspNetMenuConfiguration : BaseEntityTypeConfiguration<MenuItem>
    {
        public AspNetMenuConfiguration()
        {
            ToTable("AspNetMenu");
            HasKey(x => x.MenuItemId);
            Property(x => x.MenuItemId).HasDatabaseGeneratedOption(databaseGeneratedOption: DatabaseGeneratedOption.Identity);
            HasMany(e => e.Children).WithOptional(e => e.ParentItem).HasForeignKey(e => e.ParentId);
            HasMany(e => e.Roles).WithRequired(e => e.MenuItem).HasForeignKey(e => e.MenuId).WillCascadeOnDelete(false);

            Property(x => x.Title).HasMaxLength(50).IsRequired();
            Property(x => x.Description).HasMaxLength(100);
            Property(x => x.ParentId).IsOptional();
            Property(x => x.Icon).HasMaxLength(50);
            Property(x => x.Url).HasMaxLength(500);
        }
    }
}


﻿using Seccion.Model.Models;

namespace Seccion.Data.Configuration
{
    public class TbCardConfiguration : BaseEntityTypeConfiguration<TbCard>
    {
        public TbCardConfiguration()
        {
            ToTable("TbCards");
            HasKey(t => t.TbCardId);
            Property(x => x.Title).IsRequired().HasMaxLength(80);
            Property(x => x.Description);
            HasMany(x => x.Files).WithMany(x => x.Cards)
                .Map(cs =>
                    {
                        cs.MapLeftKey("TbCardId");
                        cs.MapRightKey("TbFileId");
                        cs.ToTable("TbFileCards");
                    });
        }
    }
}

﻿using Seccion.Model.Models;

namespace Seccion.Data.Configuration
{
    public class TbFileConfiguration : BaseEntityTypeConfiguration<TbFile>
    {
        public TbFileConfiguration()
        {
            ToTable("TbFiles");
            HasKey(t => t.TbFileId);
            Property(x => x.FileName).IsRequired();
        }
    }
}

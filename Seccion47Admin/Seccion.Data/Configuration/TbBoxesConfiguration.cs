﻿using Microsoft.Owin.Security.DataHandler;
using Seccion.Model.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Policy;
using System.Threading;

namespace Seccion.Data.Configuration
{
    public class TbBoxesConfiguration : BaseEntityTypeConfiguration<TbBoxes>
    {
        public TbBoxesConfiguration()
        {
            ToTable(nameof(TbBoxes));
            HasKey(x => x.TbBoxesId);
            Property(x => x.TbBoxesId).HasDatabaseGeneratedOption(databaseGeneratedOption: DatabaseGeneratedOption.Identity);
            HasRequired(x => x.TbEmployee).WithMany(x => x.TbBoxes).HasForeignKey(x => x.TbEmployeeId).WillCascadeOnDelete(false);
            Property(x => x.Folio547).HasMaxLength(50).IsOptional();
            Property(x => x.FolioNP).HasMaxLength(50).IsOptional();
            Property(x => x.Quantity).HasColumnType("decimal(18,2)").IsRequired();
            Property(x => x.Date).IsRequired();
            Property(x => x.Period).IsRequired();
            Property(x => x.Rate).HasColumnType("decimal(18,2)").IsRequired();
            Property(x => x.GA).HasColumnType("decimal(18,2)").IsRequired();
            Property(x => x.Month).IsRequired();
            Property(x => x.Fourteen).IsRequired();
            Property(x => x.AdditionalMonths).IsOptional();
            Property(x => x.TotalQuantity).HasColumnType("decimal(18,2)").IsRequired();
            Property(x => x.Discount).HasColumnType("decimal(18,2)").IsOptional();
            Property(x => x.NewDiscount).HasColumnType("decimal(18,2)").IsOptional();
            Property(x => x.Balance).HasColumnType("decimal(18,2)").IsOptional();
            Property(x => x.CurrentBalance).HasColumnType("decimal(18,2)").IsRequired();
            Property(x => x.FourteenRemaining).HasColumnType("int").IsOptional();
            Property(x => x.LiquidTwoCAT).IsRequired();
            Property(x => x.TypeLoan).HasMaxLength(50).IsRequired();
            Property(x => x.CreditInsurance).HasColumnType("decimal(18,2)").IsOptional();
            Property(x => x.TypeInformation).IsRequired();
        }
    }
}

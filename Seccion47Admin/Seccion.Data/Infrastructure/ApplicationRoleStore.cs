﻿using Microsoft.AspNet.Identity.EntityFramework;
using Seccion.Model.IdentityModel;
using System.Data.Entity;

namespace Seccion.Data.Infrastructure
{
	public class ApplicationRoleStore : RoleStore<ApplicationRole, int, ApplicationUserRole>
	{
        public ApplicationRoleStore() : base(new IdentityDbContext())
        {
            base.DisposeContext = true;
        }

        public ApplicationRoleStore(DbContext context)
            : base(context)
        {
        }
    }
}

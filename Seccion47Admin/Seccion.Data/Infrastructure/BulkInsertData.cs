﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace Seccion.Data.Infrastructure
{
    public abstract class BulkInsertData
    {
        public static void BulkInsert<T>(List<T> data, string tableName)
        {
            using (var connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["StoreEntities"].ToString()))
            {
                connection.Open();
                using (SqlBulkCopy bulkcopy = new SqlBulkCopy(connection))
                {
                    bulkcopy.DestinationTableName = "dbo." + tableName;
                    bulkcopy.BulkCopyTimeout = 50000;
                    try
                    {
                        if (bulkcopy.DestinationTableName == "dbo.TbVehicularAssignments")
                        {
                            bulkcopy.WriteToServer(AsDataTable02(data));

                        }
                        else
                        {
                            bulkcopy.WriteToServer(AsDataTable(data));
                        }
                    }
                    catch (Exception ex)
                    {
                        
                        connection.Close();
                        throw ex;
                    }
                    finally
                    {
                        connection.Close();
                    }                    
                }
            }
        }

        public static void BulkInsertRealVehicle<T>(List<T> data, string tableName)
        {
            using (var connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["StoreEntities"].ToString()))
            {
                connection.Open();
                using (SqlBulkCopy bulkcopy = new SqlBulkCopy(connection))
                {
                    bulkcopy.DestinationTableName = "dbo." + tableName;
                    bulkcopy.BulkCopyTimeout = 50000;
                    try
                    {
                        //if (bulkcopy.DestinationTableName == "dbo.TbVehicularAssignments")
                        //{
                        //    bulkcopy.WriteToServer(AsDataTable02(data));

                        //}
                        //else
                        //{
                            bulkcopy.WriteToServer(AsDataTableRealVehicle(data));
                        //}
                    }
                    catch (Exception ex)
                    {

                        connection.Close();
                        throw ex;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        public static DataTable AsDataTable<T>(List<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            var table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
            {
                if (prop.Name != "Rol" && prop.Name != "RankGroupID" && prop.Name != "StateToApproveID" && prop.Name != "StateToApprove" && prop.Name != "Placa" && prop.Name != "Renabam" ) 
                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    if (prop.Name != "Rol" && prop.Name != "RankGroupID" && prop.Name != "StateToApproveID" && prop.Name != "StateToApprove" && prop.Name != "Placa" && prop.Name != "Renabam" )
                        if (prop.Name == "Id")
                            row[prop.Name] = prop.GetValue(item).ToString();
                        else
                            row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;

                    //if (prop.PropertyType.Name == "Boolean")
                    //{
                    //row[prop.Name] = prop.GetValue(item).ToString() == "True" ? 1 : 0;
                    //}
                    //else
                    //{
                    //    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                    //}

                }

                table.Rows.Add(row);
            }
            return table;
        }

        public static DataTable AsDataTableRealVehicle<T>(List<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            var table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
            {
                if (prop.Name != "NameUserAnterior" && prop.Name != "NameUserActual" && prop.Name != "PeridoActual" && prop.Name != "Description" && prop.Name != "Completed" && prop.Name != "BeforeStatusID" && prop.Name != "DateChange" && prop.Name != "OrderNumberBefore" && prop.Name != "OrderNumberNow" && prop.Name != "RequesName" && prop.Name != "Concessionaire" && prop.Name != "Patio" )
                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    if (prop.Name != "NameUserAnterior" && prop.Name != "NameUserActual" && prop.Name != "PeridoActual" && prop.Name != "Description" && prop.Name != "Completed" && prop.Name != "BeforeStatusID" && prop.Name != "DateChange" && prop.Name != "OrderNumberBefore" && prop.Name != "OrderNumberNow" && prop.Name != "RequesName" && prop.Name != "Concessionaire" && prop.Name != "Patio" )
                        if (prop.Name == "Id")
                            row[prop.Name] = prop.GetValue(item).ToString();
                        else
                            row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                }

                table.Rows.Add(row);
            }
            return table;
        }

        public static DataTable AsDataTable02<T>(List<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            var table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
            {
                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                        if (prop.Name == "Id")
                            row[prop.Name] = prop.GetValue(item).ToString();
                        else
                            row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                }

                table.Rows.Add(row);
            }
            return table;
        }

    }
}

﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Data.Infrastructure
{
	public class ResultOperation : IdentityResult
	{
		public ResultOperation(params string[] errors) : base(errors)
		{
		}

		public ResultOperation(IEnumerable<string> errors) : base(errors)
		{
		}

		public ResultOperation(bool success) : base(success)
		{
		}
	}
}

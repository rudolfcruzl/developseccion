﻿

using System.Threading.Tasks;

namespace Seccion.Data.Infrastructure
{
    /// <summary>
    /// Intefaz que Adnistra la unidad de trabajo para el SaveChange() de EF
    /// </summary>
    public interface IUnitOfWork
    {

        Task Commit();

    }
}

﻿namespace Seccion.Data.Constants
{
    public static class ConstantsVarible
    {
        public const string UserTypeNissan = "AEFC6506-CD7B-48D8-9206-737BC6E40B54";
        public const string RoleUsuario = "17EDA239-8F80-4B5A-A59F-E2D55A26DFBB";
        public const string UserTypeRenault = "5BCCDE8F-649D-4DA3-93FC-7E2738826D23";
        public const string RequestPoolID = "92E4C63E-81D0-4D6B-838C-55A4ED0AB8CB";
        public const string RequestDeparment = "5A458B8B-D4FE-4F5B-AA62-39651A28628D";
        public const string VoudeNissan = "7036803E-F088-440E-A935-EFD1A94C9FFC";
        public const string TemporaryRequest = "A650176E-F8F0-45D0-8CBB-59122F10B9B3";
        public const string StatusTemporary = "2EB088D6-0357-4438-AAA5-77416C877691";
        public const string StatusBuy = "10F20E85-64C7-4230-A44A-143027045F6E";
    }
}

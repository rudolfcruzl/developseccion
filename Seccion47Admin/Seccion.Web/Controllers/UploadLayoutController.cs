﻿using AutoMapper;
using OfficeOpenXml;
using Seccion.Model.Enums;
using Seccion.Model.Models;
using Seccion.Service.Common.Message;
using Seccion.Service.ServiceInterface;
using Seccion.Web.Common;
using Seccion.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Validation;
using System.Data.SqlTypes;
using System.Globalization;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Seccion.Web.Controllers
{
    public class UploadLayoutController : Controller
    {
        public readonly INotifier _INotifier;
        public readonly IStatesService _IStatesService;
        public readonly IMunicipalitiesService _IMunicipalitiesService;
        public readonly IEmployeeService _IEmployeeService;
        public readonly ITbBoxesService _ITbBoxesService;

        public UploadLayoutController(INotifier iNotifier, IStatesService iStatesService, IMunicipalitiesService iMunicipalitiesService, IEmployeeService iEmployeeService, ITbBoxesService ITbBoxesService)
        {
            _INotifier = iNotifier;
            _IStatesService = iStatesService;
            _IMunicipalitiesService = iMunicipalitiesService;
            _IEmployeeService = iEmployeeService;
            _ITbBoxesService = ITbBoxesService;
        }

        // GET: UploadLayout
        public ActionResult UplaoadMembers()
        {
            return View();
        }


        [HttpPost]
        public async Task<ActionResult> UplaoadMembers(HttpPostedFileBase file)
        {
            try
            {
                var lstStates = await _IStatesService.GetAll();
                var lstMunicipalities = await _IMunicipalitiesService.GetAll();

                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (ExcelPackage package = new ExcelPackage(file.InputStream))
                {
                    var lstEmployee = new List<TbEmployee>();
                    var CurrentSheet = package.Workbook.Worksheets;
                    var WorkSheet = CurrentSheet.First();
                    var TotalRow = WorkSheet.Dimension.End.Row;

                    for (int iRow = 4; iRow <= TotalRow; iRow++)
                    {
                        var employeeViewModel = new EmployeeVIewModel();

                        if (WorkSheet.Cells[iRow, 2].Value != null)
                        {
                            employeeViewModel.NumberFicha = WorkSheet.Cells[iRow, 2].Text;
                        }
                        else
                        {
                            _INotifier.AddMessage(MessageSeverity.Info, $"Fila: {iRow}, Columna: {WorkSheet.Cells[iRow, 2].Address}", "Este campo es requerido");
                        }
                        if (WorkSheet.Cells[iRow, 3].Value != null)
                        {
                            employeeViewModel.Name = WorkSheet.Cells[iRow, 3].Text;
                        }
                        else
                        {
                            _INotifier.AddMessage(MessageSeverity.Info, $"Fila: {iRow}, Columna: {WorkSheet.Cells[iRow, 3].Address}", "Este campo es requerido");
                        }
                        if (WorkSheet.Cells[iRow, 4].Value != null)
                        {
                            employeeViewModel.FirstName = WorkSheet.Cells[iRow, 4].Text;
                        }
                        else
                        {
                            _INotifier.AddMessage(MessageSeverity.Info, $"Fila: {iRow}, Columna: {WorkSheet.Cells[iRow, 4].Address}", "Este campo es requerido");
                        }
                        if (WorkSheet.Cells[iRow, 5].Value != null)
                        {
                            employeeViewModel.LastName = WorkSheet.Cells[iRow, 5].Text;
                        }
                        else
                        {
                            _INotifier.AddMessage(MessageSeverity.Info, $"Fila: {iRow}, Columna: {WorkSheet.Cells[iRow, 5].Address}", "Este campo es requerido");
                        }
                        employeeViewModel.RFC = WorkSheet.Cells[iRow, 6].Value != null ? WorkSheet.Cells[iRow, 6].Text : null;
                        employeeViewModel.CURP = WorkSheet.Cells[iRow, 7].Value != null ? WorkSheet.Cells[iRow, 7].Text : null;
                        employeeViewModel.INE = WorkSheet.Cells[iRow, 8].Value != null ? WorkSheet.Cells[iRow, 8].Text : null;
                        if (WorkSheet.Cells[iRow, 9].Value != null)
                        {
                            var value = WorkSheet.Cells[iRow, 9].Text;
                            if (Enum.IsDefined(typeof(EmployeeRol), value))
                            {
                                employeeViewModel.Rol = value;
                            }
                            else
                            {
                                _INotifier.AddMessage(MessageSeverity.Info, $"Fila: {iRow}, Columna: {WorkSheet.Cells[iRow, 9].Address}", "El Rol no se encuentra registrado, favor de validar");
                            }
                        }
                        else
                        {
                            employeeViewModel.Rol = null;
                        }

                        if (WorkSheet.Cells[iRow, 10].Value != null)
                        {
                            var value = WorkSheet.Cells[iRow, 10].Text;
                            if (Enum.IsDefined(typeof(Region), value))
                            {
                                employeeViewModel.Region = value;
                            }
                            else
                            {
                                _INotifier.AddMessage(MessageSeverity.Info, $"Fila: {iRow}, Columna: {WorkSheet.Cells[iRow, 10].Address}", "La Region no se encuentra registrada, favor de validar");
                            }
                        }
                        else
                        {
                            employeeViewModel.Region = null;
                        }

                        if (WorkSheet.Cells[iRow, 11].Value != null)
                        {
                            var value = WorkSheet.Cells[iRow, 11].Text;
                            if (Enum.IsDefined(typeof(ContractualSituation), value))
                            {
                                employeeViewModel.ContractualSituation = value;
                            }
                            else
                            {
                                _INotifier.AddMessage(MessageSeverity.Info, $"Fila: {iRow}, Columna: {WorkSheet.Cells[iRow, 11].Address}", "La Situacion Contractual no se encuentra registrada, favor de validar");
                            }
                        }
                        else
                        {
                            employeeViewModel.ContractualSituation = null;
                        }

                        if (WorkSheet.Cells[iRow, 12].Value != null)
                        {
                            var value = WorkSheet.Cells[iRow, 12].Text;
                            if (Enum.IsDefined(typeof(WorkLocation), value))
                            {
                                employeeViewModel.WorkLocation = value;
                            }
                            else
                            {
                                _INotifier.AddMessage(MessageSeverity.Info, $"Fila: {iRow}, Columna: {WorkSheet.Cells[iRow, 12].Address}", "La Ubicacion Laboral no se encuentra registrada, favor de validar");
                            }
                        }
                        else
                        {
                            employeeViewModel.WorkLocation = null;
                        }
                        employeeViewModel.DepartmentKey = WorkSheet.Cells[iRow, 13].Value != null ? WorkSheet.Cells[iRow, 13].Text : null;
                        employeeViewModel.AsisteKey = WorkSheet.Cells[iRow, 14].Value != null ? WorkSheet.Cells[iRow, 14].Text : null;

                        if (WorkSheet.Cells[iRow, 15].Value != null)
                        {
                            employeeViewModel.Level = Convert.ToInt32(WorkSheet.Cells[iRow, 15].Text);
                        }
                        else
                        {
                            _INotifier.AddMessage(MessageSeverity.Info, $"Fila: {iRow}, Columna: {WorkSheet.Cells[iRow, 15].Address}", "Este campo es requerido");
                        }

                        if (WorkSheet.Cells[iRow, 16].Value != null)
                        {
                            employeeViewModel.Address = WorkSheet.Cells[iRow, 16].Text;
                        }
                        else
                        {
                            _INotifier.AddMessage(MessageSeverity.Info, $"Fila: {iRow}, Columna: {WorkSheet.Cells[iRow, 16].Address}", "Este campo es requerido");
                        }

                        if (WorkSheet.Cells[iRow, 17].Value != null)
                        {

                            var NameState = WorkSheet.Cells[iRow, 17].Text;
                            var objStates = lstStates.FirstOrDefault(x => x.NameSate == NameState);
                            if (objStates != null)
                            {
                                employeeViewModel.TbStatesId = objStates.TbStatesId;

                                if (WorkSheet.Cells[iRow, 18].Value != null)
                                {
                                    var NameMunicipalities = WorkSheet.Cells[iRow, 18].Text;
                                    var objMunicipalities = lstMunicipalities.FirstOrDefault(x => x.TbStatesId == objStates.TbStatesId && x.NameMunicipalities == NameMunicipalities);
                                    if (objStates != null)
                                    {
                                        employeeViewModel.TbMunicipalitiesId = objMunicipalities.TbMunicipalitiesId;
                                    }
                                    else
                                    {
                                        _INotifier.AddMessage(MessageSeverity.Info, $"Fila: {iRow}, Columna: {WorkSheet.Cells[iRow, 18].Address}", "la Ciudad no se encuentra registrada");
                                    }
                                }
                                else
                                {
                                    _INotifier.AddMessage(MessageSeverity.Info, $"Fila: {iRow}, Columna: {WorkSheet.Cells[iRow, 18].Address}", "Este campo es requerido");
                                }
                            }
                            else
                            {
                                _INotifier.AddMessage(MessageSeverity.Info, $"Fila: {iRow}, Columna: {WorkSheet.Cells[iRow, 17].Address}", "El Estado no se encuentra registrado");
                            }
                        }
                        else
                        {
                            _INotifier.AddMessage(MessageSeverity.Info, $"Fila: {iRow}, Columna: {WorkSheet.Cells[iRow, 17].Address}", "Este campo es requerido");
                        }

                        employeeViewModel.PhoneHouse = WorkSheet.Cells[iRow, 19].Value != null ? WorkSheet.Cells[iRow, 19].Text : null;
                        employeeViewModel.CelularPhone = WorkSheet.Cells[iRow, 20].Value != null ? WorkSheet.Cells[iRow, 20].Text : null;
                        employeeViewModel.Email = WorkSheet.Cells[iRow, 21].Value != null ? WorkSheet.Cells[iRow, 21].Text : null;

                        if (WorkSheet.Cells[iRow, 22].Value != null)
                        {
                            if (Utilities.ValidateDate(WorkSheet.Cells[iRow, 22].Text))
                            {
                                employeeViewModel.BirthDate = WorkSheet.Cells[iRow, 22].Text;
                            }
                            else
                            {
                                _INotifier.AddMessage(MessageSeverity.Info, $"Fila: {iRow}, Columna: {WorkSheet.Cells[iRow, 22].Address}", "La fecha no cuenta con el formato correcto, dd/mm/yyyy");
                            }
                        }
                        else
                        {
                            employeeViewModel.BirthDate = null;
                        }

                        if (WorkSheet.Cells[iRow, 23].Value != null)
                        {
                            employeeViewModel.Bank = WorkSheet.Cells[iRow, 23].Text;
                        }
                        else
                        {
                            _INotifier.AddMessage(MessageSeverity.Info, $"Fila: {iRow}, Columna: {WorkSheet.Cells[iRow, 23].Address}", "Este campo es requerido");
                        }

                        if (WorkSheet.Cells[iRow, 24].Value != null)
                        {
                            employeeViewModel.BranchOffice = WorkSheet.Cells[iRow, 24].Text;
                        }
                        else
                        {
                            _INotifier.AddMessage(MessageSeverity.Info, $"Fila: {iRow}, Columna: {WorkSheet.Cells[iRow, 24].Address}", "Este campo es requerido");
                        }

                        if (WorkSheet.Cells[iRow, 25].Value != null)
                        {
                            employeeViewModel.AccountNumber = WorkSheet.Cells[iRow, 25].Text;
                        }
                        else
                        {
                            _INotifier.AddMessage(MessageSeverity.Info, $"Fila: {iRow}, Columna: {WorkSheet.Cells[iRow, 25].Address}", "Este campo es requerido");
                        }

                        if (WorkSheet.Cells[iRow, 26].Value != null)
                        {
                            employeeViewModel.ClaveInterbancaria = WorkSheet.Cells[iRow, 26].Text;
                        }
                        else
                        {
                            _INotifier.AddMessage(MessageSeverity.Info, $"Fila: {iRow}, Columna: {WorkSheet.Cells[iRow, 26].Address}", "Este campo es requerido");
                        }

                        var objEmployeet = Mapper.Map<EmployeeVIewModel, TbEmployee>(employeeViewModel);
                        objEmployeet.ActiveEmployee = true;
                        lstEmployee.Add(objEmployeet);

                    }

                    if (_INotifier.Messages.Count > 0)
                    {
                        return RedirectToAction("UplaoadMembers");
                    }
                    else
                    {
                        _IEmployeeService.SaveEmployeeData(lstEmployee);
                        await _IEmployeeService.Save();
                        _INotifier.Success("Exito", "Operacion Exitosa");
                    }
                }
                return View("UplaoadMembers");
            }
            catch (Exception ex)
            {
                _INotifier.Error("Error", $"Ocurrio el siguiente problema: {ex.Message}");
                return View("UplaoadMembers");
            }
        }

        public ActionResult UploadInformationBoxes()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> UploadInformationBoxes(UploadInformationBoxesViewModel model)
        {
            try
            {
                if (!model.OldBox && !model.CashBox && !model.NewBox)
                {
                    _INotifier.Warning("upss!!", "Te has olvidado de seleccionar el tipo de Archivo a subir.");
                    return RedirectToAction("UploadInformationBoxes");
                }
                var lstEmployee = await _IEmployeeService.GetEmployeeAllForRequest();
                var lstProperties = typeof(BoxesViewModel).GetProperties();
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (ExcelPackage package = new ExcelPackage(model.file.InputStream))
                {
                    var lstTbBoxes = new List<TbBoxes>();
                    var CurrentSheet = package.Workbook.Worksheets;
                    var WorkSheet = CurrentSheet.First();
                    var TotalRow = WorkSheet.Dimension.End.Row;
                    var TotalCol = WorkSheet.Dimension.End.Column;
                    for (int iRow = 4; iRow <= TotalRow; iRow++)
                    {
                        var objBoxesViewModel = new BoxesViewModel();
                        var iProperty = 1;
                        for (int iCol = 2; iCol <= TotalCol; iCol++)
                        {
                            var property = lstProperties[iProperty];
                            switch (iCol)
                            {
                                case 2:
                                    if (WorkSheet.Cells[iRow, iCol].Value != null)
                                    {
                                        var objEmployee = lstEmployee.FirstOrDefault(x => x.NumberFicha == WorkSheet.Cells[iRow, iCol].Text);
                                        if (objEmployee != null)
                                        {
                                            property.SetValue(objBoxesViewModel, Utilities.ChangeType(objEmployee.TbEmployeeId, property.PropertyType));
                                        }
                                        else
                                        {
                                            _INotifier.AddMessage(MessageSeverity.Info, $"Fila: {iRow}, Columna: {WorkSheet.Cells[iRow, iCol].Address}", "El numero de Ficha no se encutra registrado");
                                        }
                                    }
                                    else
                                    {
                                        _INotifier.AddMessage(MessageSeverity.Info, $"Fila: {iRow}, Columna: {WorkSheet.Cells[iRow, iCol].Address}", "Este campo es requerido");
                                    }
                                    break;
                                case 12:
                                case 15:
                                case 18:
                                    if (model.OldBox)
                                    {
                                        if (WorkSheet.Cells[iRow, iCol].Value != null)
                                        {
                                            property.SetValue(objBoxesViewModel, Utilities.ChangeType(WorkSheet.Cells[iRow, iCol].Value, property.PropertyType));
                                        }
                                        else
                                        {
                                            _INotifier.AddMessage(MessageSeverity.Info, $"Fila: {iRow}, Columna: {WorkSheet.Cells[iRow, iCol].Address}", "Este campo es requerido");
                                        }
                                    }
                                    else
                                    {
                                        property.SetValue(objBoxesViewModel, Utilities.ChangeType(WorkSheet.Cells[iRow, iCol].Value, property.PropertyType));
                                    }
                                    break;
                                case 19:
                                    if (model.OldBox)
                                    {
                                        if (WorkSheet.Cells[iRow, iCol].Value != null)
                                        {
                                            var value = WorkSheet.Cells[iRow, iCol].Value.Equals("SI");
                                            property.SetValue(objBoxesViewModel, Utilities.ChangeType(value, property.PropertyType));
                                        }
                                        else
                                        {
                                            _INotifier.AddMessage(MessageSeverity.Info, $"Fila: {iRow}, Columna: {WorkSheet.Cells[iRow, iCol].Address}", "Este campo es requerido");
                                        }
                                    }
                                    else
                                    {
                                        var value = (WorkSheet.Cells[iRow, iCol].Value != null) && WorkSheet.Cells[iRow, iCol].Value.Equals("SI");
                                        property.SetValue(objBoxesViewModel, Utilities.ChangeType(value, property.PropertyType));
                                    }
                                    break;
                                case 21:
                                    if (!model.OldBox)
                                    {
                                        if (WorkSheet.Cells[iRow, iCol].Value != null)
                                        {
                                            property.SetValue(objBoxesViewModel, Utilities.ChangeType(WorkSheet.Cells[iRow, iCol].Value, property.PropertyType));
                                        }
                                        else
                                        {
                                            _INotifier.AddMessage(MessageSeverity.Info, $"Fila: {iRow}, Columna: {WorkSheet.Cells[iRow, iCol].Address}", "Este campo es requerido");
                                        }
                                    }
                                    else
                                    {
                                        property.SetValue(objBoxesViewModel, Utilities.ChangeType(WorkSheet.Cells[iRow, iCol].Value, property.PropertyType));
                                    }
                                    break;
                                default:
                                    if (WorkSheet.Cells[iRow, iCol].Value != null)
                                    {
                                        if ("DateTime" == property.GetType().Name)
                                        {
                                            if (Utilities.ValidateDate(WorkSheet.Cells[iRow, iCol].Text))
                                            {
                                                property.SetValue(objBoxesViewModel, Utilities.ChangeType(WorkSheet.Cells[iRow, iCol].Value, property.PropertyType));
                                            }
                                            else
                                            {
                                                _INotifier.AddMessage(MessageSeverity.Info, $"Fila: {iRow}, Columna: {WorkSheet.Cells[iRow, 6].Address}", "La fecha no cuenta con el formato correcto, dd/mm/yyyy");
                                            }
                                        }
                                        else
                                        {
                                            property.SetValue(objBoxesViewModel, Utilities.ChangeType(WorkSheet.Cells[iRow, iCol].Value, property.PropertyType));
                                        }

                                    }
                                    else
                                    {
                                        _INotifier.AddMessage(MessageSeverity.Info, $"Fila: {iRow}, Columna: {WorkSheet.Cells[iRow, iCol].Address}", "Este campo es requerido");
                                    }
                                    break;
                            }
                            iProperty++;
                        }

                        var objBoxes = Mapper.Map<BoxesViewModel, TbBoxes>(objBoxesViewModel);

                        if (model.OldBox)
                        {
                            objBoxes.TypeInformation = (int)TypeInformation.OldBox;
                        }
                        else if (model.CashBox)
                        {
                            objBoxes.TypeInformation = (int)TypeInformation.CashBox;
                        }
                        else
                        {
                            objBoxes.TypeInformation = (int)TypeInformation.NewBox;
                        }

                        objBoxes.UserInsert = User.Identity.Name;
                        objBoxes.DateInsert = DateTime.Now;
                        lstTbBoxes.Add(objBoxes);
                    }

                    if (_INotifier.Messages.Count > 0)
                    {
                        return RedirectToAction("UploadInformationBoxes");
                    }
                    else
                    {
                        _ITbBoxesService.CreateModel(lstTbBoxes);
                        await _ITbBoxesService.Save();
                        _INotifier.Success("Exito", "Operacion Exitosa");
                    }
                }
                return View("UploadInformationBoxes");
            }
            catch (Exception ex)
            {
                _INotifier.Error("Error", $"Ocurrio el siguiente problema: {ex.Message}");
                return View("UploadInformationBoxes");
            }
        }
    }
}
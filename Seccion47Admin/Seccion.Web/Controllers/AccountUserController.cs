﻿
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Seccion.Data.Enum;
using Seccion.Data.ExtensionsIIdentity;
using Seccion.Model.IdentityModel;
using Seccion.Service.Common.Message;
using Seccion.Service.ServiceInterface;
using Seccion.Web.Common;
using Seccion.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Seccion.Web.Controllers
{
    [Authorize]
    public class AccountUserController : Controller
    {
        private readonly INotifier notifier;
        private readonly IAccountUserService accountUserService;
		private readonly ISendingEmailsService sendingEmailsService;

		public AccountUserController(INotifier notifier, IAccountUserService  accountUserService, ISendingEmailsService sendingEmailsService)//, MapperConfiguration config)
        {
            this.notifier = notifier;
            this.accountUserService = accountUserService;
			this.sendingEmailsService = sendingEmailsService;
		}
        // GET: AccountUser
        public async Task<ActionResult> ListAccountUsers()
        {
            IEnumerable<ApplicationUser> listUserModel = await accountUserService.GetUserList();

            IEnumerable<ListAccountUsersViewModel> mapperListUserModel = Mapper.Map<IEnumerable<ApplicationUser>, IEnumerable<ListAccountUsersViewModel>>(listUserModel);

            return View(mapperListUserModel);
        }
        // GET: AccountUser
        public ActionResult RegisterUser()
        {
            var listRoles = accountUserService.GetRoles();
            ViewBag.Role = new SelectList(listRoles.OrderBy(x => x.Item1), "Item1", "Item2" );
            ViewBag.Gender = new SelectList( ListsDrops.GetListGender(), "Value", "Text");
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RegisterUser(AccountUserViewModel userViewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var listRoles = accountUserService.GetRoles();
                    ViewBag.Role = new SelectList(listRoles.OrderBy(x => x.Item1), "Item1", "Item2");
                    ViewBag.Gender = new SelectList(ListsDrops.GetListGender(), "Value", "Text");
                    return View();
                }
                var user = Mapper.Map<AccountUserViewModel, ApplicationUser>(userViewModel);
                user.JoinDate = DateTime.Now;
                user.Active = true;
                user.UserName = CreateUserName.GenerateRandomUsername(user.Name + user.FirstName + user.LastName);
                var resultCreate = await accountUserService.AccountUserCreate(user, user.UserName);
                if (resultCreate.Succeeded)
                {
                    string nameRole = await accountUserService.GetRoleById(Convert.ToInt32(userViewModel.Role));
                    await accountUserService.AddRole(user.Id, nameRole);
                    string resultToken = await accountUserService.EmailConfirmation(user.Id);
                    var emailAccountConfirm = Url.Action("ConfirmEmail", "Login", new { userId = user.Id, code = resultToken }, Request.Url.Scheme);
                    await sendingEmailsService.ForgotPasswordEmailService(user.Email, string.Join(" ", user.Name, user.FirstName, user.LastName), emailAccountConfirm, EmailType.AccoutConfirmEmail);
                    string userName = "<strong>" + string.Join(" ", user.Name, user.FirstName, user.LastName) + "</strong>";
                    notifier.Success("Exito!!", string.Format("{0}{1}{2}", "Los datos del usuario se registraron correctamente. Se envio un correo a ", userName, " para validar y confirmar su cuenta."));
                    
                }
                return RedirectToAction("ListAccountUsers", "AccountUser");
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

        // GET: Editar usaurio
        public async Task<ActionResult> EditRegisterUser(int id)
        {
            var resultUser = await accountUserService.GetUserByIdAsync(id);
            var listRoles = accountUserService.GetRoles();
            ViewBag.Role = new SelectList(listRoles.OrderBy(x => x.Item1), "Item1", "Item2", resultUser.Roles.SingleOrDefault().RoleId);
            ViewBag.Gender = new SelectList(ListsDrops.GetListGender(), "Value", "Text", resultUser.Gender);
            AccountUserViewModel modelo = Mapper.Map<ApplicationUser, AccountUserViewModel>(resultUser);
            
            return View(modelo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditRegisterUser(AccountUserViewModel userViewModel)
        {
            try
            {
                var resultUser = await accountUserService.GetUserByIdAsync(userViewModel.Id);
                var updateData = Mapper.Map(userViewModel, resultUser);

                accountUserService.AccountUserUpdate(updateData);
                await accountUserService.SaveUser();
                
                var userRoles = await accountUserService.GetRoleByUserId(updateData.Id);
                if (userRoles.Any())
                {
                    foreach(var rol in userRoles)
                    {
                        await accountUserService.DeleteRoleByUserId(updateData.Id, rol);
                    }
                }
                string nameRole = await accountUserService.GetRoleById(Convert.ToInt32(userViewModel.Role));
                await accountUserService.AddRole(updateData.Id, nameRole);
                notifier.Success("Éxito !!", "Los datos del usuario se actualizaron correctamente.");
               

                return RedirectToAction("ListAccountUsers", "AccountUser");
            }
            catch (Exception ex)
            {
                return View("Error", new HandleErrorInfo(ex, "AccountUser", "ListAccountUsers"));
            }
            
        }
        
 
        public async Task<ActionResult> UserDetail(int id)
		{
            var resultUser = await accountUserService.GetUserByIdAsync(id);
            var model = Mapper.Map<ApplicationUser, AccountUserViewModel>(resultUser);
            foreach(var rol in resultUser.Roles)
			{
                model.Role = rol.Role.Name;
			}
            return View(model);
		}
        public async Task<JsonResult> InactiveUser(int id)
		{
            try
			{
                var resultUser = await accountUserService.GetUserByIdAsync(id);

                resultUser.Active = false;

                accountUserService.AccountUserUpdate(resultUser);
                await accountUserService.SaveUser();

                return Json(new Message { Title = "Exito!!", TextMessage = "La operación se realizo Exitosamente.", Severity = MessageSeverity.Success });
            }
            catch(Exception ex)
			{
                return Json(new Message { Title = "Error", TextMessage = ex.Message, Severity = MessageSeverity.Danger });
            }
		}
        public async Task<ActionResult> MyUserData()
        {
            var resultUser = await accountUserService.GetUserByIdAsync(Convert.ToInt32(User.Identity.GetUserId()));
            MyDataViewModel modelo = Mapper.Map<ApplicationUser, MyDataViewModel>(resultUser);
            return View(modelo);
        }
        public async Task<ActionResult> EditMyData(int id)
        {
            var resultUser = await accountUserService.GetUserByIdAsync(id);

            ViewBag.Gender = new SelectList(ListsDrops.GetListGender(), "Value", "Text", resultUser.Gender);
            MyDataViewModel modelo = Mapper.Map<ApplicationUser, MyDataViewModel>(resultUser);
            return View(modelo);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditMyData(MyDataViewModel model)
        {
            try
            {
                var resultUser = await accountUserService.GetUserByIdAsync(model.Id);
                var updateData = Mapper.Map(model, resultUser);

                accountUserService.AccountUserUpdate(updateData);
                accountUserService.SaveUser();

                notifier.Success("Éxito !!", "Tus datos se actualizaron correctamente. Recuerda, si modificaste el <strong> Nombre de Usuario </strong>, el proximo inicio de sesion es con el nuevo <strong> Nombre de Usuario </strong>");


                return RedirectToAction("MyUserData", "AccountUser");
            }
            catch (Exception ex)
            {

                return View("Error", new HandleErrorInfo(ex, "AccountUser", "MyUserData"));
            }
            
        }

        public ActionResult ChangePassword()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await accountUserService.ChangePasswordUser(Convert.ToInt32(User.Identity.GetUserId()), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                //var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                //if (user != null)
                //{
                //    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                //}
                return Json(new { success = true, message = "changePassword" }, JsonRequestBehavior.AllowGet);
            }
            //AddErrors(result);
            //return View(model);
            return Json(new { success = false, message = result }, JsonRequestBehavior.AllowGet);

        }
        public JsonResult LogOff()
        {
            var authenticationManager = HttpContext.GetOwinContext().Authentication;
            authenticationManager.SignOut();
            var ctx = Request.GetOwinContext();
            ctx.Response.Cookies.Delete("__RequestVerificationToken");
            AuthenticationManager.SignOut();
            HttpContext.User = null;
            string[] myCookies = Request.Cookies.AllKeys;
            foreach (string cookie in myCookies)
            {
                Response.Cookies[cookie].Expires = DateTime.Now.AddDays(-1);
            }
            return Json("Login/LoginOn");
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
        private void AddErrorsR(IEnumerable<ModelError> result)
        {
            foreach (var error in result)
            {
                ModelState.AddModelError("", error.ErrorMessage);
            }
        }

        private bool HasPassword()
        {
            //var user = UserManager.FindById(User.Identity.GetUserId());
            //if (user != null)
            //{
            //    return user.PasswordHash != null;
            //}
            return false;
        }

        private bool HasPhoneNumber()
        {
            //var user = UserManager.FindById(User.Identity.GetUserId());
            //if (user != null)
            //{
            //    return user.PhoneNumber != null;
            //}
            return false;
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }

        #endregion
    }
}
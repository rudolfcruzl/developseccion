﻿using AutoMapper;
using Seccion.Data.ResposeDto;
using Seccion.Model.Models;
using Seccion.Service.Common.Message;
using Seccion.Service.ServiceInterface;
using Seccion.Web.Common;
using Seccion.Web.Models.GridListModel;
using Seccion.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Seccion.Web.Controllers
{
	[Authorize]
    public class RequestController : Controller
    {
        private readonly INotifier notifier;
        private readonly IEmployeeService employeeService;
        private readonly IRequestService requestService;
        private readonly ITbBoxesService _ITbBoxesService;

        public RequestController(INotifier notifier, IEmployeeService employeeService, IRequestService requestService, ITbBoxesService ITbBoxesService)
        {
            this.notifier = notifier;
            this.employeeService = employeeService;
            this.requestService = requestService;
            _ITbBoxesService = ITbBoxesService;
        }


        public ActionResult ListRequestPrueba()
        {
            return View();
        }

        // GET: Request
        public ActionResult ListRequest()
        {
            return View();
        }

        public async Task<JsonResult> GetListRequestResponse(DatatableServerSideModel param, string fechaInicial, string fechaFinal)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[7];
                parameters[0] = new SqlParameter("@DisplayLength", param.length);
                parameters[1] = new SqlParameter("@DisplayStart", param.start);
                parameters[2] = new SqlParameter("@SortCol", param.order[0].column);
                parameters[3] = new SqlParameter("@SortDir", param.order[0].dir);
                parameters[4] = new SqlParameter("@Search", param.search.value);
                parameters[5] = new SqlParameter("@DateStart", fechaInicial);
                parameters[6] = new SqlParameter("@DateEnd", fechaFinal);

                var requestList = await requestService.GetListRequest(parameters);
                var model = Mapper.Map<IEnumerable<SP_RequestDto>, IEnumerable<ListRequestViewModelcs>>(requestList);
                int RecordsTotal = model.FirstOrDefault().TotalRequest;
                int FilterCount = model.FirstOrDefault().TotalFilter;
                return Json(new
                {
                    draw = Convert.ToInt32(param.draw),
                    recordsTotal = RecordsTotal,
                    recordsFiltered = FilterCount,
                    data = model
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new Message { Title = "Error", TextMessage = ex.Message, Severity = MessageSeverity.Danger });
            }
        }
        [HttpGet]
        public async Task<ActionResult> CreateRequest()
        {
            var listEmployee = await employeeService.GetEmployeeAllForRequest();
            ViewBag.Employee = new SelectList(listEmployee.OrderBy(x => x.TbEmployeeId), "NumberFicha", "EmployeeNameComplet");
            List<SelectListItem> list = new List<SelectListItem>() { new SelectListItem() { Text = "", Value = "0" } };
            ViewBag.AvalKey = new SelectList(list, "Text", "Value");
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateRequest(RequestViewModel model)
        {
            try
            {
                if (model.AdministrationExpensesResult == null && model.TotalPay == null && model.AbonoCatorcenalMensual == null)
                {
                    decimal Expensesresult, TotalPay, CatorcenalMensual;
                    Calculator.OperationResult(model, out Expensesresult, out TotalPay, out CatorcenalMensual);
                    model.AdministrationExpensesResult = SeparateDecimal.CleanDecimalFromDataBase(Expensesresult.ToString());
                    model.TotalPay = SeparateDecimal.CleanDecimalFromDataBase(TotalPay.ToString());
                    model.AbonoCatorcenalMensual = SeparateDecimal.CleanDecimalFromDataBase(CatorcenalMensual.ToString());
                }
                var request = Mapper.Map<RequestViewModel, TbRequest>(model);
                //En este caso request.TbEmployeeId es la Ficha del empleado, por tema de validacion en el FrontEnd
                var result = await employeeService.GetEmployeeByFicha(request.TbEmployeeId.ToString());
                request.TbEmployeeId = result.EmployeeId;
                if (request.AvalKey != null)
                {
                    //En este caso request.AvalKey es la Ficha del empleado, por tema de validacion en el FrontEnd
                    var resultAval = await employeeService.GetEmployeeByFicha(request.AvalKey.ToString());
                    request.AvalKey = resultAval.EmployeeId.ToString();
                    request.AvalName = string.Join(" ", resultAval.NameEmployee, resultAval.FirstName, resultAval.LastName);
                }
                request.RequestNumber = requestService.GetMaxOrderNumber();
                request.DateCreateRequest = DateTime.Now;
                request.DateInsert = DateTime.Now;
                request.Status = 0;
                request.UserInsert = User.Identity.Name;
                requestService.SaveRequest(request);
                await requestService.Save();
                notifier.Success("Éxito!!", "Los datos de la Solicitud se Guardaron correctamente.");
                return RedirectToAction("ListRequest", "Request");
            }
            catch (Exception ex)
            {

                return View("Error", new HandleErrorInfo(ex, "ListRequest", "Request"));
            }


        }
        [HttpGet]
        public async Task<ActionResult> EditRequest(int id)
        {
            var resultRequest = await requestService.GetRequestById(id);
            RequestViewModel model = null;
            if (resultRequest != null)
            {
                model = Mapper.Map<TbRequest, RequestViewModel>(resultRequest);
                model.Ficha = resultRequest.Employee.NumberFicha;
                if (model.AvalName != null && model.AvalKey != null)
                {
                    var fichaAval = await employeeService.GetEmployeeById(int.Parse(model.AvalKey));
                    model.FichaAval = fichaAval.NumberFicha;
                }
                if (model.DebitAval != null)
                    model.DebitAval = model.DebitAval.Replace(",", ".");

                model.Location = resultRequest.Employee.WorkLocation == "TIERRA" ? 1 : 2;
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditRequest(RequestViewModel model)
        {
            try
            {
                var resultRequest = await requestService.GetRequestById(model.TbRequestId);
                if (resultRequest != null)
                {
                    //En este caso request.TbEmployeeId es la Ficha del empleado, por tema de validacion en el FrontEnd
                    var resultEmployee = await employeeService.GetEmployeeByFicha(model.TbEmployeeId.ToString());
                    model.TbEmployeeId = resultEmployee.EmployeeId;
                    if (model.AvalKey != null)
                    {
                        //En este caso request.AvalKey es la Ficha del empleado, por tema de validacion en el FrontEnd
                        var resultAval = await employeeService.GetEmployeeByFicha(model.AvalKey.ToString());
                        model.AvalKey = resultAval.EmployeeId.ToString();
                        model.AvalName = string.Join(" ", resultAval.NameEmployee, resultAval.FirstName, resultAval.LastName);
                    }
                    decimal Expensesresult, TotalPay, CatorcenalMensual;
                    Calculator.OperationResult(model, out Expensesresult, out TotalPay, out CatorcenalMensual);
                    model.AdministrationExpensesResult = SeparateDecimal.CleanDecimalFromDataBase(Expensesresult.ToString());
                    model.TotalPay = SeparateDecimal.CleanDecimalFromDataBase(TotalPay.ToString());
                    model.AbonoCatorcenalMensual = SeparateDecimal.CleanDecimalFromDataBase(CatorcenalMensual.ToString());
                    resultRequest = Mapper.Map(model, resultRequest);

                    resultRequest.DateUpdate = DateTime.Now;
                    resultRequest.UserUpdate = User.Identity.Name;
                    requestService.UpdateRequest(resultRequest);
                    await requestService.Save();
                    notifier.Success("Éxito!!", "Los datos del Empleado se Actualizaron correctamente.");
                }
                return RedirectToAction("ListRequest", "Request");
            }
            catch (Exception ex)
            {
                return View("Error", new HandleErrorInfo(ex, "ListRequest", "Request"));
            }
        }

        [HttpGet]
        public async Task<ActionResult> DetailsRequest(int id)
        {
            try
            {
                var resultRequest = await requestService.GetRequestById(id);
                RequestViewModel model = Mapper.Map<TbRequest, RequestViewModel>(resultRequest);
                return View(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public ActionResult ListOldBox()
        {
            return View();
        }

        public ActionResult ListCashBox()
        {
            return View();
        }

        public ActionResult ListNewBox()
        {
            return View();
        }

        public async Task<JsonResult> GetListBox(DatatableServerSideModel param, int TypeInformation)
        {
            SqlParameter[] parameters = new SqlParameter[6];
            parameters[0] = new SqlParameter("@TypeInformation", TypeInformation);
            parameters[1] = new SqlParameter("@DisplayLength", param.length);
            parameters[2] = new SqlParameter("@DisplayStart", param.start);
            parameters[3] = new SqlParameter("@SortCol", param.order[0].column);
            parameters[4] = new SqlParameter("@SortDir", param.order[0].dir);
            parameters[5] = new SqlParameter("@Search", param.search.value);

            var lstOldBoxInfo = await _ITbBoxesService.GetInfoBox("SP_GetInfoBox", parameters);
            int RecordsTotal = lstOldBoxInfo.FirstOrDefault().TotalData;
            int FilterCount = lstOldBoxInfo.FirstOrDefault().TotalFilter;
            return Json(new
            {
                draw = Convert.ToInt32(param.draw),
                recordsTotal = RecordsTotal,
                recordsFiltered = FilterCount,
                data = lstOldBoxInfo
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetInfoTbBox(int TbBoxId)
        {
            var objTbBox = _ITbBoxesService.GetTbBoxById(TbBoxId);
            return PartialView("_DetailBox", objTbBox);
        }

        public ActionResult EditDataBox(int TbBoxId, string RootActionName)
        {
            var objTbBox = _ITbBoxesService.GetTbBoxById(TbBoxId);
            var model = Mapper.Map<TbBoxes, BoxesViewModel>(objTbBox);
            model.RootActionName = RootActionName;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditDataBox(BoxesViewModel model)
        {
            try
            {
                var updateTbBox = _ITbBoxesService.GetTbBoxById(model.TbBoxesId);
                if (updateTbBox != null)
                {
                    updateTbBox = Mapper.Map(model, updateTbBox);
                    updateTbBox.UserUpdate = User.Identity.Name;
                    updateTbBox.DateUpdate = DateTime.Now;
                    _ITbBoxesService.UpodateModel(updateTbBox);
                    await _ITbBoxesService.Save();
                    notifier.Success("Éxito!!", "Los datos se actualizaron de manera correcta.");
                    return RedirectToAction(model.RootActionName, "Request");
                }

                return View("Error", new HandleErrorInfo(new Exception("A ocurrido un Error, verifica nuevamente"), "ListOldBox", "Request"));
            }
            catch (Exception ex)
            {

                return View("Error", new HandleErrorInfo(ex, "ListOldBox", "Request"));
            }
        }
    }
}
﻿using Microsoft.Owin.Security;
using Seccion.Data.Enum;
using Seccion.Service.Common.Message;
using Seccion.Service.ServiceInterface;
using Seccion.Web.Models;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Seccion.Web.Models.ViewModels;
using NWebsec.Mvc.HttpHeaders;
using Microsoft.AspNet.Identity;
using System.Data.SqlClient;
using Seccion.Web.Common;
using Seccion.Web.Models.Dtos;
using System.Security.Claims;
using System.Collections.Generic;
using Seccion.Data.Infrastructure;

namespace Seccion.Web.Controllers
{
    public class LoginController : Controller
    {
        private readonly IAccountUserService _accountUserService;
        private readonly ISendingEmailsService sendingEmailsService;
        private readonly INotifier notifier;
        private readonly IMenuService _IMenuService;

        public LoginController(IAccountUserService accountUserService, ISendingEmailsService sendingEmailsService, INotifier notifier, IMenuService IMenuService)
        {
            _accountUserService = accountUserService;
            this.sendingEmailsService = sendingEmailsService;
            this.notifier = notifier;
            _IMenuService = IMenuService;

        }

        // GET: Login
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        [SetNoCacheHttpHeaders(Enabled = false)]
        public ActionResult LoginOn()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        [SetNoCacheHttpHeaders(Enabled = false)]
        public async Task<JsonResult> LoginOn(LoginViewModel model)
        {
            try
            {
                var user = await _accountUserService.GetUserByIdForLogin(model.Usuario);
                if (user == null)
                {
                    return Json(new { type = "LoginFailed", message = "" }, JsonRequestBehavior.AllowGet);
                }
                if (!user.EmailConfirmed)
                {
                    return Json(new { type = "EmailConfirm", message = "" }, JsonRequestBehavior.AllowGet);
                }
                if (!user.Active)
                {
                    return Json(new { type = "UserInactive", message = "" }, JsonRequestBehavior.AllowGet);
                }
                var validUser = await _accountUserService.SignInUser(user.UserName, model.Password);
                if (validUser == SignInStatusUser.Success)
                {
                    var objMenuViewModel = new MenuViewModel();
                    
                    SqlParameter[] parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("Id", user.Id);

                    var responce = _IMenuService.GetXmlDocument("SP_GetMenuXML @UserId = @Id", parameters);

                    objMenuViewModel.MenuItems = SerializeConfig<MenuDto>.Deserialize(responce.OuterXml, "Menus");

                    string lstRolesId = string.Empty;

                    objMenuViewModel.MenuItems[0].Roles.ForEach(x =>
                    {
                        lstRolesId += x.RoleId + ",";
                    });

                    lstRolesId = lstRolesId.Remove(lstRolesId.Length - 1);

                    Session["Menu"] = objMenuViewModel;


                    var userIdentity = await _accountUserService.CreateUserIdentity(user);

                    userIdentity.AddClaim(new Claim("Roles", lstRolesId));

                    var authenticationManager = HttpContext.GetOwinContext().Authentication;

					authenticationManager.AuthenticationResponseGrant = new AuthenticationResponseGrant(
						new ClaimsPrincipal(userIdentity),
						new AuthenticationProperties() { IsPersistent = true });


					//HttpContext.GetOwinContext().Authentication.SignIn(
					//    new AuthenticationProperties() { IsPersistent = true },
					//    userIdentity);

				}
                else
                {
                    return Json(new { type = "LoginFailed", message = "" }, JsonRequestBehavior.AllowGet);
                }

                return Json("/Home/Index");
            }
            catch (Exception ex)
            {
                return Json(new { type = "Exception", message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = await _accountUserService.GetUserByIdForLogin(model.Email);
                    if (user == null)
                    {
                        notifier.Error("Error!!", "Datos no encontrados, verifica que se ingrese correctamente un correo valido.");
                        return View();
                    }
                    var emailToken = await _accountUserService.GeneratePasswordResetToken(user.Id);
                    var resultUrl = Url.Action("ResetPassword", "Login", new { userId = user.Id, code = emailToken }, Request.Url.Scheme);
                    await sendingEmailsService.ForgotPasswordEmailService(model.Email, string.Join(" ", user.Name, user.FirstName, user.LastName), resultUrl, EmailType.ForgotPassword);
                    return RedirectToAction("ForgotPasswordConfirmation", "Login");
                }
                return View(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }
        public async Task<ActionResult> ResetPassword(int? userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }

            if (await _accountUserService.VerifyUserToken((int)userId, code))
            {
                var resetPasswordModel = new ResetViewModel
                {
                    UserId = (int)userId,
                    code = code,
                };
                return View(resetPasswordModel);
            }
            // If the password request is invalid, returns a view informing the user
            return View("ResetPasswordInvalid");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            try
            {
                var result = await _accountUserService.ResetPassword(model.UserId, model.code, model.NewPassword);

                if (result.Succeeded)
                {
                    return RedirectToAction("ResetPasswordSucceeded", "Login");
                }
                return View("ResetPasswordInvalid");
            }
            catch (Exception ex)
            {

                return View("Error", new HandleErrorInfo(ex, "Login", "ResetPassword"));
            }
        }

        public ActionResult ResetPasswordSucceeded()
        {
            return View();
        }
        public ActionResult ResetPasswordInvalid()
        {
            return View();
        }
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(int? userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await _accountUserService.EmailConfirmn((int)userId, code);
            if (result.Succeeded)
            {
                var emailToken = await _accountUserService.GeneratePasswordResetToken((int)userId);
                var resultUrl = Url.Action("ResetPassword", "Login", new { userId = (int)userId, code = emailToken }, Request.Url.Scheme);
                ViewBag.UrlResetPassword = resultUrl;
            }
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }
        [Authorize]
        public ActionResult LogOff()
        {
            var authenticationManager = HttpContext.GetOwinContext().Authentication;
            authenticationManager.SignOut();
            var ctx = Request.GetOwinContext();
            ctx.Response.Cookies.Delete("__RequestVerificationToken");
            AuthenticationManager.SignOut();
            HttpContext.User = null;
            string[] myCookies = Request.Cookies.AllKeys;
            foreach (string cookie in myCookies)
            {
                Response.Cookies[cookie].Expires = DateTime.Now.AddDays(-1);
            }
            return RedirectToAction("LoginOn", "Login");
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult ChangePassword(string user)
        {
            var model = new ChangePasswordViewModel()
            {
                UserName = user
            };

            return View(model);
        }
    }
}
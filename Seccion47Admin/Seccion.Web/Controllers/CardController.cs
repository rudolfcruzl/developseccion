﻿using AutoMapper;
using Seccion.Data.ResposeDto;
using Seccion.Model.Enums;
using Seccion.Model.Models;
using Seccion.Service.Common.Message;
using Seccion.Service.ServiceInterface;
using Seccion.Web.Models.GridListModel;
using Seccion.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Seccion.Web.Controllers
{
    [Authorize]
    public class CardController : Controller
    {
        private readonly IAccountUserService _accountUserService;
        private readonly ICardService _cardService;
        private readonly INotifier _notifier;

        public CardController(IAccountUserService accountUserService, ICardService cardService, INotifier notifier)
        {
            _accountUserService = accountUserService;
            _cardService = cardService;
            _notifier = notifier;
        }

        // GET: Card
        public async Task<ActionResult> Index()
        {
            return View();
        }

        public async Task<ActionResult> GetListNotifications(DatatableServerSideModel param)
        {
            // Falta el parametro de usuario
            SqlParameter[] parameters = new SqlParameter[6];
            parameters[0] = new SqlParameter("@DisplayLength", param.length);
            parameters[1] = new SqlParameter("@DisplayStart", param.start);
            parameters[2] = new SqlParameter("@SortCol", param.order[0].column);
            parameters[3] = new SqlParameter("@SortDir", param.order[0].dir);
            parameters[4] = new SqlParameter("@Search", param.search.value);
            parameters[5] = new SqlParameter("@UserName", User.Identity.Name);

            var employeeList = await _cardService.GetAllNotifications(parameters);
            var model = Mapper.Map<IEnumerable<SP_GetNotificationDto>, IEnumerable<ListNotificationViewModel>>(employeeList);
            int recordsTotal = (model.Any() ? model.FirstOrDefault().TotalRequest : 0);
            int filterCount = (model.Any() ? model.FirstOrDefault().TotalFilter : 0);

            return Json(new
            {
                draw = Convert.ToInt32(param.draw),
                recordsTotal = recordsTotal,
                recordsFiltered = filterCount,
                data = model
            }, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> CreateCard()
        {
            var listUser = await _accountUserService.GetUserList();

            ViewBag.Users = listUser.OrderBy(x => x.FirstName).Select(s => new SelectListItem { Text = $"{s.Name} {s.FirstName} {s.LastName}", Value = s.Id.ToString() });

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateCard(CardViewModel model)
        {
            try
            {
                var card = Mapper.Map<CardViewModel, TbCard>(model);
                card.UserInsert = User.Identity.Name;
                card.DateInsert = DateTime.Now;

                _cardService.CreateCard(card);
                await _cardService.Save();

                if (card.TbCardId == 0)
                    return View("Error", new HandleErrorInfo(new Exception(), "Card", "Index"));

                var response = await _cardService.CreateFilesCard(card, model.ImportFiles);

                if (!response)
                {
                    _cardService.DeleteCard(card);

                    return View("Error", new HandleErrorInfo(new Exception(), "Card", "Index"));
                }

                var user = await _accountUserService.GetUserByIdForLogin(User.Identity.Name);

                if (user == null)
                    return View("Error", new HandleErrorInfo(new Exception(), "Card", "Index"));

                foreach (var userId in model.Users)
                {
                    var notification = new TbNotification()
                    {
                        TbCardId = card.TbCardId,
                        UserId = userId,
                        Status = StatusNotification.ToDo,
                        UserInsert = User.Identity.Name,
                        DateInsert = DateTime.Now
                    };

                    _cardService.CreateNotification(notification);
                }

                await _cardService.Save();
                _notifier.Success("Éxito!!", "Se creo la nueva asignación.");


                return RedirectToAction("Index", "Home");
            }
            catch (Exception ex)
            {
                return View("Error", new HandleErrorInfo(ex, "Card", "Index"));
            }
        }

        [HttpGet]
        public async Task<ActionResult> EditCard(int id)
        {
            var notification = _cardService.GetNotificationWithCard(id);

            var model = Mapper.Map<TbCard, EditCardViewModel>(notification.Card);
            model.TbNotificationId = notification.TbNotificationId;
            model.Status = notification.Status;
            model.Comment = notification.Comment;
            model.ImportFiles = notification.Card.Files;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditCard(EditCardViewModel model)
        {
            try
            {
                var notifications = await _cardService.GetNotificationsInCard(model.TbCardId);

                if (notifications.Any(r => r.Status == StatusNotification.Done))
                {
                    return View("Error", new HandleErrorInfo(new Exception(), "Card", "Index"));
                }

                foreach (var notification in notifications)
                {
                    if (notification.TbNotificationId == model.TbNotificationId)
                    {
                        notification.Comment = model.Comment;
                    }

                    notification.Status = model.Status;
                    notification.UserUpdate = User.Identity.Name;
                    notification.DateUpdate = DateTime.Now;

                    _cardService.UpdateNotification(notification);
                }

                await _cardService.Save();

                return RedirectToAction("Index", "Card");
            }
            catch (Exception ex)
            {
                return View("Error", new HandleErrorInfo(ex, "Card", "Index"));
            }
        }

        public async Task<ActionResult> DownloadFile(string blobName)
        {
            MemoryStream memoryStream = new MemoryStream();

            if (string.IsNullOrEmpty(blobName))
            {
                return View("Error", new HandleErrorInfo(new Exception(), "Card", "Index"));
            }

            await _cardService.DownloadFile(memoryStream, blobName);

            return File(memoryStream.ToArray(), System.Net.Mime.MediaTypeNames.Application.Octet, blobName);
        }
    }
}
﻿using Seccion.Data.RepositoriesSP;
using Seccion.Service.ServiceInterface;
using Seccion.Web.Models.GridListModel;
using Seccion.Web.Models.ViewModels;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Seccion.Web.Controllers
{
    public class CommonController : Controller
    {
        private readonly IEmployeeService employeeService;
        private readonly IAccountUserService _IAccountUserService;
        private readonly IMenuService _IMenuService;

        public CommonController(IEmployeeService employeeService, IAccountUserService IAccountUserService, IMenuService IMenuService, ISP_GetMenuRepository ISP_GetMenuRepository)
        {
            this.employeeService = employeeService;
            _IAccountUserService = IAccountUserService;
            _IMenuService = IMenuService;
        }
        // GET: Common
        public ActionResult Index()
        {
            return View();
        }
        public PartialViewResult DataTableMenu(MenuIdModel model)
        {
            return PartialView("_TableMenu", model);
        }
        [HttpGet]
        public async Task<JsonResult> GetEmployeeAval()
        {
            var listEmployee = await employeeService.GetEmployeeData();
            return Json(listEmployee, JsonRequestBehavior.AllowGet);
        }


        public ActionResult RenderMenu()
        {
            return PartialView("_LayoutMenu", Session["Menu"] as MenuViewModel);
        }

    }
}
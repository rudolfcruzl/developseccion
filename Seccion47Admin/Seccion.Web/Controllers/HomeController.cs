﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using Seccion.Model.Models;
using Seccion.Service.ServiceInterface;
using Seccion.Web.Models.ViewModels;

namespace Seccion.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ICardService _cardService;

        public HomeController(ICardService cardService)
        {
            _cardService = cardService;
        }

        public ActionResult Index()
        {
            var claimUser = User.Identity as ClaimsIdentity;
            var test = claimUser.Claims.SingleOrDefault(x => x.Type == "Test");
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public JsonResult GetNotifications()
        {
            var notifications = _cardService.GetNewNotificationByUserName(User.Identity.Name);

            var data = Mapper.Map<IEnumerable<TbNotification>, IEnumerable<NewNotificationViewModel>>(notifications);
            data.OrderByDescending(r => r.DateInsert);

            return Json(new
            {
                newNotification = notifications.Count(),
                data = data

            }, JsonRequestBehavior.AllowGet);
        }
    }
}
﻿using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;

namespace Seccion.Web.Hubs
{
    public class NotificationHub : Hub
    {
        public async Task GetNotificationByUser()
        {

        }

        public override Task OnConnected()
        {
            Clients.Client(Context.ConnectionId);

            return base.OnConnected();
        }
    }
}
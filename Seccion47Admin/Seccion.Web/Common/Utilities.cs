﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Seccion.Web.Common
{
    public class Utilities
    {
        protected internal static object ChangeType(object value, Type conversion)
        {
            var t = conversion;

            if (t.IsGenericType && t.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                if (value == null)
                {
                    return null;
                }

                t = Nullable.GetUnderlyingType(t);
            }

            return Convert.ChangeType(value, t);
        }

        protected internal static bool ValidateDate(string date)
        {
            DateTime _date;

            if (!string.IsNullOrEmpty(date))
            {
                if (DateTime.TryParseExact(date, "dd/mm/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal, out _date))
                {
                    if (_date >= SqlDateTime.MinValue.Value && _date <= SqlDateTime.MaxValue.Value)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace Seccion.Web.Common
{
    public static class SerializeConfig<T> where T : class
    {
        public static List<T> Deserialize (string input, string root) 
        {
            XmlSerializer ser = new XmlSerializer(typeof(List<T>), new XmlRootAttribute(root));

            using (StringReader sr = new StringReader(input))
            {
                return (List<T>)ser.Deserialize(sr);
            }
        }

        public static string Serialize(T ObjectToSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(ObjectToSerialize.GetType());

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, ObjectToSerialize);
                return textWriter.ToString();
            }
        }
    }
}
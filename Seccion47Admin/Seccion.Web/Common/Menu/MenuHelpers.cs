﻿using Seccion.Model.IdentityModel;
using Seccion.Web.Models.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WebGrease.Css.Extensions;

namespace Seccion.Web.Common.Menu
{
    public static class MenuHelpers
    {

        public static IHtmlString CreateMenu(this HtmlHelper helper, IEnumerable<MenuDto> lstMenuItem)
        {
            var sb = new StringBuilder();

            if (lstMenuItem == null || !lstMenuItem.Any())
            {
                return new MvcHtmlString(string.Empty);
            }

            lstMenuItem.ForEach(e => CreateMenu(e, sb));
            return sb.ToHtmlString();
        }

        private static MvcHtmlString ToHtmlString(this StringBuilder input)
        {
            return MvcHtmlString.Create(input.ToString());
        }

        private static void CreateMenu(MenuDto menuItem, StringBuilder sb)
        {
            if (menuItem.Children.Any())
            {
                var liTag = new TagBuilder("li");
                liTag.AddCssClass("nav-item");
                var aTag = new TagBuilder("a");
                aTag.AddCssClass("nav-link dropdown-toggle");
                aTag.MergeAttribute("href", "#");

                var iTag = new TagBuilder("i");
                iTag.AddCssClass($"nav-icon {menuItem.Icon}");
                iTag.MergeAttribute("aria-hidden", "true");
                var spanTag = new TagBuilder("span");
                spanTag.AddCssClass("nav-text fadeable");
                spanTag.InnerHtml = menuItem.Title;

                var b = new TagBuilder("b");
                b.AddCssClass("caret fa fa-angle-left rt-n90");

                var SubArrow = new TagBuilder("b");
                SubArrow.AddCssClass("sub-arrow");

                sb.Append(liTag.ToString(TagRenderMode.StartTag));
                sb.Append(aTag.ToString(TagRenderMode.StartTag));
                sb.Append(iTag.ToString(TagRenderMode.Normal));
                sb.Append(spanTag.ToString(TagRenderMode.Normal));
                sb.Append(b.ToString(TagRenderMode.Normal));
                sb.Append(aTag.ToString(TagRenderMode.EndTag));

                var div = new TagBuilder("div");
                div.AddCssClass("hideable submenu collapse");
                var ul = new TagBuilder("ul");
                ul.AddCssClass("submenu-inner");

                sb.Append(div.ToString(TagRenderMode.StartTag));
                sb.Append(ul.ToString(TagRenderMode.StartTag));

                CreateNodeWithSubNodes(menuItem, sb, 0);

                sb.Append(ul.ToString(TagRenderMode.EndTag));
                sb.Append(div.ToString(TagRenderMode.EndTag));
                sb.Append(SubArrow.ToString(TagRenderMode.Normal));
                sb.Append(liTag.ToString(TagRenderMode.EndTag));
            }
            else
            {
                CreateSingleNode(menuItem, sb);
            }
        }

        private static void CreateSingleNode(MenuDto menuItem, StringBuilder sb)
        {
            var liTag = new TagBuilder("li");
            liTag.AddCssClass("nav-item");
            var aTag = new TagBuilder("a");
            if (string.IsNullOrEmpty(menuItem.Url))
            {
                aTag.AddCssClass("nav-link dropdown-toggle");
                aTag.MergeAttribute("href", "#");
            }
            else
            {
                aTag.AddCssClass("nav-link");
                aTag.MergeAttribute("href", menuItem.Url);
            }

            var iTag = new TagBuilder("i");
            iTag.AddCssClass($"nav-icon {menuItem.Icon}");
            iTag.MergeAttribute("aria-hidden", "true");
            var spanTag = new TagBuilder("span");
            spanTag.AddCssClass("nav-text fadeable");
            spanTag.InnerHtml = menuItem.Title;

            var SubArrow = new TagBuilder("b");
            SubArrow.AddCssClass("sub-arrow");

            sb.Append(liTag.ToString(TagRenderMode.StartTag));
            sb.Append(aTag.ToString(TagRenderMode.StartTag));
            sb.Append(iTag.ToString(TagRenderMode.Normal));
            sb.Append(spanTag.ToString(TagRenderMode.Normal));
            sb.Append(aTag.ToString(TagRenderMode.EndTag));
            sb.Append(SubArrow.ToString(TagRenderMode.Normal));
            sb.Append(liTag.ToString(TagRenderMode.EndTag));

        }

        private static void CreateNodeWithSubNodes(MenuDto menuItem, StringBuilder sb, int index)
        {
            var DataNode = menuItem.Children.ToList();
            if (index < menuItem.Children.Count)
            {
                var liTag = new TagBuilder("li");
                liTag.AddCssClass("nav-item");
                var aTag = new TagBuilder("a");
                if (DataNode[index].Children.Any())
                {
                    aTag.AddCssClass("nav-link dropdown-toggle");
                    aTag.MergeAttribute("href", "#");
                }
                else
                {
                    aTag.AddCssClass("nav-link");
                    aTag.MergeAttribute("href", DataNode[index].Url);
                }

                var iTag = new TagBuilder("i");
                iTag.AddCssClass($"nav-icon {DataNode[index].Icon}");
                iTag.MergeAttribute("aria-hidden", "true");
                var spanTag = new TagBuilder("span");
                spanTag.AddCssClass("nav-text fadeable");
                spanTag.InnerHtml = DataNode[index].Title;

                var b = new TagBuilder("b");
                b.AddCssClass("caret fa fa-angle-left rt-n90");

                sb.Append(liTag.ToString(TagRenderMode.StartTag));
                sb.Append(aTag.ToString(TagRenderMode.StartTag));
                sb.Append(iTag.ToString(TagRenderMode.Normal));
                sb.Append(spanTag.ToString(TagRenderMode.Normal));
                if (DataNode[index].Children.Any())
                {
                    sb.Append(b.ToString(TagRenderMode.Normal));
                }
                sb.Append(aTag.ToString(TagRenderMode.EndTag));

                var div = new TagBuilder("div");
                div.AddCssClass("hideable submenu collapse");
                var ul = new TagBuilder("ul");
                ul.AddCssClass("submenu-inner");

                sb.Append(div.ToString(TagRenderMode.StartTag));
                sb.Append(ul.ToString(TagRenderMode.StartTag));

                if (DataNode[index].Children.Any())
                {
                    createNode(DataNode[index], sb, 0);
                }

                sb.Append(ul.ToString(TagRenderMode.EndTag));
                sb.Append(div.ToString(TagRenderMode.EndTag));
                sb.Append(liTag.ToString(TagRenderMode.EndTag));
                CreateNodeWithSubNodes(menuItem, sb, index + 1);
            }
        }

        private static void createNode(MenuDto menuItem, StringBuilder sb, int index)
        {
            var DataNode = menuItem.Children.ToList();
            if (index < menuItem.Children.Count)
            {
                var li = new TagBuilder("li");
                li.AddCssClass("nav-item");
                var a = new TagBuilder("a");
                a.AddCssClass("nav-link");
                a.MergeAttribute("href", DataNode[index].Url);
                var i = new TagBuilder("i");
                i.AddCssClass($"nav-icon {DataNode[index].Icon}");
                i.MergeAttribute("aria-hidden", "true");
                var span = new TagBuilder("span");
                span.AddCssClass("nav-text");
                span.InnerHtml = DataNode[index].Title;

                sb.Append(li.ToString(TagRenderMode.StartTag));
                sb.Append(a.ToString(TagRenderMode.StartTag));
                sb.Append(i.ToString(TagRenderMode.Normal));
                sb.Append(span.ToString(TagRenderMode.Normal));
                sb.Append(a.ToString(TagRenderMode.EndTag));
                sb.Append(li.ToString(TagRenderMode.EndTag));
                createNode(menuItem, sb, index + 1);
            }
        }



    }
}
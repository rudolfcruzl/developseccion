﻿using System.Web.Optimization;

namespace Seccion.Web
{
	public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/loginLayoutJs").Include(
            "~/Scripts/jquery-3.4.1.min.js",
            "~/Scripts/bootstrap.min.js",
            "~/assetsAce/node_modules/js/plugins/loaders/blockui.min.js",
            "~/assetsAce/node_modules/dist/js/ace.js",
            "~/assetsAce/js/page.login/page-login-script.js",
            "~/Scripts/jquery.unobtrusive-ajax.min.js",
            "~/assetsAce/node_modules/js/plugins/loadingoverlay/loadingoverlay.min.js",
            "~/assetsAce/node_modules/js/plugins/notify/PNotify.js",
            "~/assetsAce/node_modules/js/plugins/notify/PNotifyStyleMaterial.js",
            "~/assetsAce/node_modules/js/plugins/notify/PNotifyButtons.js",
            "~/assetsAce/node_modules/js/plugins/notify/PNotifyAnimate.js",
            "~/Scripts/Forms/loginLoading.js"));

            bundles.Add(new ScriptBundle("~/bundles/layoutMasterJs").Include(
                
                "~/assetsAce/node_modules/js/plugins/notify/PNotify.js",
                "~/assetsAce/node_modules/js/plugins/notify/PNotifyStyleMaterial.js",
                "~/assetsAce/node_modules/js/plugins/notify/PNotifyButtons.js",
                "~/assetsAce/node_modules/js/plugins/notify/PNotifyAnimate.js",
                "~/Scripts/jquery.unobtrusive-ajax.min.js",
                "~/Scripts/jquery.validate-vsdoc.js",
                "~/Scripts/jquery.validate.min.js",
                "~/Scripts/jquery.validate.unobtrusive.min.js",
                "~/assetsAce/node_modules/js/plugins/loadingoverlay/loadingoverlay.min.js"));

                bundles.Add(new ScriptBundle("~/bundles/employeeJs").Include(
                "~/assetsAce/node_modules/js/plugins/datatable/datatables.min.js",
                "~/assetsAce/node_modules/js/plugins/datatable/responsive/responsive.bootstrap4.min.js",
                "~/assetsAce/node_modules/js/plugins/chosen-js/chosen.jquery.js",
                "~/assetsAce/node_modules/js/plugins/sweetalert/sweetalert2.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/listRequestJs").Include(
                "~/assetsAce/node_modules/js/plugins/datatable/jquery.dataTables.min.js",
                "~/assetsAce/node_modules/js/plugins/datatable/dataTables.bootstrap4.min.js",
                "~/assetsAce/node_modules/js/plugins/datatable/button/dataTables.buttons.min.js",
                "~/assetsAce/node_modules/js/plugins/datatable/button/buttons.bootstrap4.min.js",
                "~/assetsAce/node_modules/js/plugins/datatable/button/jszip.min.js",
                "~/assetsAce/node_modules/js/plugins/datatable/button/pdfmake.min.js",
                "~/assetsAce/node_modules/js/plugins/datatable/button/vfs_fonts.js",
                "~/assetsAce/node_modules/js/plugins/datatable/button/buttons.html5.js",
                "~/assetsAce/node_modules/js/plugins/datatable/dataTables.responsive.min.js",
                "~/assetsAce/node_modules/js/plugins/chosen-js/chosen.jquery.min.js",
                "~/assetsAce/node_modules/js/plugins/tiny-date-picker/date-range-picker.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/RequestJs").Include(
                        "~/assetsAce/node_modules/js/plugins/chosen-js/chosen.jquery.js",
                        "~/assetsAce/node_modules/js/plugins/formatCurrency/jquery.formatCurrency-1.4.0.min.js",
                        "~/assetsAce/node_modules/js/plugins/inputmask/jquery.inputmask.min.js",
                        "~/assetsAce/node_modules/js/plugins/inputmask/inputmask.binding.js",
                        "~/assetsAce/node_modules/js/plugins/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive-ajax.min",
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));
            bundles.Add(new StyleBundle("~/Ace/loginLayoutCss").Include(
                      "~/Content/bootstrap.min.css",
                      "~/assetsAce/node_modules/fortawesome/fontawesome-free/css/fontawesome.css",                      
                      "~/assetsAce/node_modules/dist/css/ace.min.css",
                      "~/assetsAce/css/login/page-login-style.css"
                      ).Include("~/assetsAce/node_modules/fortawesome/fontawesome-free/css/solid.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/bundles/employeeCss").Include(
                      "~/assetsAce/node_modules/js/plugins/datatable/datatables.min.css",
                      "~/assetsAce/node_modules/js/plugins/datatable/responsive/responsive.bootstrap4.min.css",
                      "~/assetsAce/node_modules/js/plugins/datatable/select/select.bootstrap4.min.css",
                      "~/assetsAce/node_modules/js/plugins/chosen-js/chosen.css",
                      "~/assetsAce/node_modules/js/plugins/datatable/css.custom.datatable.css",
                      "~/assetsAce/node_modules/js/plugins/sweetalert/sweetalert2.min.css"));

            bundles.Add(new StyleBundle("~/bundles/listRequestCss").Include(
                      "~/assetsAce/node_modules/js/plugins/datatable/dataTables.bootstrap4.min.css",
                      "~/assetsAce/node_modules/js/plugins/datatable/button/buttons.bootstrap4.min.css",
                      "~/assetsAce/node_modules/js/plugins/datatable/responsive.bootstrap4.min.css",
                      "~/assetsAce/node_modules/js/plugins/chosen-js/chosen.min.css",
                      "~/assetsAce/node_modules/js/plugins/tiny-date-picker/tiny-date-picker.css",
                      "~/assetsAce/node_modules/js/plugins/tiny-date-picker/date-range-picker.css",
                      "~/assetsAce/node_modules/js/plugins/datatable/css.custom.datatable.css"));
            BundleTable.EnableOptimizations = true;
        }
    }
}

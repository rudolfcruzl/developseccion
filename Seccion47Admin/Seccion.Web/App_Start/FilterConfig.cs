﻿using NWebsec.Mvc.HttpHeaders;
using System.Web.Mvc;

namespace Seccion.Web
{
    public class FilterConfig
    {
		protected FilterConfig()
		{
		}
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new SetNoCacheHttpHeadersAttribute());
        }
    }
}
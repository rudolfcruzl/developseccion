﻿

function GridDataModel(IdTable, catalog, url, titleModal) {
    //var $tableByClass ='.' + IdTable
    $.ajax({
        type: "GET",
        url: "/" + lang + "/Catalogs/FillDatTableModel",
        data: { typeCat: catalog },
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    }).done(function (response) {
        //var data = $.parseJSON(response)
       $(IdTable).dataTable({
            "bDestroy": true,
            "aaData": response,
            responsive: {
                details: {
                    type: 'column',
                    target: 'tr'
                }
            },
            order: [1, 'asc'],
            columnDefs: [
                {
                    className: 'control',
                    orderable: false,
                    targets: 0,
                    data: null,
                    'defaultContent': ''
                },
                {
                    //title: "Prueba",
                    targets: 1,
                    orderable: true,
                    data: "Description"
                },
                {
                    targets: 2,
                    orderable: true,
                    data: "Active",
                    'mRender': function (param, type, full) {

                        return param ? "<i class='icon-checkmark-circle icon-1x text-success'>" : "<i class='icon-1x text-danger icon-cancel-circle2'>";
                    }
                },
                {
                    targets: 3,
                    className: "text-center",
                    orderable: false,
                    data: "ModelID",
                    'mRender': function (param, type, full) {

                        //var url = "@Url.Action("CreateModel", "Catalogs")";
                        //var menu = "<button type="button" class="btn btn-primary btn-icon btn-rounded legitRipple"><i class="icon-menu7"></i></button>"
                        var menu = "<a title='Editar Modelo' class='btn bg-color-nissan btn-icon btn-rounded legitRipple' href='javascript:GetModalViewPartial(\"" + "/" + lang + url + "\",\"" + param + "\",\"" + catalog + "\",\"" + titleModal + "\")'><i class='icon-pencil'></i></a>";


                        return menu;
                    }

                }
            ]
        });
        // Enable Select2 select for the length option
        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            dropdownAutoWidth: true,
            width: 'auto'
        });
    }).fail(function (jqXHR, textStatus, err) {
        //MessageError(jqXHR, textStatus, err, Title, "LnkMessage");
        alert(err);
    });

}


function GridDataColor(IdTable, catalog, url) {
    //var $tableByClass ='.' + IdTable
    //var lang = MultiLanguageDemo.Cookies.getCookie("LangForMultiLanguageDemo");
    $.ajax({
        type: "GET",
        url: "/" + lang + "/Catalogs/FillDatTableModel",
        data: { typeCat: catalog },
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    }).done(function (response) {
        //var data = $.parseJSON(response)
        oTable = $(IdTable).dataTable({
            "bDestroy": true,
            "aaData": response,
            responsive: {
                details: {
                    type: 'column',
                    target: 'tr'
                }
            },
            order: [1, 'asc'],
            columnDefs: [
                {
                    className: 'control',
                    orderable: false,
                    targets: 0,
                    data: null,
                    'defaultContent': ''
                },
                {
                    //title: "Prueba",
                    targets: 1,
                    orderable: true,
                    data: "Description"
                },
                {
                    targets: 2,
                    className: "text-center",
                    orderable: false,
                    data: "ModelID",
                    'mRender': function (param, type, full) {

                        //var url = "@Url.Action("CreateModel", "Catalogs")";
                        //var menu = "<button type="button" class="btn btn-primary btn-icon btn-rounded legitRipple"><i class="icon-menu7"></i></button>"
                        var menu = "<a title='Editar Modelo' class='btn bg-color-nissan btn-icon btn-rounded legitRipple' href='javascript:GetModalViewPartial(\"" + "/" + lang + url + "\",\"" + param + "\",\"" + catalog + "\",\"Editar Cor\")'><i class='icon-pencil'></i></a>";


                        return menu;
                    }

                }
            ]
        });
        // Enable Select2 select for the length option
        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            dropdownAutoWidth: true,
            width: 'auto'
        });
    }).fail(function (jqXHR, textStatus, err) {
        //MessageError(jqXHR, textStatus, err, Title, "LnkMessage");
        alert(err);
    });


}

function GridModel(IdTable, catalog, url) {
    //var $tableByClass ='.' + IdTable
    $.ajax({
        type: "GET",
        url: "/" + lang + "/Catalogs/FillDatTableModel",
        data: { typeCat: catalog },
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    }).done(function (response) {
        //var data = $.parseJSON(response)
        oTable = $(IdTable).dataTable({
            "bDestroy": true,
            "aaData": response,
            responsive: {
                details: {
                    type: 'column',
                    target: 'tr'
                }
            },
            order: [1, 'asc'],
            //drawCallback: function () {
            //	$('[data-popup="popover"]').popover({
            //		template: '<div class="popover border-teal-400"><div class="arrow"></div><h3 class="popover-title bg-teal-400"></h3><div class="popover-content"></div></div>'
            //	});
            //},
            columnDefs: [
                {
                    className: 'control',
                    orderable: false,
                    targets: 0,
                    data: null,
                    'defaultContent': ''
                },
                {
                    //title: "Prueba",
                    targets: 1,
                    orderable: true,
                    data: "Description"
                },
                //{
                //	targets: 2,
                //	orderable: true,
                //	data: "GeneralName"
                //},
                {
                    targets: 2,
                    orderable: true,
                    data: "Active",
                    'mRender': function (param, type, full) {

                        return param ? "<i class='icon-checkmark-circle icon-1x text-success'>" : "<i class='icon-1x text-danger icon-cancel-circle2'>";
                    }
                },
                {
                    targets: 3,
                    className: "text-center",
                    orderable: false,
                    data: "ModelID",
                    'mRender': function (param, type, full) {

                        //var url = "@Url.Action("CreateModel", "Catalogs")";
                        //var menu = "<button type="button" class="btn btn-primary btn-icon btn-rounded legitRipple"><i class="icon-menu7"></i></button>"
                        var menu = "<a title='Editar Modelo' class='btn bg-color-nissan btn-icon btn-rounded legitRipple' href='javascript:GetModalViewPartial(\"" + "/" + lang + url + "\",\"" + param + "\",\"" + catalog + "\",\"Editar Modelo\")'><i class='icon-pencil'></i></a>";


                        return menu;
                    }

                }
            ]
        });
        // Enable Select2 select for the length option
        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            dropdownAutoWidth: true,
            width: 'auto'
        });
    }).fail(function (jqXHR, textStatus, err) {
        //MessageError(jqXHR, textStatus, err, Title, "LnkMessage");
        alert(err);
    });

}



function GridDataLocation(IdTable, catalog, url, titleModal) {
    //var $tableByClass ='.' + IdTable
    $.ajax({
        type: "GET",
        url: "/" + lang + "/Catalogs/FillDatTableModel",
        data: { typeCat: catalog },
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    }).done(function (response) {
        //var data = $.parseJSON(response)
        oTable = $(IdTable).dataTable({
            "bDestroy": true,
            "aaData": response,
            responsive: {
                details: {
                    type: 'column',
                    target: 'tr'
                }
            },
            order: [1, 'asc'],
            columnDefs: [
                {
                    className: 'control',
                    orderable: false,
                    targets: 0,
                    data: null,
                    'defaultContent': ''
                },
                {
                    //title: "Prueba",
                    targets: 1,
                    orderable: true,
                    data: "Description",
                    'mRender': function (param, type, full) {
                        return param;
                    }
                },
                {
                    //title: "Prueba",
                    targets: 2,
                    orderable: true,
                    data: "GeneralName"
                },
                {
                    targets: 3,
                    orderable: true,
                    data: "Active",
                    'mRender': function (param, type, full) {

                        return param ? "<i class='icon-checkmark-circle icon-1x text-success'>" : "<i class='icon-1x text-danger icon-cancel-circle2'>";
                    }
                },
                {
                    //title: "Area RH"

                    targets: 4,
                    orderable: true,
                    data: "Area RH",

                    'mRender': function (param, type, full) {
                        return full.RH ? "<i class='icon-checkmark-circle icon-1x text-success'>" : "";
                    }
                },
                {
                    targets: 5,
                    className: "text-center",
                    orderable: false,
                    data: "ModelID",
                    'mRender': function (param, type, full) {

                        //var url = "@Url.Action("CreateModel", "Catalogs")";
                        //var menu = "<button type="button" class="btn btn-primary btn-icon btn-rounded legitRipple"><i class="icon-menu7"></i></button>"
                        var menu = "<a title='Editar Modelo' class='btn bg-color-nissan btn-icon btn-rounded legitRipple' href='javascript:GetModalViewPartial(\"" + "/" + lang + url + "\",\"" + param + "\",\"" + catalog + "\",\"" + titleModal + "\")'><i class='icon-pencil'></i></a>";


                        return menu;
                    }
                }
            ]
        });
        // Enable Select2 select for the length option
        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            dropdownAutoWidth: true,
            width: 'auto'
        });
    }).fail(function (jqXHR, textStatus, err) {
        //MessageError(jqXHR, textStatus, err, Title, "LnkMessage");
        alert(err);
    });

}

function GridVoudeNissanCC(IdTable, catalog, url, titleModal) {
    //var $tableByClass ='.' + IdTable
    $.ajax({
        type: "GET",
        url: "/" + lang + "/Catalogs/FillDatTableModel",
        data: { typeCat: catalog },
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    }).done(function (response) {
        //var data = $.parseJSON(response)
        oTable = $(IdTable).dataTable({
            "bDestroy": true,
            "aaData": response,
            responsive: {
                details: {
                    type: 'column',
                    target: 'tr'
                }
            },
            order: [1, 'asc'],
            columnDefs: [
                {
                    className: 'control',
                    orderable: false,
                    targets: 0,
                    data: null,
                    'defaultContent': ''
                },
                {
                    //title: "Prueba",
                    targets: 1,
                    orderable: true,
                    data: "Description"
                },
                //{
                //    //title: "Prueba",
                //    targets: 2,
                //    orderable: true,
                //    data: "GeneralName"
                //},
                {
                    targets: 2,
                    orderable: true,
                    data: "Active",
                    'mRender': function (param, type, full) {

                        return param ? "<i class='icon-checkmark-circle icon-1x text-success'>" : "<i class='icon-1x text-danger icon-cancel-circle2'>";
                    }
                },
                {
                    targets: 3,
                    className: "text-center",
                    orderable: false,
                    data: "ModelID",
                    'mRender': function (param, type, full) {

                        //var url = "@Url.Action("CreateModel", "Catalogs")";
                        //var menu = "<button type="button" class="btn btn-primary btn-icon btn-rounded legitRipple"><i class="icon-menu7"></i></button>"
                        var menu = "<a title='Editar Modelo' class='btn bg-color-nissan btn-icon btn-rounded legitRipple' href='javascript:GetModalViewPartial(\"" + "/" + lang + url + "\",\"" + param + "\",\"" + catalog + "\",\"" + titleModal + "\")'><i class='icon-pencil'></i></a>";


                        return menu;
                    }

                }
            ]
        });
        // Enable Select2 select for the length option
        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            dropdownAutoWidth: true,
            width: 'auto'
        });
    }).fail(function (jqXHR, textStatus, err) {
        //MessageError(jqXHR, textStatus, err, Title, "LnkMessage");
        alert(err);
    });

}

function GridUniqueCC(IdTable, catalog, url, titleModal) {
    //var $tableByClass ='.' + IdTable
    $.ajax({
        type: "GET",
        url: "/" + lang + "/Catalogs/FillDatTableModel",
        data: { typeCat: catalog },
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    }).done(function (response) {
        //var data = $.parseJSON(response)
        oTable = $(IdTable).dataTable({
            "bDestroy": true,
            "aaData": response,
            responsive: {
                details: {
                    type: "column",
                    target: "tr"
                }
            },
            order: [1, 'asc'],
            columnDefs: [
                {
                    className: "control",
                    orderable: false,
                    targets: 0,
                    data: null,
                    'defaultContent': ""
                },
                {
                    targets: 1,
                    orderable: true,
                    data: "Description"
                },
                {
                    targets: 2,
                    orderable: true,
                    data: "Active",
                    'mRender': function (param, type, full) {

                        return param ? "<i class='icon-checkmark-circle icon-1x text-success'>" : "<i class='icon-1x text-danger icon-cancel-circle2'>";
                    }
                },
                {
                    targets: 3,
                    className: "text-center",
                    orderable: false,
                    data: "ModelID",
                    'mRender': function (param, type, full) {

                        //var url = "@Url.Action("CreateModel", "Catalogs")";
                        //var menu = "<button type="button" class="btn btn-primary btn-icon btn-rounded legitRipple"><i class="icon-menu7"></i></button>"
                        var menu = "<a title='Editar Modelo' class='btn bg-color-nissan btn-icon btn-rounded legitRipple' href='javascript:GetModalViewPartial(\"" + "/" + lang + url + "\",\"" + param + "\",\"" + catalog + "\",\"" + titleModal + "\")'><i class='icon-pencil'></i></a>";


                        return menu;
                    }

                }
            ]
        });
        // Enable Select2 select for the length option
        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            dropdownAutoWidth: true,
            width: 'auto'
        });
    }).fail(function (jqXHR, textStatus, err) {
        //MessageError(jqXHR, textStatus, err, Title, "LnkMessage");
        alert(err);
    });

}

function GridUniqueCCRenault(IdTable, catalog, url, titleModal) {
    //var $tableByClass ='.' + IdTable
    $.ajax({
        type: "GET",
        url: "/" + lang + "/Catalogs/FillDatTableModel",
        data: { typeCat: catalog },
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    }).done(function (response) {
        //var data = $.parseJSON(response)
        oTable = $(IdTable).dataTable({
            "bDestroy": true,
            "aaData": response,
            responsive: {
                details: {
                    type: "column",
                    target: "tr"
                }
            },
            order: [3, 'asc'],
            columnDefs: [
                {
                    className: "control",
                    orderable: false,
                    targets: 0,
                    data: null,
                    'defaultContent': ""
                },
                {
                    targets: 1,
                    orderable: true,
                    data: "Description"
                },
                {
                    targets: 2,
                    orderable: true,
                    data: "AlianzaRCI",
                    'mRender': function (param, type, full) {

                        return param ? "<i class='icon-checkmark-circle icon-1x text-success'>" : "";
                    }
                },
                {
                    targets: 3,
                    orderable: true,
                    data: "RCI",
                    'mRender': function (param, type, full) {

                        return param ? "<i class='icon-checkmark-circle icon-1x text-success'>" : "";
                    }
                },
                {
                    targets: 4,
                    orderable: true,
                    data: "Active",
                    'mRender': function (param, type, full) {

                        return param ? "<i class='icon-checkmark-circle icon-1x text-success'>" : "<i class='icon-1x text-danger icon-cancel-circle2'>";
                    }
                },
                {
                    targets: 5,
                    className: "text-center",
                    orderable: false,
                    data: "ModelID",
                    'mRender': function (param, type, full) {

                        //var url = "@Url.Action("CreateModel", "Catalogs")";
                        //var menu = "<button type="button" class="btn btn-primary btn-icon btn-rounded legitRipple"><i class="icon-menu7"></i></button>"
                        var menu = "<a title='Editar Modelo' class='btn bg-color-nissan btn-icon btn-rounded legitRipple' href='javascript:GetModalViewPartial(\"" + "/" + lang + url + "\",\"" + param + "\",\"" + catalog + "\",\"" + titleModal + "\")'><i class='icon-pencil'></i></a>";


                        return menu;
                    }

                }
            ]
        });
        // Enable Select2 select for the length option
        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            dropdownAutoWidth: true,
            width: 'auto'
        });
    }).fail(function (jqXHR, textStatus, err) {
        //MessageError(jqXHR, textStatus, err, Title, "LnkMessage");
        alert(err);
    });

}

function GridDataCostrCenter(IdTable, catalog, url, titleModal) {
    //var $tableByClass ='.' + IdTable
    $.ajax({
        type: "GET",
        url: "/" + lang + "/Catalogs/FillDatTableModel",
        data: { typeCat: catalog },
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    }).done(function (response) {
        //var data = $.parseJSON(response)
        oTable = $(IdTable).dataTable({
            "bDestroy": true,
            "aaData": response,
            responsive: {
                details: {
                    type: 'column',
                    target: 'tr'
                }
            },
            order: [1, 'asc'],
            columnDefs: [
                {
                    className: 'control',
                    orderable: false,
                    targets: 0,
                    data: null,
                    'defaultContent': ''
                },
                {
                    //title: "Prueba",
                    targets: 1,
                    orderable: true,
                    data: "Description"
                },
                {
                    //title: "Prueba",
                    targets: 2,
                    orderable: true,
                    data: "CostCenterNumber"
                },
                {
                    targets: 3,
                    orderable: true,
                    data: "Active",
                    'mRender': function (param, type, full) {

                        return param ? "<i class='icon-checkmark-circle icon-1x text-success'>" : "<i class='icon-1x text-danger icon-cancel-circle2'>";
                    }
                },
                {
                    targets: 4,
                    className: "text-center",
                    orderable: false,
                    data: "ModelID",
                    'mRender': function (param, type, full) {

                        //var url = "@Url.Action("CreateModel", "Catalogs")";
                        //var menu = "<button type="button" class="btn btn-primary btn-icon btn-rounded legitRipple"><i class="icon-menu7"></i></button>"
                        var menu = "<a title='Editar Modelo' class='btn bg-color-nissan btn-icon btn-rounded legitRipple' href='javascript:GetModalViewPartial(\"" + "/" + lang + url + "\",\"" + param + "\",\"" + catalog + "\",\"" + titleModal + "\")'><i class='icon-pencil'></i></a>";


                        return menu;
                    }

                }
            ]
        });
        // Enable Select2 select for the length option
        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            dropdownAutoWidth: true,
            width: 'auto'
        });
    }).fail(function (jqXHR, textStatus, err) {
        //MessageError(jqXHR, textStatus, err, Title, "LnkMessage");
        alert(err);
    });

}

function GridDataRanKGroup(IdTable, catalog, url, titleModal) {
    //var $tableByClass ='.' + IdTable
    $.ajax({
        type: "GET",
        url: "/" + lang + "/Catalogs/FillDatTableModel",
        data: { typeCat: catalog },
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    }).done(function (response) {
        //var data = $.parseJSON(response)
        oTable = $(IdTable).dataTable({
            "bDestroy": true,
            "aaData": response,
            responsive: {
                details: {
                    type: 'column',
                    target: 'tr'
                }
            },
            order: [1, 'asc'],
            columnDefs: [
                {
                    className: 'control',
                    orderable: false,
                    targets: 0,
                    data: null,
                    'defaultContent': ''
                },
                {
                    targets: 1,
                    orderable: true,
                    data: "Description"
                },
                {
                    targets: 2,
                    orderable: true,
                    data: "SubGroupCode"
                },
                //{
                //	targets: 3,
                //	orderable: true,
                //	data: "GeneralName"
                //},
                {
                    targets: 3,
                    orderable: false,
                    data: "Active",
                    'mRender': function (param, type, full) {

                        return param ? "<i class='icon-checkmark-circle icon-1x text-success'>" : "<i class='icon-1x text-danger icon-cancel-circle2'>";
                    }
                },
                {
                    targets: 4,
                    className: "text-center",
                    orderable: false,
                    data: "ModelID",
                    'mRender': function (param, type, full) {

                        //var url = "@Url.Action("CreateModel", "Catalogs")";
                        //var menu = "<button type="button" class="btn btn-primary btn-icon btn-rounded legitRipple"><i class="icon-menu7"></i></button>"
                        var menu = "<a title='Editar Modelo' class='btn bg-color-nissan btn-icon btn-rounded legitRipple' href='javascript:GetModalViewPartial(\"" + "/" + lang + url + "\",\"" + param + "\",\"" + catalog + "\",\"" + titleModal + "\")'><i class='icon-pencil'></i></a>";


                        return menu;
                    }

                }
            ]
        });
        // Enable Select2 select for the length option
        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            dropdownAutoWidth: true,
            width: 'auto'
        });
    }).fail(function (jqXHR, textStatus, err) {
        //MessageError(jqXHR, textStatus, err, Title, "LnkMessage");
        alert(err);
    });

}

function GridDataRanK(IdTable, catalog, url, titleModal) {
    //var $tableByClass ='.' + IdTable
    $.ajax({
        type: "GET",
        url: "/" + lang + "/Catalogs/FillDatTableModel",
        data: { typeCat: catalog },
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    }).done(function (response) {
        //var data = $.parseJSON(response)
        oTable = $(IdTable).dataTable({
            "bDestroy": true,
            "aaData": response,
            responsive: {
                details: {
                    type: 'column',
                    target: 'tr'
                }
            },
            order: [1, 'asc'],
            columnDefs: [
                {
                    className: 'control',
                    orderable: false,
                    targets: 0,
                    data: null,
                    'defaultContent': ''
                },
                {
                    targets: 1,
                    orderable: true,
                    data: "Description"
                },
                {
                    targets: 2,
                    orderable: true,
                    data: "RankNumber"
                },
                {
                    targets: 3,
                    orderable: true,
                    data: "GeneralName"
                },
                {
                    targets: 4,
                    orderable: true,
                    data: "Active",
                    'mRender': function (param, type, full) {

                        return param ? "<i class='icon-checkmark-circle icon-1x text-success'>" : "<i class='icon-1x text-danger icon-cancel-circle2'>";
                    }
                },
                {
                    targets: 5,
                    className: "text-center",
                    orderable: false,
                    data: "ModelID",
                    'mRender': function (param, type, full) {

                        //var url = "@Url.Action("CreateModel", "Catalogs")";
                        //var menu = "<button type="button" class="btn btn-primary btn-icon btn-rounded legitRipple"><i class="icon-menu7"></i></button>"
                        var menu = "<a title='Editar Modelo' class='btn bg-color-nissan btn-icon btn-rounded legitRipple' href='javascript:GetModalViewPartial(\"" + "/" + lang + url + "\",\"" + param + "\",\"" + catalog + "\",\"" + titleModal + "\")'><i class='icon-pencil'></i></a>";


                        return menu;
                    }

                }
            ]
        });
        // Enable Select2 select for the length option
        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            dropdownAutoWidth: true,
            width: 'auto'
        });
    }).fail(function (jqXHR, textStatus, err) {
        //MessageError(jqXHR, textStatus, err, Title, "LnkMessage");
        alert(err);
    });

}

function GridDataApproval(url, titleModal) {
    //var $tableByClass ='.' + IdTable
    $.ajax({
        type: "GET",
        url: "/" + lang + "/Catalogs/FillDatTableApprovalFlows",
        //data: { typeCat: catalog },
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    }).done(function (response) {
        //var data = $.parseJSON(response)
        oTable = $(".datatable-responsive-column-controlled").dataTable({
            "bDestroy": true,
            "aaData": response,
            responsive: {
                details: {
                    type: 'column',
                    target: 'tr'
                }
            },
            order: [1, 'asc'],
            columnDefs: [
                {
                    className: 'control',
                    orderable: false,
                    targets: 0,
                    data: null,
                    'defaultContent': ''
                },
                {
                    targets: 1,
                    orderable: true,
                    data: "Description"
                },
                {
                    targets: 2,
                    orderable: true,
                    data: "StatusFlow"
                },
                {
                    targets: 3,
                    orderable: true,
                    data: "ApprovateType"
                },
                {
                    targets: 4,
                    orderable: true,
                    data: "NameApprovateType"
                },
                {
                    targets: 5,
                    className: "text-center",
                    orderable: false,
                    data: "ApprovalFlowID",
                    'mRender': function (param, type, full) {

                        //var menu = "<button type="button" class="btn btn-primary btn-icon btn-rounded legitRipple"><i class="icon-menu7"></i></button>"
                        //var menu = "<a title='Editar Modelo' class='btn bg-color-nissan btn-icon btn-rounded legitRipple' href='javascript:GetModalViewPartial(\"" + param + "\")'><i class='icon-pencil'></i></a>"
                        var menu = "<a title='Editar Fluxos de Aprovação' class='btn bg-color-nissan btn-icon btn-rounded legitRipple' href='javascript:GetModalFluxo(\"" + "/" + lang + url + "\",\"" + param + "\",\"" + titleModal + "\")'><i class='icon-pencil'></i></a>";
                        menu += "<a title='Excluir Fluxos de Aprovação' class='btn bg-color-nissan btn-icon btn-rounded legitRipple' href='javascript:DeleteApproval(\"" + param + "\")'><i class='icon-bin'></i></a>";

                        return menu;
                    }

                }
            ]
        });
        // Enable Select2 select for the length option
        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            dropdownAutoWidth: true,
            width: 'auto'
        });
    }).fail(function (jqXHR, textStatus, err) {
        //MessageError(jqXHR, textStatus, err, Title, "LnkMessage");
        alert(err);
    });

}

function GridDataRequest() {
    //var $tableByClass ='.' + IdTable
    $.ajax({
        type: "GET",
        url: "/" + lang + "/VehicularRequest/ListFrotaData",
        //data: { typeCat: catalog },
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    }).done(function (response) {
        //var data = $.parseJSON(response)
        oTable = $("#RequestTable").dataTable({
            "bDestroy": true,
            "aaData": response,
            responsive: {
                details: {
                    type: 'column',
                    target: 'tr'
                }
            },
            columnDefs: [
                {
                    className: 'control',
                    orderable: false,
                    targets: 0,
                    data: null,
                    'defaultContent': ''
                },
                {
                    targets: 1,
                    orderable: true,
                    data: "OrderNumber"
                },
                {
                    targets: 2,
                    orderable: true,
                    data: "RequestType"
                },
                {
                    targets: 3,
                    orderable: true,
                    data: "Name"
                },
                {
                    targets: 4,
                    orderable: true,
                    data: "Vehicle"
                },
                {
                    targets: 5,
                    orderable: true,
                    data: "Comment"
                },
                {
                    targets: 6,
                    orderable: true,
                    data: "Description",
                    "mRender": function (param, type, full) {
                        var dataStatus = param;
                        if (full.StepStatus === -1) {
                            dataStatus = "<a type='button' class='' data-popup='popover' title='Popover title' data-trigger='hover' data-content='" + full.Comments + "'> " + param + " <i class=' icon-eye2 position-right'></i></a>";
                        }
                        return dataStatus;
                    }
                },
                {
                    targets: 7,
                    orderable: true,
                    data: "RequestDate",
                    'mRender': function (param) {
                        return ToJavaScriptDateTime(param);
                    }
                },
                {
                    targets: 8,
                    className: "text-center",
                    orderable: false,
                    data: "VehicularRequestID",
                    'mRender': function (param, type, full) {

                        var menu = "";
                        menu += "<ul class='icons-list'>";
                        menu += "<li class='dropdown'>";
                        menu += "<a href='#' class='dropdown-toggle' data-toggle='dropdown'> <i class='icon-menu9'></i>";
                        menu += "</a>";
                        menu += "<ul class='dropdown-menu dropdown-menu-right'>";
                        //menu += "<li><a href='javascript:AprrovaFlota(\"" + full.VehicularRequestID + "\",\"" + full.RequestTypeID + "\",\"" + full.StepStatus + "\",,\"" + full.Email + "\")'><i class='icon-thumbs-up2'></i> Aceptar</a></li>";
                        //menu += "<li><a href='/VehicularRequest/ConfirmDeny/" + full.VehicularRequestID + "'><i class='icon-thumbs-down2'></i> Cancelar</a></li>";
                        switch (full.RequestName) {
                            case "Pool":
                                break;
                            case "Department":
                                if (full.StepStatus === 1) {
                                    menu += "<li><a href='javascript:AprrovaFlota(\"" + full.VehicularRequestID + "\",\"" + full.RequestTypeID + "\",\"" + full.StepStatus + "\",,\"" + full.Email + "\")'><i class='icon-thumbs-up2'></i> Aceptar</a></li>";
                                    menu += "<li><a href='/VehicularRequest/ConfirmDeny/" + full.VehicularRequestID + "'><i class='icon-thumbs-down2'></i> Cancelar</a></li>";
                                }
                                break;
                            case "NissanToAll":
                                break;
                            default:
                                if (full.StepStatus === 1) {
                                    //menu += "<li><a href='javascript:AprrovaFlota(\"" + full.VehicularRequestID + "\",\"" + full.RequestTypeID + "\",\"" + full.StepStatus + "\",,\"" + full.Email + "\")'><i class='icon-thumbs-up2'></i> Aceptar</a></li>";
                                    //menu += "<li><a href='/VehicularRequest/ConfirmDeny/" + full.VehicularRequestID + "'><i class='icon-thumbs-down2'></i> Cancelar</a></li>";
                                }
                                if (full.StepStatus === 2) {
                                    //menu += "<li><a href='/Vehicle/AssignChassis/" + full.VehicularRequestID + "'><i class='icon-pencil6'></i> Asignar Chassis</a></li>";
                                    //menu += "";
                                }
                                if (full.StepStatus === 3) {
                                    //menu += "<li><a href='javascript:AprrovaFlota(\"" + full.VehicularRequestID + "\",\"" + full.RequestTypeID + "\",\"" + full.StepStatus + "\",,\"" + full.Email + "\")'><i class='icon-thumbs-up2'></i> Agregar Imobilizado</a></li>";
                                    //menu += "<li><a href='/VehicularRequest/ContabilidadeInmovilazion/" + full.IDVehicle + "/" + full.VehicularRequestID + "/" + full.RequestTypeID + "/" + full.StepStatus + "' ><i class='icon-thumbs-up2'></i> Agregar Imobilizado</a></li>";
                                    //menu += "<li><a href='/VehicularRequest/NoInmovilizado/" + full.VehicularRequestID + "/" + full.Chassis + "'><i class='icon-thumbs-down2'></i> No Imobilizado</a></li>";
                                }
                                if (full.StepStatus === 4) {
                                    menu += "<li><a href='/Vehicle/AssignPlate/" + full.VehicularRequestID + "' ><i class='icon-clippy'></i> Atribuir placa y RENAVAM</a></li>";
                                }
                                if (full.StepStatus === 5) {
                                    menu += "<li><a href='/Vehicle/AssignPlate/" + full.VehicularRequestID + "' ><i class='icon-clippy'></i>Nota Fiscal Remesa</a></li>";
                                }
                                break;
                        }
                        menu += "</ul>";
                        menu += "</li>";
                        menu += "</ul>";
                        return menu;
                    }
                }
            ],
            order: [1, 'asc'],
            drawCallback: function () {
                $('[data-popup="popover"]').popover({
                    template: '<div class="popover border-teal-400"><div class="arrow"></div><h3 class="popover-title bg-teal-400"></h3><div class="popover-content"></div></div>'
                });
            }
        });
        // Enable Select2 select for the length option
        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            dropdownAutoWidth: true,
            width: 'auto'
        });

    }).fail(function (jqXHR, textStatus, err) {
        //MessageError(jqXHR, textStatus, err, Title, "LnkMessage");
        alert(err);
    });

}

function GridDataVersion(IdTable, url) {
    //var $tableByClass ='.' + IdTable
    $.ajax({
        type: "GET",
        url: "/" + lang + "/Catalogs/FillDatTableVersion",
        //data: { typeCat: catalog },
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    }).done(function (response) {
        //var data = $.parseJSON(response)
        oTable = $(IdTable).dataTable({
            "bDestroy": true,
            "aaData": response,
            responsive: {
                details: {
                    type: 'column',
                    target: 'tr'
                }
            },
            order: [1, 'asc'],
            columnDefs: [
                {
                    className: 'control',
                    orderable: false,
                    targets: 0,
                    data: null,
                    'defaultContent': ''
                },
                {
                    targets: 1,
                    orderable: true,
                    data: "ModelCar"
                },
                {
                    targets: 2,
                    orderable: true,
                    data: "VersionCar"
                },
                //{
                //    targets: 3,
                //    orderable: true,
                //    data: "Motor"
                //},
                {
                    targets: 3,
                    orderable: true,
                    data: "Company"
                },
                {
                    targets: 4,
                    className: "text-center",
                    orderable: false,
                    data: "VersionID",
                    'mRender': function (param, type, full) {
                        var menu = "<a title='Editar Modelo' class='btn bg-color-nissan btn-icon btn-rounded legitRipple' href='javascript:GetModalVersion(\"" + "/" + lang + url + "\",\"" + param + "\",\"Editar Versão\")'><i class='icon-pencil'></i></a>";
                        return menu;
                    }

                }
            ]
        });
        // Enable Select2 select for the length option
        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            dropdownAutoWidth: true,
            width: 'auto'
        });
    }).fail(function (jqXHR, textStatus, err) {
        //MessageError(jqXHR, textStatus, err, Title, "LnkMessage");
        alert(err);
    });
    
}
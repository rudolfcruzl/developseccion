﻿
var validationInputTypes = ['creditcard', 'max', 'email', 'min', 'phone', 'number', 'maxlength', 'url',
    'range', 'date', 'minlength', 'equalTo', 'digits', 'customEmail', 'RegularCPFRG', 'RegularCP', 'TelephoneBR', 'MayusculaNumeros', 'check_Select',
    'numberNotStartWithZero'];

jQuery.validator.addMethod('customEmail', function (value, element) {
    return validateEmail(value);
}, "Por favor, ingrese una dirección de correo electrónico válida.");


jQuery.validator.addMethod("RegularCPFRG", function (value, element) {
    //return /^([0-9.-]+)$/.test(value);
    return /^([0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[-]?[0-9]{2})$/.test(value);
}, jQuery.validator.messages.CPF);

jQuery.validator.addMethod("RegularCP", function (value, element) {

    return IsCEP(value);
}, jQuery.validator.messages.ziprange);

jQuery.validator.addMethod("TelephoneBR", function (value, eelement) {
    return /^(?:([0-9]{2})[ ]?)?[0-9]{2}[ ]?(?:[0-9][ ]?){1,4}(?:[0-9][ ]?){3}$/.test(value);
}, jQuery.validator.messages.extension);

jQuery.validator.addMethod("MayusculaNumeros", function (value, eelement) {
    return /^[0-9A-Z]+$/.test(value);
}, 'Apenas letras maiúsculas e números');

jQuery.validator.addMethod("numberNotStartWithZero", function (value, element) {
    return IsEqualsZero(value);
}, jQuery.validator.messages.required);



var ValidateRegisterUserJson = {
    "rows": [
      [
        {
            "Name": "CPF",
            "IsRequired": true,
            "Datatype": ['maxlength', 'RegularCPFRG', 'minlength'],
            "maxlength": 18,
            "minlength": 11
        }
      ],
      [
        {
            "Name": "RG",
            "IsRequired": true,
            "Datatype": ['maxlength'],
            "maxlength": 100
        }
      ],
      [
        {
            "Name": "Email",
            "IsRequired": true,
            "Datatype": ['customEmail']
        }
      ],
      [
        {
            "Name": "CP",
            "IsRequired": true,
            "Datatype": ['RegularCP']
        }
      ],
      [
        {
            "Name": "Enrollment",
            "IsRequired": false,
            "Datatype": ['number', 'maxlength', 'minlength'],
            "maxlength": 8,
            "minlength": 8,
        }
      ],
      [
        {
            "Name": "ResidencialPhone",
            "IsRequired": true,
            "Datatype": ['TelephoneBR']
        }
      ],
      [
        {
            "Name": "HomePhone",
            "IsRequired": true,
            "Datatype": ['TelephoneBR']
        }
      ],
      [
        {
            "Name": "CelPhone",
            "IsRequired": true,
            "Datatype": ['TelephoneBR']
        }
      ],
	  [
        {
            "Name": "CNH",
            "IsRequired": false,
            "Datatype": ['maxlength', 'minlength'],
            "maxlength": 11,
            "minlength": 11
        }
        ],
            [
                {
                    "Name": "Plate",
                    "IsRequired": true,
                    "Datatype": ['MayusculaNumeros', 'maxlength', 'minlength'],
                    "maxlength": 7,
                    "minlength": 7
                }
            ],
            [
                {
                    "Name": "RENAVAM",
                    "IsRequired": false,
                    "Datatype": ['maxlength', 'minlength'],
                    "maxlength": 11,
                    "minlength": 11
                }
            ],
        [
            {
                "Name": "RadioBtn",
                "IsRequired": true
            }
        ]
    ]
};

function IsEqualsZero(value) {
    if (value === "0.00") {
        return false;
    }
    return true;
}

function validateEmail(email) {
    //var re = /^(([^<>()[\]\\.,;:\s@('@')\"]+(\.[^<>()[\]\\.,;:\s@('@')\"]+)*)|(\".+\"))@('@')((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function IsCEP(strCEP) {
    // Caso o CEP não esteja nesse formato ele é inválido!
    var objER = /^[0-9]{5}-[0-9]{3}$/;

    strCEP = Trim(strCEP);
    if (strCEP.length > 0) {
        //if (objER.test(strCEP))
        //    return true;
        //else
        //    return false;
        return objER.test(strCEP);
    }
}
function Trim(strTexto) {
    // Substitúi os espaços vazios no inicio e no fim da string por vazio.
    return strTexto.replace(/^s+|s+$/g, '');
}
var confirmPassword = {
    "rows": [
        [
            {
                "Name": "NewPassword",
                "IsRequired": true
            }
        ],
        [
            {
                "Name": "ConfirmPassword",
                "IsRequired": true,
                "equalTo": "#NewPassword"
            }
        ]
    ]

}

var ValidateChassis = {
    "rows": [
         [
        {
            "Name": "ChassisDescription",
            "IsRequired": false,
            "Datatype": ['maxlength', 'minlength'],
            "maxlength": 17,
            "minlength": 17
        }
         ]
    ]
};

var ValidateRENAVAM = {
    "rows": [
         [
            {
                 "Name": "Plate",
                 "IsRequired": true,
                 "Datatype": ['MayusculaNumeros', 'maxlength', 'minlength'],
                 "maxlength": 7,
                 "minlength": 7
             }
         ],
         [
            {
                "Name": "RENAVAM",
                "IsRequired": false,
                "Datatype": ['maxlength', 'minlength'],
                "maxlength": 11,
                "minlength": 11
            }
        ]
    ]
};
var EmailValid = {
    "rows": [
        [
            {
                "Name": "Email",
                "IsRequired": false,
                "Datatype": ['customEmail']
            }
        ]
    ]
};

var WeddingRequestValid = {
    "rows": [
        [
            {
                "Name": "DriverEmail",
                "IsRequired": false
            }
        ],
        [
            {
                "Name": "ConfirmPassword",
                "IsRequired": true
            }
        ]
    ]
};

var DriverRequestValid = {
    "rows": [
        [
            {
                "Name": "DriverEmail",
                "IsRequired": false,
                "Datatype": ['customEmail']
            }
        ],
        [
            {
                "Name": "DriverName",
                "IsRequired": true,
                "Datatype": ['maxlength'],
                "maxlength": 100
            }
        ],
        [
            {
                "Name": "RentalCost",
                "IsRequired": true
            }
        ]
    ]
};

var EditCar = {
    "rows": [
        [
            {
                "Name": "DiscountPercent",
                "IsRequired": true,
                "Datatype": ['numberNotStartWithZero']
            }
        ],
        [
            {
                "Name": "EnssuranceDiscount",
                "IsRequired": true,
                "Datatype": ['numberNotStartWithZero']
            }
        ],
        [
            {
                "Name": "UserId",
                "IsRequired": true
            }
        ],
        [
            {
                "Name": "CostCenterId",
                "IsRequired": true
            }
        ],
        [
            {
                "Name": "Department",
                "IsRequired": true
            }
        ],
        [
            {
                "Name": "RankID",
                "IsRequired": true
            }
        ],
        [
            {
                "Name": "RankGroupID",
                "IsRequired": true
            }
        ],
        [
            {
                "Name": "Plate",
                "IsRequired": false,
                "Datatype": ['MayusculaNumeros', 'maxlength', 'minlength'],
                "maxlength": 7,
                "minlength": 7
            }
        ],
        [
            {
                "Name": "RENAVAM",
                "IsRequired": false,
                "Datatype": ['maxlength', 'minlength'],
                "maxlength": 11,
                "minlength": 11
            }
        ],
        [
            {
                "Name": "Chassis",
                "IsRequired": false,
                "Datatype": ['maxlength', 'minlength'],
                "maxlength": 17,
                "minlength": 17
            }
        ]
    ]
};



var TypeBuyer = {
    "rows": [
        [
            {
                "Name": "RadioBtn",
                "IsRequired": true
            }
        ]
    ]
};

var daysOfWeek = ["dom", "seg", "ter", "qua", "qui", "sex", "sáb"];
var monthNames = [
    "Janeiro",
    "Fevereiro",
    "Março",
    "Abril",
    "Maio",
    "Junho",
    "Julho",
    "Agosto",
    "Setembro",
    "Outubro",
    "Novembro",
    "Dezembro"
];
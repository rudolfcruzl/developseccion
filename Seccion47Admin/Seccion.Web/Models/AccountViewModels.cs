﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Seccion.Web.Models
{
    public class LoginViewModel
    {
        [Required]
        [Display(Name = "User")]
        public string Usuario { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }


        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }

        [Display(Name = "Politics")]
        public bool TermsConditions { get; set; }

    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ResetViewModel
    {

        [Required]
        [StringLength(100, ErrorMessage = "O {0} deve ter pelo menos {2} caracteres.", MinimumLength = 8)]
        [RegularExpression(@"(?=^.{6,255}$)((?=.*\d)(?=.*[A-Z0-9])(?=.*[a-z0-9])(?=.*[!%&@#$*?_~-])
                            |(?=.*\d)(?=.*[^0-9])(?=.*[^A-Za-z0-9])(?=.*[^a-zA-Z0-9])(?=.*[!%&@#$*?_~-])
                            |(?=.*\d)(?=.*[^A-Za-z0-9])(?=.*[!%&@#$*?_~-])
                            |(?=.*\d)(?=.*[^0-9])(?=.*[^a-zA-Z])(?=.*[^A-Za-z])(?=.*[!%&@#$*?_~-])
                            |(?=.*\d)(?=.*[^0-9])(?=.*[!%&@#$*?_~-])){8,15}^.*", ErrorMessage = "Pelo menos uma letra maiúscula, Pelo menos uma letra minucula, Pelo menos um dígito, Sem espaços em branco, Pelo menos 1 caractere especial")]
        [DataType(DataType.Password)]
        [Display(Name = "Nueva Contraseña")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar Nueva Contraseña")]
        [Compare("NewPassword", ErrorMessage = "The entered passwords do not match.")]
        public string ConfirmPassword { get; set; }

        public int UserId { get; set; }
        public string code { get; set; }

    }
}
﻿using Seccion.Model.Enums;
using System;

namespace Seccion.Web.Models.GridListModel
{
    public class ListNotificationViewModel
    {
        public Int64 RowNum { get; set; }
        public int TotalFilter { get; set; }
        public int TotalRequest { get; set; }
        public int TbNotificationId { get; set; }
        public string Title { get; set; }
        public string Status { get; set; }
        public string Email { get; set; }
        public string DateInsert { get; set; }
    }
}
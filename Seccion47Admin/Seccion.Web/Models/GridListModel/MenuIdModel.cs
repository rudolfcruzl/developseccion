﻿namespace Seccion.Web.Models.GridListModel
{
    public class MenuIdModel
	{
		public int Id { get; set; }
		public string EditLink { get; set; }
		public string DetailLink { get; set; }
        public string CompleteLink { get; set; }
        public string Delete { get; set; }
        public string ModalDetail { get; set; }
    }
}
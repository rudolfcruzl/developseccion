﻿using System.Collections.Generic;

namespace Seccion.Web.Models.Dtos
{

    public class MenuDto
    {
        
        public int MenuItemId { get; set; }
        
        public string Title { get; set; }
       
        public string Description { get; set; }
        
        public string Icon { get; set; }
        
        public string Url { get; set; }

        public List<MenuDto> Children { get; set; }
        public List<MenuPermission> Permission { get; set; }
        public List<Rol> Roles { get; set; }
    }

    public class MenuPermission
    {
        public int PermissionId { get; set; }
    }

    public class Rol
    {
        public int RoleId { get; set; }
    }
}

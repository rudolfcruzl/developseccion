﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using Seccion.Model.Enums;

namespace Seccion.Web.Models.ViewModels
{
    public class CardViewModel
    {
        /// <summary>
        /// PK de la tabla TbCard
        /// </summary>
        public int TbCardId { get; set; }

        /// <summary>
        /// Titutlo de la tarea
        /// </summary>
        [Display(Name = "Titulo:")]
        public string Title { get; set; }

        /// <summary>
        /// Descripcion de la tarea
        /// </summary>
        [Display(Name = "Descripcion:")]
        public string Description { get; set; }

        /// <summary>
        /// Lista de usuarios a quien se le asigna la tarea
        /// </summary>
        [Display(Name = "Asignar a usuarios:")]
        public List<int> Users { get; set; }

        /// <summary>
        /// Lista de archivos a importar
        /// </summary>
        [Display(Name = "Asignar a usuarios:")]
        public List<HttpPostedFileBase> ImportFiles { get; set; }
    }
}
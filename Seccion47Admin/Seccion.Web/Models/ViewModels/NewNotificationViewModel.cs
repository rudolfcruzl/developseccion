﻿using System;

namespace Seccion.Web.Models.ViewModels
{
    public class NewNotificationViewModel
    {
        public int TbNotificationId { get; set; }
        public string Title { get; set; }
        public string UserInsert { get; set; }
        public DateTime? DateInsert { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations;

namespace Seccion.Web.Models.ViewModels
{
    public class BoxesViewModel
    {
        public int TbBoxesId { get; set; }
        public int TbEmployeeId { get; set; }
        [Display(Name = "Folio 547:")]
        public string Folio547 { get; set; }
        [Display(Name = "Folio NP:")]
        public string FolioNP { get; set; }
        [Display(Name = "Cantidad:")]
        public string Quantity { get; set; }
        [Display(Name = "Fecha:")]
        public string Date { get; set; }
        [Display(Name = "Periodo:")]
        public int Period { get; set; }
        [Display(Name = "Tasa:")]
        public decimal Rate { get; set; }
        [Display(Name = "Gastos de Admin.:")]
        public decimal GA { get; set; }
        [Display(Name = "Meses:")]
        public int Month { get; set; }
        [Display(Name = "Catorcenas:")]
        public int Fourteen { get; set; }
        [Display(Name = "Meses Adicionales:")]
        public int? AdditionalMonths { get; set; }
        [Display(Name = "Cantidad Total:")]
        public string TotalQuantity { get; set; }
        [Display(Name = "Descuento:")]
        public string Discount { get; set; }
        [Display(Name = "Nuevo Descuento:")]
        public string NewDiscount { get; set; }
        [Display(Name = "Saldo:")]
        public string Balance { get; set; }
        [Display(Name = "Saldo Actual:")]
        public string CurrentBalance { get; set; }
        [Display(Name = "Catorcenas Restantes:")]
        public int FourteenRemaining { get; set; }
        [Display(Name = "Liquida en 2 CAT:")]
        public bool LiquidTwoCAT { get; set; }
        [Display(Name = "Tipo de Prestamo:")]
        public string TypeLoan { get; set; }
        [Display(Name = "Seguro de Credito:")]
        public string CreditInsurance { get; set; }
        [Display(Name = "Número de Ficha:")]
        public string NumberFicha { get; set; }
        [Display(Name = "Nombre:")]
        public string UserName { get; set; }

        public int TypeInformation { get; set; }
        public string RootActionName { get; set; }
    }
}
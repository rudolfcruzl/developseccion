﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seccion.Web.Models.ViewModels
{
	public class ListAccountUsersViewModel
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string BirthDate { get; set; }
		public string Gender { get; set; }
		public string Email { get; set; }
		public string PhoneNumber { get; set; }
	}
}
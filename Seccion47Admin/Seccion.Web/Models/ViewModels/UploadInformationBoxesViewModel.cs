﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seccion.Web.Models.ViewModels
{
    public class UploadInformationBoxesViewModel
    {
        public bool OldBox { get; set; }
        public bool CashBox { get; set; }
        public bool NewBox { get; set; }
        public HttpPostedFileBase file { get; set; }
    }
}
﻿using Seccion.Model.Enums;
using Seccion.Model.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Seccion.Web.Models.ViewModels
{
    public class EditCardViewModel
    {
        /// <summary>
        /// PK de la tabla TbCard
        /// </summary>
        public int TbCardId { get; set; }

        public int TbNotificationId { get; set; }

        /// <summary>
        /// Titutlo de la tarea
        /// </summary>
        [Display(Name = "Titulo:")]
        public string Title { get; set; }

        /// <summary>
        /// Descripcion de la tarea
        /// </summary>
        [Display(Name = "Descripción:")]
        public string Description { get; set; }

        /// <summary>
        /// Comentario de cierre de tarea
        /// </summary>
        [Display(Name = "Comentario:")]
        public string Comment { get; set; }

        [Display(Name = "Estado:")]
        public StatusNotification Status { get; set; }

        /// <summary>
        /// Lista de archivos a importar
        /// </summary>
        [Display(Name = "Asignar a usuarios:")]
        public IEnumerable<TbFile> ImportFiles { get; set; }
    }
}
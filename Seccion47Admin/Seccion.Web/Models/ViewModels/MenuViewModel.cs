﻿using Seccion.Model.IdentityModel;
using Seccion.Web.Models.Dtos;
using System.Collections.Generic;

namespace Seccion.Web.Models.ViewModels
{
    public class MenuViewModel
    {
        public List<MenuDto> MenuItems { get; set; }
    }
}

var href = "";
var paginaActual = window.location.href;
$(document).ready(function () {
	loading();
	$.LoadingOverlay("show");
	var xhr = new XMLHttpRequest();
	var paginaActual = window.location.href;
	xhr.open('GET', paginaActual, true);
	xhr.addEventListener('readystatechange', function () {
		if (xhr.readyState === 4 && xhr.status === 200) {
			var dark = $('.login-container');
			$.LoadingOverlay("hide");
		}
		if (xhr.readyState === 2 && xhr.status === 500) {
			var dark = $('.bodyContainer');
			$.LoadingOverlay("hide");
		}
	});
	xhr.send(null);
});
$(document).on('click', 'a', function () {
	href = $(this).attr("href");
});

$(window).on('beforeunload', function () {
	if (href != "") {

		client = new XMLHttpRequest();
		client.open("GET", href, true);
		client.onprogress = function (pe) {
			if (pe.lengthComputable) {
				loading();
				$.LoadingOverlay("show");
			}
		}
		client.onloadend = function (pe) {
			//var dark = $('.login-container');
			//$(dark).unblock();
			$.LoadingOverlay("hide");
		}
		client.send(null);
    }
});

$(document).ajaxStart(function () {
	loading();
	$.LoadingOverlay("show");
});

$(document).ajaxStop(function () {
	//var dark = $('.login-container');
	//$(dark).unblock();
	$.LoadingOverlay("hide");
});
$(document).ajaxSend(function (event, jqxhr, settings) {
	loading();
	$.LoadingOverlay("show");
});
$(document).ajaxComplete(function (event, jqxhr, settings) {
	$.LoadingOverlay("hide");
});
function loading() {
	$.LoadingOverlaySetup({
		backgroundClass: "custom_background",
		image: "/assetsAce/image/circles.svg",
		imageAnimation: "2000ms rotate_right",
		imageColor: "#fb8c00",
		text: "Por favor espere, Cargando...",
		textClass: "custom_textLoading",
	});
}

$(".DownloadFile").on("click",
    function (e) {
		client = new XMLHttpRequest();
		client.open("GET", href, true);
		client.onprogress = function (pe) {
			if (pe.lengthComputable) {
				loading();
				$.LoadingOverlay("show");
			}
		}
		client.onloadend = function (pe) {
			$.LoadingOverlay("hide");
		}
		client.send(null);
    });
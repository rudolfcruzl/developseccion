﻿using AutoMapper;
using Seccion.Data.ResposeDto;
using Seccion.Model.IdentityModel;
using Seccion.Model.Models;
using Seccion.Web.Common;
using Seccion.Web.Models.GridListModel;
using Seccion.Web.Models.ViewModels;
using System;
using AutoMapper.Configuration.Conventions;

namespace Seccion.Web.Mappings
{

    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            ConfigureMappings();
        }


        /// <summary>
        /// Creates a mapping between source (Domain) and destination (ViewModel)
        /// </summary>
        private void ConfigureMappings()
        {
            CreateMap<ApplicationUser, ListAccountUsersViewModel>()
                .ForMember(dest => dest.BirthDate, opt => opt.MapFrom(src => src.BirthDate == null ? "" : src.BirthDate.Value.ToString("yyyy-MM-dd")));
            CreateMap<ApplicationUser, AccountUserViewModel> ()
                .ForMember(dest => dest.BirthDate, opt => opt.MapFrom(src => src.BirthDate == null ? "" : src.BirthDate.Value.ToString("yyyy-MM-dd")));
            CreateMap<ApplicationUser, MyDataViewModel>()
                .ForMember(dest => dest.BirthDate, opt => opt.MapFrom(src => src.BirthDate == null ? "" : src.BirthDate.Value.ToString("yyyy-MM-dd")));
            CreateMap<TbEmployee, EmployeeVIewModel> ();
            CreateMap<TbEmployee, EmployeeCompleteDataViewModel>();

            CreateMap<TbCard, EditCardViewModel>();

            CreateMap<TbNotification, NewNotificationViewModel>()
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Card.Title));

            CreateMap<SP_EmployeeDto, ListEmployeeViewModel>();
            CreateMap<SP_GetNotificationDto, ListNotificationViewModel>()
                .ForMember(dest => dest.DateInsert, opt => opt.MapFrom(src => src.DateInsert.ToString("dd-MM-yyyy HH:mm")));

            CreateMap<TbRequest, RequestViewModel>()
                .ForMember(dest => dest.DateCreateRequest, opt => opt.MapFrom(src => src.DateCreateRequest == null ? "" : src.DateCreateRequest.ToString()))
                .ForMember(dest => dest.AbonoCatorcenalMensual, opt => opt.MapFrom(src => SeparateDecimal.CleanDecimalFromDataBase(src.AbonoCatorcenalMensual.ToString())))
                .ForMember(dest => dest.AdministrationExpensesResult, opt => opt.MapFrom(src => SeparateDecimal.CleanDecimalFromDataBase(src.AdministrationExpensesResult.ToString())))
                .ForMember(dest => dest.TotalPay, opt => opt.MapFrom(src => SeparateDecimal.CleanDecimalFromDataBase(src.TotalPay.ToString())));
            _ = CreateMap<TbBoxes, BoxesViewModel>()
                .ForMember(dest => dest.Quantity, opt => opt.MapFrom(src => Math.Round(src.Quantity, 0)))
                .ForMember(dest => dest.TotalQuantity, opt => opt.MapFrom(src => Math.Round(src.TotalQuantity, 0)))
                .ForMember(dest => dest.Discount, opt => opt.MapFrom(src => Math.Round(src.Discount.Value, 0)))
                .ForMember(dest => dest.NewDiscount, opt => opt.MapFrom(src => Math.Round(src.NewDiscount.Value, 0)))
                .ForMember(dest => dest.Balance, opt => opt.MapFrom(src => Math.Round(src.Balance.Value, 0)))
                .ForMember(dest => dest.CurrentBalance, opt => opt.MapFrom(src => Math.Round(src.CurrentBalance, 0)))
                .ForMember(dest => dest.CreditInsurance, opt => opt.MapFrom(src => src.CreditInsurance == null ? 0 : Math.Round(src.CreditInsurance.Value , 0)))
                .ForMember(dest => dest.Date, opt => opt.MapFrom(src => src.Date == null ? "" : src.Date.ToString("dd-MM-yyyy")))
                .ForMember(dest => dest.NumberFicha, opt => opt.MapFrom(src => src.TbEmployee.NumberFicha))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => $"{src.TbEmployee.Name} {src.TbEmployee.FirstName} {src.TbEmployee.LastName}"));
        }
    }
}
﻿using AutoMapper;
using Seccion.Model.IdentityModel;
using Seccion.Model.Models;
using Seccion.Web.Common;
using Seccion.Web.Models.ViewModels;
using System;

namespace Seccion.Web.Mappings
{
    /// <summary>
    /// Creates a mapping between source (ViewModel) and destination (Domain)
    /// </summary>
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {

            CreateMap<ListAccountUsersViewModel, ApplicationUser>();
            CreateMap<AccountUserViewModel, ApplicationUser>();
            CreateMap<MyDataViewModel, ApplicationUser> ();
            CreateMap<EmployeeVIewModel, TbEmployee>();
            CreateMap<CardViewModel, TbCard>();
            CreateMap<EditCardViewModel, TbNotification>();

            CreateMap<EmployeeCompleteDataViewModel, TbEmployee>()
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember, destMember) => destMember == null));

            CreateMap<RequestViewModel, TbRequest>()
                //.ForMember(dest => dest.Amount, opt => opt.MapFrom(src => Math.Round(Convert.ToDecimal(src.Amount.Replace("$", "").Replace(",", "")), 2)))
                .ForMember(dest => dest.Amount, opt => opt.MapFrom(src => Math.Round(Convert.ToDecimal(SeparateDecimal.CleanDecimal(src.Amount)), 2)))
                .ForMember(dest => dest.AbonoCatorcenalMensual, opt => opt.MapFrom(src => Math.Round(Convert.ToDecimal(SeparateDecimal.CleanDecimal(src.AbonoCatorcenalMensual)), 2)))
                .ForMember(dest => dest.AdministrationExpensesResult, opt => opt.MapFrom(src => Math.Round(Convert.ToDecimal(SeparateDecimal.CleanDecimal( src.AdministrationExpensesResult)),2)))
                .ForMember(dest => dest.TotalPay, opt => opt.MapFrom(src => Math.Round(Convert.ToDecimal(SeparateDecimal.CleanDecimal(src.TotalPay)),2)))
                .ForMember(dest => dest.DiscountRate, opt => opt.MapFrom(src => Convert.ToDecimal(SeparateDecimal.CleanDecimal(src.DiscountRate))))
                .ForMember(dest => dest.DebitAval, opt => opt.MapFrom(src => src.DebitAval != null ? Convert.ToDecimal(SeparateDecimal.CleanDecimal(src.DebitAval)) : 0))
                .ForMember(dest => dest.AdministrationExpenses, opt => opt.MapFrom(src => Convert.ToDecimal(SeparateDecimal.CleanDecimal(src.AdministrationExpenses))))
                .ForMember(dest => dest.DateCreateRequest, opt => opt.MapFrom(src => Convert.ToDateTime(src.DateCreateRequest)));
            CreateMap<ExpensesViewModel, TbFuneralExpenses>();
            CreateMap<BoxesViewModel, TbBoxes>()
                .ForMember(dest => dest.Quantity, opt => opt.MapFrom(src => Convert.ToDecimal(SeparateDecimal.CleanDecimal(src.Quantity))))
                .ForMember(dest => dest.TotalQuantity, opt => opt.MapFrom(src => Convert.ToDecimal(SeparateDecimal.CleanDecimal(src.TotalQuantity))))
                .ForMember(dest => dest.Discount, opt => opt.MapFrom(src => Convert.ToDecimal(SeparateDecimal.CleanDecimal(src.Discount))))
                .ForMember(dest => dest.NewDiscount, opt => opt.MapFrom(src => Convert.ToDecimal(SeparateDecimal.CleanDecimal(src.NewDiscount))))
                .ForMember(dest => dest.Balance, opt => opt.MapFrom(src => Convert.ToDecimal(SeparateDecimal.CleanDecimal(src.Balance))))
                .ForMember(dest => dest.CurrentBalance, opt => opt.MapFrom(src => Convert.ToDecimal(SeparateDecimal.CleanDecimal(src.CurrentBalance))))
                .ForMember(dest => dest.CreditInsurance, opt => opt.MapFrom(src => Convert.ToDecimal(SeparateDecimal.CleanDecimal(src.CreditInsurance))));
        }
    }
}
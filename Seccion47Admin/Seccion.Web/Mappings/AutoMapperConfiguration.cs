﻿using AutoMapper;
using AutoMapper.Configuration;
using System;
using System.Linq;

namespace Seccion.Web.Mappings
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(x =>
             {
                 x.AddProfile<ViewModelToDomainMappingProfile>();
                 x.AddProfile(new DomainToViewModelMappingProfile());
             });

        }
        //public static IMapper Mapper { get; private set; }

        //public static MapperConfiguration MapperConfiguration { get; private set; }
        //public static void Configure()
        //{
        //    MapperConfiguration = new MapperConfiguration(x =>
        //    {


        //        x.AddProfile(new ViewModelToDomainMappingProfile());
        //        x.AddProfile(new DomainToViewModelMappingProfile());
        //    });

        //    Mapper = MapperConfiguration.CreateMapper();
        //}


    }
}
﻿using Microsoft.Owin;
using Owin;
using Seccion.Web.Connections;


[assembly: OwinStartup(typeof(Seccion.Data.Infrastructure.StartupOwin))]
namespace Seccion.Web
{
    public class Startup
    {
        public  void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
            app.MapSignalR<NotificationConnection>("/notification");
            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888
            //var configuration = new StartupOwin();
            app.UseRedirectValidation();
            app.UseReferrerPolicy(opts => opts.NoReferrer());
            Data.Infrastructure.StartupOwin.ConfigureAuth(app);
        }
    }
}

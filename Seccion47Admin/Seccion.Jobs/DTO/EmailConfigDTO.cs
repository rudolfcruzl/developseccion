﻿using Microsoft.AspNet.Identity;

namespace Seccion.Jobs.DTO
{

    public class EmailConfigDTO
    {
        public string SendGridKey { get; set; }
        public string From { get; set; }
        public string TemplateBlockUser { get; set; }
        public string TemplateChangePassword { get; set; }
        public string UrlChangePassword { get; set; }
    }
}

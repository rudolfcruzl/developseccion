﻿using System.Collections.Generic;

namespace Seccion.Jobs.DTO
{
    public class UserSendEmailDTO
    {
        public string Email { get; set; }
        public string From { get; set; }
        public string ApiKey { get; set; }
        public string TemplateId { get; set; }
        public Dictionary<string, string> DinamicTemplateData { get; set; }
    }
}

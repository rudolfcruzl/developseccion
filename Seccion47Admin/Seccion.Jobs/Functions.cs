using Microsoft.Azure.WebJobs;
using Seccion.Data;
using Seccion.Jobs.DTO;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Seccion.Jobs
{
    public class Functions
    {
        // This function will get triggered/executed when a new message is written
        // on an Azure Queue called queue.
        public static void ProcessQueueMessage([QueueTrigger("queue")] string message, TextWriter log)
        {
            log.WriteLine(message);
        }

        public static async Task ExpiredAccount(TextWriter log)
        {
            try
            {
                log.WriteLine($"Comenzando Job: {DateTime.Now}");

                var settings = ConfigurationManager.ConnectionStrings;
                var appSettings = ConfigurationManager.AppSettings;

                var emailConfig = new EmailConfigDTO()
                {
                    SendGridKey = appSettings["SendGridKey"],
                    TemplateBlockUser = appSettings["TemplateBlockUser"],
                    TemplateChangePassword = appSettings["TemplateChangePassword"],
                    UrlChangePassword = appSettings["UrlChangePassword"]
                };

                using (var context = new StoreEntities(settings["StoreEntities"].ToString()))
                {
                    var users = await context.Users.Where(w => w.JoinDate != null || w.UpdatePasswordDate != null).ToListAsync();

                    var userExpired = users.Where(w =>
                        (w.JoinDate != null && w.Active &&
                             Convert.ToDateTime(w.JoinDate).AddMonths(3) <= DateTime.Now.AddDays(-15)
                        ) || (w.UpdatePasswordDate != null &&
                              Convert.ToDateTime(w.UpdatePasswordDate).AddMonths(3) <= DateTime.Now.AddDays(-15)
                        )).ToList();

                    var userRenew = users.Where(w =>
                        (w.JoinDate != null && w.Active &&
                            Convert.ToDateTime(w.JoinDate).AddMonths(3) >= DateTime.Now.AddDays(-15) &&
                            Convert.ToDateTime(w.JoinDate).AddMonths(3) <= DateTime.Now
                        ) ||
                            (w.UpdatePasswordDate != null &&
                             Convert.ToDateTime(w.UpdatePasswordDate).AddMonths(3) >= DateTime.Now.AddDays(-15) &&
                             Convert.ToDateTime(w.UpdatePasswordDate).AddMonths(3) <= DateTime.Now
                        )).ToList();

                    //Eliminar es solo para pruebas
                    userRenew = userRenew.Where(w => w.Email != "dario@hotnail.com").ToList();

                    foreach (var user in userRenew)
                    {
                        var userSendEmail = new UserSendEmailDTO()
                        {
                            Email = user.Email,
                            ApiKey = emailConfig.SendGridKey,
                            TemplateId = emailConfig.TemplateChangePassword,
                            DinamicTemplateData = new Dictionary<string, string>()
                            {
                                { "Sender_Name", string.Concat(user.Name, " ", user.FirstName, " ", user.LastName) },
                                { "Expired_Date", (user.UpdatePasswordDate != null
                                    ? Convert.ToDateTime(user.UpdatePasswordDate).AddMonths(3).ToShortDateString()
                                    : Convert.ToDateTime(user.JoinDate).AddMonths(3).ToShortDateString()) },
                                { "Url_UpdatePassword", $"{emailConfig.UrlChangePassword}{user.UserName}" }
                            }
                        };

                        log.WriteLine($"Usuario a expirar contraseņa - UserName:{user.UserName}, Fecha de vencimiento: {userSendEmail.DinamicTemplateData["Expired_Date"]}");

                        var response = await SendEmail(userSendEmail);

                        log.WriteLine(response.ToString());
                    }

                    foreach (var user in userExpired)
                    {
                        var userSendEmail = new UserSendEmailDTO()
                        {
                            Email = user.Email,
                            ApiKey = emailConfig.SendGridKey,
                            TemplateId = emailConfig.TemplateBlockUser,
                            DinamicTemplateData = new Dictionary<string, string>()
                            {
                                { "Sender_Name", string.Concat(user.Name, " ", user.FirstName, " ", user.LastName) },
                                { "Url_UpdatePassword", $"{emailConfig.UrlChangePassword}{user.UserName}" }
                            }
                        };

                        log.WriteLine($"Usuario espirados - UserName:{user.UserName}, Fecha de bloqueo: {DateTime.Now}");

                        var response = await SendEmail(userSendEmail);

                        log.WriteLine(response.ToString());

                        user.Active = false;

                        context.Users.AddOrUpdate(user);
                        context.SaveChanges();
                    }

                    log.WriteLine("Finalizando proceso.");
                }
            }
            catch (Exception e)
            {
                log.WriteLine(e);
                throw;
            }
        }

        private static async Task<object> SendEmail(UserSendEmailDTO userSendEmail)
        {
            var client = new SendGridClient(userSendEmail.ApiKey);
            var from = new EmailAddress("suport@example.com", "Soporte");
            var to = new EmailAddress(userSendEmail.Email, "Soporte contraseņa");

            var msg = MailHelper.CreateSingleTemplateEmail(from, to, userSendEmail.TemplateId, userSendEmail.DinamicTemplateData);
            var response = await client.SendEmailAsync(msg);

            return response;
        }
    }
}

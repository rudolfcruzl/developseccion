﻿using System.Collections.Specialized;
using Seccion.Jobs.DTO;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Threading.Tasks;

namespace Seccion.Jobs.Common
{
    public static class Email
    {
        public static async Task<object> SendEmail(UserSendEmailDTO userSendEmail)
        {
            var client = new SendGridClient(userSendEmail.ApiKey);
            var from = new EmailAddress(userSendEmail.From, "Soporte");
            var to = new EmailAddress(userSendEmail.Email, "Soporte contraseña");

            var msg = MailHelper.CreateSingleTemplateEmail(from, to, userSendEmail.TemplateId, userSendEmail.DinamicTemplateData);
            var response = await client.SendEmailAsync(msg);

            return response;
        }

        public static EmailConfigDTO GetEmailConfig(NameValueCollection appSettings)
        {
            var emailConfig = new EmailConfigDTO()
            {
                SendGridKey = appSettings["SendGridKey"],
                From = appSettings["Sengrid_From"],
                TemplateBlockUser = appSettings["TemplateBlockUser"],
                TemplateChangePassword = appSettings["TemplateChangePassword"],
                UrlChangePassword = appSettings["UrlChangePassword"]
            };

            return emailConfig;
        }
    }
}

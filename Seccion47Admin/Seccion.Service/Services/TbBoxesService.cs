﻿using Seccion.Data.Infrastructure;
using Seccion.Data.InterfazRepositories;
using Seccion.Data.Repositories;
using Seccion.Data.RepositoriesSP;
using Seccion.Data.ResposeDto;
using Seccion.Model.Models;
using Seccion.Service.ServiceInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Service.Services
{
    public class TbBoxesService : ITbBoxesService
    {
        private readonly ITbBoxesRepository _ITbBoxesRepository;
        private readonly IUnitOfWork _IUnitOfWork;
        private readonly ISP_GetInfoBoxRepository _ISP_GetInfoBoxRepository;

        public TbBoxesService(ITbBoxesRepository ITbBoxesRepository, IUnitOfWork IUnitOfWork, ISP_GetInfoBoxRepository ISP_GetInfoBoxRepository)
        {
            _ITbBoxesRepository = ITbBoxesRepository;
            _IUnitOfWork = IUnitOfWork;
            _ISP_GetInfoBoxRepository = ISP_GetInfoBoxRepository;
        }

        public void CreateModel(TbBoxes model) => _ITbBoxesRepository.Add(model);

        public void CreateModel(List<TbBoxes> lstModel) => _ITbBoxesRepository.Add(lstModel);

        public async Task<IEnumerable<SP_GetInfoBoxDto>> GetInfoBox(string sp, params object[] parameters) => await _ISP_GetInfoBoxRepository.ExecStoreProcedureAsync(sp, parameters);

        public TbBoxes GetTbBoxById(int id) => _ITbBoxesRepository.Get(x => x.TbBoxesId == id, null, x => x.TbEmployee).FirstOrDefault();

        public async Task Save() => await _IUnitOfWork.Commit();

        public void UpodateModel(TbBoxes model) => _ITbBoxesRepository.Update(model);


    }
}

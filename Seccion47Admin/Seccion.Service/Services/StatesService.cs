﻿using Seccion.Data.Infrastructure;
using Seccion.Data.Repositories;
using Seccion.Model.Models;
using Seccion.Service.ServiceInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Service.Services
{
    public class StatesService : IStatesService
    {
        private readonly IUnitOfWork _IUnitOfWork;
        private readonly IStatesRepository _IStatesRepository;

        public StatesService(IUnitOfWork IUnitOfWork, IStatesRepository IStatesRepository)
        {
            _IStatesRepository = IStatesRepository;
            _IUnitOfWork = IUnitOfWork;
        }

        public void CreateModel(TbStates model) =>  _IStatesRepository.Add(model);

        public async Task<IEnumerable<TbStates>> GetAll() => await _IStatesRepository.GetAll();

        public async Task Save() => await _IUnitOfWork.Commit();

        public void SaveModel(TbStates model) => _IStatesRepository.Update(model);
    }
}

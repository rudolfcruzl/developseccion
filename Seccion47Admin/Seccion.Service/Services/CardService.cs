﻿using Seccion.Data.Constants;
using Seccion.Data.Infrastructure;
using Seccion.Data.InterfazRepositories;
using Seccion.Data.ResposeDto;
using Seccion.Model.Enums;
using Seccion.Model.Models;
using Seccion.Service.ServiceInterface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Seccion.Service.Services
{
    public class CardService : ICardService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICardRepository _cardRepository;
        private readonly ICardStorageRepository _cardStorageRepository;
        private readonly IFileRepository _fileRepository;
        private readonly INotificationRepository _notificationRepository;

        public CardService(IUnitOfWork unitOfWork, ICardRepository cardRepository, ICardStorageRepository cardStorageRepository
            , IFileRepository fileRepository, INotificationRepository notificationRepository)
        {
            _unitOfWork = unitOfWork;
            _cardRepository = cardRepository;
            _cardStorageRepository = cardStorageRepository;
            _fileRepository = fileRepository;
            _notificationRepository = notificationRepository;
        }

        /// <summary>
        /// Obtiene todas las notificaciones asignadas a un usuario mediante un sp.
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns>IEnumerable<SP_GetNotificationDto></returns>
        public async Task<IEnumerable<SP_GetNotificationDto>> GetAllNotifications(params object[] parameters)
        {
            return await _notificationRepository.GetNotifications(parameters);
        }

        /// <summary>
        /// Crea una nueva tarea.
        /// </summary>
        /// <param name="card">TbCard</param>
        public void CreateCard(TbCard card)
        {
            _cardRepository.Add(card);
        }

        /// <summary>
        /// Elimina la tarea.
        /// </summary>
        /// <param name="card">TbCard</param>
        public void DeleteCard(TbCard card)
        {
            _cardRepository.Delete(card);
        }

        /// <summary>
        /// Crea una nueva notificacion.
        /// </summary>
        /// <param name="notification">TbNotification</param>
        public void CreateNotification(TbNotification notification)
        {
            _notificationRepository.Add(notification);
        }

        /// <summary>
        /// Importa los archivos al blob storage y guarda el registro en la base de datos.
        /// </summary>
        /// <param name="card">TbCard</param>
        /// <param name="Files">List<HttpPostedFileBase></param>
        /// <returns></returns>
        public async Task<bool> CreateFilesCard(TbCard card, List<HttpPostedFileBase> Files)
        {
            var cards = new List<TbCard>();
            cards.Add(card);

            foreach (var file in Files)
            {
                if (file == null)
                {
                    break;
                }

                var tbFile = new TbFile()
                {
                    FileName = string.Concat(card.TbCardId, "-", DateTime.Now.ToString("ddMMyyyyHHmmssfff"), Path.GetExtension(file.FileName)),
                    ContainerName = ContainerStorage.Card,
                    Cards = cards,
                    UserInsert = card.UserInsert,
                    DateInsert = DateTime.Now
                };

                MemoryStream target = new MemoryStream();
                file.InputStream.CopyTo(target);
                byte[] data = target.ToArray();

                var response = await _cardStorageRepository.UploadFileAsync(data, tbFile.FileName, card.Title, card.Description, file.ContentType);

                if (string.IsNullOrEmpty(response.Name))
                    return false;

                _fileRepository.Add(tbFile);
            }

            return true;
        }

        public async Task<IEnumerable<TbNotification>> GetNotificationWithStatusToDo(int userId)
        {
            var result = await _notificationRepository.GetMany(x => x.UserId == userId && x.Status == StatusNotification.ToDo);

            return result;
        }

        public async Task<IEnumerable<TbNotification>> GetNotificationsInCard(int cardId)
        {
            return await _notificationRepository.GetMany(x => x.Card.TbCardId == cardId);
        }

        public List<TbNotification> GetNewNotificationByUserName(string userName)
        {
            return _notificationRepository.Get(x => x.ApplicationUser.UserName == userName && x.Status == StatusNotification.ToDo, null, x => x.Card);
        }

        public void UpdateNotification(TbNotification notification)
        {
            _notificationRepository.Update(notification);
        }

        /// <summary>
        /// Obtiene las notificacion con su tarea asignada e informacion de los archivos importados
        /// </summary>
        /// <param name="notificationId">NotificationId</param>
        /// <returns>TbNotification</returns>
        public TbNotification GetNotificationWithCard(int notificationId)
        {
            return _notificationRepository.Get(x => x.TbNotificationId == notificationId, null, x => x.Card.Files).FirstOrDefault();
        }

        public async Task DownloadFile(MemoryStream memoryStream, string blobName)
        {
            await _cardStorageRepository.DownloadFileAsync(memoryStream, blobName);
        }

        public async Task Save()
        {
            await _unitOfWork.Commit();
        }
    }
}

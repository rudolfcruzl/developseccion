﻿using Seccion.Data.Infrastructure;
using Seccion.Data.Repositories;
using Seccion.Data.ResposeDto;
using Seccion.Model.Models;
using Seccion.Service.ServiceInterface;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Seccion.Service.Services
{

    public class EmployeeService : IEmployeeService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IStatesRepository statesRespository;
        private readonly IMunicipalitiesRepository municipalitiesRepository;
        private readonly IEmployeeRepository employeeRepository;


        public EmployeeService(IUnitOfWork unitOfWork, IStatesRepository statesRespository,
            IMunicipalitiesRepository municipalitiesRepository, IEmployeeRepository employeeRepository)
        {
            this.unitOfWork = unitOfWork;
            this.statesRespository = statesRespository;
            this.municipalitiesRepository = municipalitiesRepository;
            this.employeeRepository = employeeRepository;
        }

        public async Task<IEnumerable<TbStates>> GetStatesAll()
        {
            return await statesRespository.GetAll();
        }
        public async Task<IEnumerable<TbEmployee>> GetEmployeeAllForRequest()
        {
            return await employeeRepository.GetAll();
        }
        public async Task<TbEmployee> GetEmployeeById(int? id)
        {
            return await employeeRepository.Get(x => x.TbEmployeeId == id);
        }
        public async Task<IEnumerable<TbMunicipalities>> GetMunicipalitiesAll(int stateId)
        {
            return await municipalitiesRepository.GetMany(x => x.TbStatesId == stateId);
        }
        public void SaveEmployeeData(TbEmployee employee)
        {
            employeeRepository.Add(employee);
        }
        public void UpdateEmployeeData(TbEmployee employee)
        {
            employeeRepository.Update(employee);
        }
        public async Task Save()
        {
            await unitOfWork.Commit();
        }

        public async Task<IEnumerable<SP_EmployeeDto>> GetEmployeeAll(params object[] parameters)
        {
            return await employeeRepository.GetEmployeeAll(parameters);
        }

        public async Task<IEnumerable<EmployeeDto>> GetEmployeeData()
        {
            return await employeeRepository.GetEmployeeData();
        }

        public async Task<EmployeeDto> GetEmployeeByFicha(string numberFicha)
        {
            return await employeeRepository.GetEmployeeByFicha(numberFicha);
        }

        public void SaveEmployeeData(List<TbEmployee> lstTbEmployee)
        {
            employeeRepository.Add(lstTbEmployee);
        }
    }
}

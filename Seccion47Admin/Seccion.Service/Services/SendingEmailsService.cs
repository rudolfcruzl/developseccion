﻿using Seccion.Data.Enum;
using Seccion.Data.InterfazRepositories;
using Seccion.Service.ServiceInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Service.Services
{
	public class SendingEmailsService : ISendingEmailsService
	{
		private readonly ISendingEmails sendingEmails;

		public SendingEmailsService(ISendingEmails sendingEmails)
		{
			this.sendingEmails = sendingEmails;
		}

		public async Task ForgotPasswordEmailService(string ToEmail, string userName, string url, EmailType emailType)
		{
			await sendingEmails.ForgotPasswordEmail(ToEmail, userName, url, emailType);
		}
	}
}

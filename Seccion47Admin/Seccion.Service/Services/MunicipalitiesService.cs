﻿using Seccion.Data.Infrastructure;
using Seccion.Data.Repositories;
using Seccion.Model.Models;
using Seccion.Service.ServiceInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Service.Services
{
    public class MunicipalitiesService : IMunicipalitiesService
    {
        public readonly IUnitOfWork _IUnitOfWork;
        public readonly IMunicipalitiesRepository _IMunicipalitiesRepository;

        public MunicipalitiesService(IMunicipalitiesRepository IMunicipalitiesRepository, IUnitOfWork IUnitOfWork)
        {
            _IMunicipalitiesRepository = IMunicipalitiesRepository;
            _IUnitOfWork = IUnitOfWork;
        }
        public void CreateModel(TbMunicipalities model) => _IMunicipalitiesRepository.Add(model);

        public async Task<IEnumerable<TbMunicipalities>> GetAll() => await _IMunicipalitiesRepository.GetAll();

        public async Task Save() => await _IUnitOfWork.Commit();

        public void UpdateModel(TbMunicipalities model) => _IMunicipalitiesRepository.Update(model);
    }
}

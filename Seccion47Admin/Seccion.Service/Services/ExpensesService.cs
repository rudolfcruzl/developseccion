﻿using Seccion.Data.Infrastructure;
using Seccion.Data.InterfazRepositories;
using Seccion.Model.Models;
using Seccion.Service.ServiceInterface;
using System.Threading.Tasks;

namespace Seccion.Service.Services
{
	public class ExpensesService : IExpensesService
	{
		private readonly IUnitOfWork unitOfWork;
		private readonly IExpensesRepository expensesRepository;

		public ExpensesService(IUnitOfWork unitOfWork, IExpensesRepository expensesRepository)
		{
			this.unitOfWork = unitOfWork;
			this.expensesRepository = expensesRepository;
		}

		public void InsertExpenses(TbFuneralExpenses model)
		{
			expensesRepository.Add(model);
		}
		public async Task Save()
		{
			await unitOfWork.Commit();
		}
	}
}

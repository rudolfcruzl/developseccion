﻿using Seccion.Data.Repositories;
using Seccion.Model.IdentityModel;
using Seccion.Service.ServiceInterface;
using System;
using System.Collections.Generic;
using System.Xml;

namespace Seccion.Service.Services
{
    public class MenuService: IMenuService
    {
        private readonly IMenuRepository _IMenuRepository;

        public MenuService(IMenuRepository IMenuRepository)
        {
            _IMenuRepository = IMenuRepository;
        }

        public ICollection<MenuItem> GetMenuByUser(int UserId, Func<MenuPermission, bool> filterFunc = null)
        {
            return _IMenuRepository.GetMenuByUser(UserId);
        }

        public XmlDocument GetXmlDocument(string storeProcedure, params object[] parameters)
        {
            return _IMenuRepository.GetXmlDocument(storeProcedure, parameters);
        }
    }
}

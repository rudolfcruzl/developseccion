﻿using System.Text;
using System.Web.Mvc;

namespace Seccion.Service.Common.Message
{
    public enum MessageSeverity
    {
        None,
        Info,
        Success,
        Warning,
        Danger

    }
    public class Message
    {
        public MessageSeverity Severity { get; set; }

        public string Title { get; set; }
        public string TextMessage { get; set; }
        public string Link { get; set; } = string.Empty;

        public string Generate()
        {
            //var isDismissable = Severity != MessageSeverity.Danger;
            if (Severity == MessageSeverity.None)
                Severity = MessageSeverity.Info;

            var sb = new StringBuilder();

            string typeIconMessage = string.Empty;
            string typeColorIconMessage = string.Empty;
            string typeColorTitleMessage = string.Empty;
            switch (Severity)
            {
                case MessageSeverity.Info:
                    typeIconMessage = "fa-info-circle";
                    typeColorIconMessage = "text-blue";
                    typeColorTitleMessage = typeColorIconMessage;
                    break;
                case MessageSeverity.Success:
                    typeIconMessage = "fa-check-circle";
                    typeColorIconMessage = "text-success-m1";
                    typeColorTitleMessage = "text-success";
                    break;
                case MessageSeverity.Warning:
                    typeIconMessage = "fa-exclamation-triangle";
                    typeColorIconMessage = "text-warning-d2";
                    typeColorTitleMessage = "text-orange-d2";
                    break;
                case MessageSeverity.Danger:
                    typeIconMessage = "fa-times-circle";
                    typeColorIconMessage = "text-danger-m1";
                    typeColorTitleMessage = typeColorIconMessage;
                    break;
                default:
                    break;
            }
            var divTagSecond = new TagBuilder("div");
            divTagSecond.AddCssClass("flex-grow-1");

            var divTagIcon = new TagBuilder("i");
            divTagIcon.AddCssClass("fas "+ typeIconMessage + " mr-2 text-120 " + typeColorIconMessage);

            var tagStrongTitleMessage = new TagBuilder("strong");
            tagStrongTitleMessage.AddCssClass(typeColorTitleMessage + " mr-1");
            

            var tagSpanMessage = new TagBuilder("span");
            tagSpanMessage.AddCssClass("text-105 text-dark-tp3");
            

            sb.Append(divTagSecond.ToString(TagRenderMode.StartTag));
            sb.Append(divTagIcon.ToString(TagRenderMode.StartTag));
            sb.Append(divTagIcon.ToString(TagRenderMode.EndTag));
            sb.Append(tagStrongTitleMessage.ToString(TagRenderMode.StartTag));
            sb.Append(Title + "  ");
            sb.Append(tagStrongTitleMessage.ToString(TagRenderMode.EndTag));
            sb.Append(tagSpanMessage.ToString(TagRenderMode.StartTag));
            sb.Append(TextMessage);
            sb.Append(tagSpanMessage.ToString(TagRenderMode.EndTag));

            sb.Append(divTagSecond.ToString(TagRenderMode.EndTag));



            return sb.ToString();
        }
        public string GenerateList()
        {
            //var isDismissable = Severity != MessageSeverity.Danger;
            if (Severity == MessageSeverity.None)
                Severity = MessageSeverity.Info;

            var sb = new StringBuilder();



            //*****
            var liTag = new TagBuilder("li");
            var spanTitleTag = new TagBuilder("span");
            spanTitleTag.MergeAttribute("class", "font-weight-semibold");
            spanTitleTag.InnerHtml = Title;

            //var aTag = new TagBuilder("a");
            //aTag.MergeAttribute("class", "alert-link");
            //aTag.MergeAttribute("href", Link);


            //*****
            sb.Append(liTag.ToString(TagRenderMode.StartTag));
            sb.Append(spanTitleTag.ToString(TagRenderMode.StartTag));
            sb.Append(Title + "  ");
            sb.Append(spanTitleTag.ToString(TagRenderMode.EndTag));
            sb.Append(TextMessage);
            sb.Append(liTag.ToString(TagRenderMode.EndTag));



            return sb.ToString();
        }

    }
}
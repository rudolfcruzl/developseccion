﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace Seccion.Service.Common.Message
{
    public static class Constants
    {
        public const string TempDataKey = "Messages";
        public const string countMessage = "countMessage";
    }

    public class NotifierFilterAttribute : IActionFilter
    {
        
        private INotifier Notifier { get; set; }
        public NotifierFilterAttribute(INotifier Notifier)
        {
            this.Notifier = Notifier;
        }

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {

            var messageTempdata = (IList<Message>)filterContext.Controller.TempData[Constants.TempDataKey];

            if (messageTempdata != null)
            {
                Notifier.Messages = messageTempdata;
            }

            var messages = Notifier.Messages;

            if (messages.Count > 0)
            {
                filterContext.Controller.TempData[Constants.TempDataKey] = messages;
                filterContext.Controller.TempData[Constants.countMessage] = messages.Count;
            }
        }
        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // Method intentionally left empty.
        }
    }
    public static class NotifierExtensions
    {
        public static void Error(this INotifier notifier, string title, string text, params object[] format)
        {
            notifier.AddMessage(MessageSeverity.Danger, title, text, format);
        }

        public static void Info(this INotifier notifier, string title, string text, params object[] format)
        {
            notifier.AddMessage(MessageSeverity.Info, title, text, format);
        }

        public static void Success(this INotifier notifier, string title, string text, params object[] format)
        {
            notifier.AddMessage(MessageSeverity.Success, title, text, format);
        }

        public static void Warning(this INotifier notifier, string title, string text, params object[] format)
        {
            notifier.AddMessage(MessageSeverity.Warning, title, text, format);
        }

        public static MvcHtmlString DisplayMessages(this ViewContext context)
        {
            if (!context.Controller.TempData.ContainsKey(Constants.TempDataKey))
            {
                return null;
            }

            var messages = (IEnumerable<Message>)context.Controller.TempData[Constants.TempDataKey];
            int cout = (int)context.Controller.TempData[Constants.countMessage];
            string typeMessge = messages.Select(x => x.Severity).First().ToString().ToLower();
            var sb = new StringBuilder();
            var divTagPrincipal = new TagBuilder("div");
            divTagPrincipal.MergeAttribute("role", "alert");
            divTagPrincipal.AddCssClass("alert alert-lg " + "bgc-"+ typeMessge +"-l3" +  " border-0 border-l-4 brc-" + typeMessge +"-m1 mt-4 mb-3 pr-3 d-flex");



            var tagButton = new TagBuilder("button");
            tagButton.MergeAttribute("type", "button");
            tagButton.MergeAttribute("class", "close align-self-start");
            tagButton.MergeAttribute("data-dismiss", "alert");
            tagButton.MergeAttribute("aria-label", "Close");

            var spanTag = new TagBuilder("span");
            spanTag.MergeAttribute("aria-hidden", "true");

            var tagSpanIcon = new TagBuilder("i");
            tagSpanIcon.AddCssClass("fa fa-times text-80");

            sb.Append(divTagPrincipal.ToString(TagRenderMode.StartTag));




            if (cout > 1)
            {
                foreach (var message in messages)
                {
                    sb.AppendLine(message.GenerateList());
                }
            }
            else
            {
                foreach (var message in messages)
                {
                    sb.AppendLine(message.Generate());
                }
            }
            sb.Append(tagButton.ToString(TagRenderMode.StartTag));
            sb.Append(spanTag.ToString(TagRenderMode.StartTag));
            sb.Append(tagSpanIcon.ToString(TagRenderMode.StartTag));
            sb.Append(tagSpanIcon.ToString(TagRenderMode.EndTag));
            sb.Append(spanTag.ToString(TagRenderMode.EndTag));

            sb.Append(tagButton.ToString(TagRenderMode.EndTag));
            sb.Append(divTagPrincipal.ToString(TagRenderMode.EndTag));
            return sb.ToHtmlString();
        }

        private static MvcHtmlString ToHtmlString(this StringBuilder input)
        {
            return MvcHtmlString.Create(input.ToString());
        }
    }
}
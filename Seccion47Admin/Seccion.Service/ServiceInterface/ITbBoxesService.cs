﻿using Seccion.Data.ResposeDto;
using Seccion.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Service.ServiceInterface
{
    public interface ITbBoxesService
    {
        void CreateModel(TbBoxes model);
        void CreateModel(List<TbBoxes> lstModel);
        void UpodateModel(TbBoxes model);
        Task Save();
        TbBoxes GetTbBoxById(int id);

        Task<IEnumerable<SP_GetInfoBoxDto>> GetInfoBox(string sp, params object[] parameters);
    }
}

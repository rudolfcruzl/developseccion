﻿using Seccion.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Service.ServiceInterface
{
    public interface IStatesService
    {
        void CreateModel(TbStates model);
        void SaveModel(TbStates model);
        Task Save();
        Task<IEnumerable<TbStates>> GetAll();
    }
}

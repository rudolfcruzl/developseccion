﻿using Seccion.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Service.ServiceInterface
{
    public interface IMunicipalitiesService
    {
        void CreateModel(TbMunicipalities model);
        void UpdateModel(TbMunicipalities model);
        Task Save();
        Task<IEnumerable<TbMunicipalities>> GetAll();
    }
}

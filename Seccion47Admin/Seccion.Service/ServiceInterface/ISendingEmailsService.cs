﻿using Seccion.Data.Enum;
using System.Security.Policy;
using System.Threading.Tasks;

namespace Seccion.Service.ServiceInterface
{
	public interface ISendingEmailsService
	{
		Task ForgotPasswordEmailService(string ToEmail, string userName, string url, EmailType emailType);
	}
}

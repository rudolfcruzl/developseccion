﻿using System.Collections.Generic;
using System.IO;
using Seccion.Model.Models;
using System.Threading.Tasks;
using System.Web;
using Seccion.Data.ResposeDto;

namespace Seccion.Service.ServiceInterface
{
    public interface ICardService
    {
        Task<IEnumerable<SP_GetNotificationDto>> GetAllNotifications(params object[] parameters);
        void CreateCard(TbCard card);
        void DeleteCard(TbCard card);
        void CreateNotification(TbNotification notification);
        Task<bool> CreateFilesCard(TbCard card, List<HttpPostedFileBase> Files);
        Task<IEnumerable<TbNotification>> GetNotificationWithStatusToDo(int userId);
        Task<IEnumerable<TbNotification>> GetNotificationsInCard(int cardId);
        void UpdateNotification(TbNotification notification);
        TbNotification GetNotificationWithCard(int notificationId);
        List<TbNotification> GetNewNotificationByUserName(string userName);
        Task DownloadFile(MemoryStream memoryStream, string blobName);
        Task Save();
    }
}

﻿using Seccion.Model.IdentityModel;
using System;
using System.Collections.Generic;
using System.Xml;

namespace Seccion.Service.ServiceInterface
{
    public interface IMenuService
    {
        ICollection<MenuItem> GetMenuByUser(int UserId, Func<MenuPermission, bool> filterFunc = null);
        XmlDocument GetXmlDocument(string storeProcedure, params object[] parameters);
    }
}

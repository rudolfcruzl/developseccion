﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Model.Utilities
{
    public class CResponse
    {
        public bool Success { get; set; }
        public String Message { get; set; }
        public bool NotifyUser { get; set; }

    }
}

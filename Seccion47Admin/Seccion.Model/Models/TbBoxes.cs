﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Model.Models
{
    public class TbBoxes: BaseEntityModel
    {
        public int TbBoxesId { get; set; }
        public int TbEmployeeId { get; set; }
        public string Folio547 { get; set; }
        public string FolioNP { get; set; }
        public decimal Quantity { get; set; }
        public DateTime Date { get; set; }
        public int Period { get; set; }
        public decimal Rate { get; set; }
        public decimal GA { get; set; }
        public int Month { get; set; }
        public int Fourteen { get; set; }
        public int? AdditionalMonths { get; set; }
        public decimal TotalQuantity { get; set; }
        public decimal? Discount { get; set; }
        public decimal? NewDiscount { get; set; }
        public decimal? Balance { get; set; }
        public decimal CurrentBalance { get; set; }
        public int? FourteenRemaining { get; set; }
        public bool LiquidTwoCAT { get; set; }
        public string TypeLoan { get; set; }
        public decimal? CreditInsurance { get; set; }
        public int TypeInformation { get; set; }

        public virtual TbEmployee TbEmployee { get; set; }
    }
}

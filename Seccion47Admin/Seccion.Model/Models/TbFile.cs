﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Model.Models
{
    public class TbFile : BaseEntityModel
    {
        /// <summary>
        /// PK de la tabla TbFile
        /// </summary>
        public int TbFileId { get; set; }

        /// <summary>
        /// Nombre del archivo en blob storage
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Nombre del contenedor
        /// </summary>
        public string ContainerName { get; set; }

        public virtual ICollection<TbCard> Cards { get; set; }
    }
}

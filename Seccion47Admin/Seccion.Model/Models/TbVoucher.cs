﻿using System;

namespace Seccion.Model.Models
{
    public class TbVoucher : BaseEntityModel
    {
        /// <summary>
        /// ID de la tabla Voucherr
        /// </summary>
        public int TbVoucherId { get; set; }
        /// <summary>
        /// ID, FK de Gatos Funerales
        /// </summary>
        public int TbFuneralExpensesId { get; set; }
        /// <summary>
        /// Nombre del Comprobante Voucher
        /// </summary>
        public string NameVoucher { get; set; }
        /// <summary>
        /// Referencia al Modelo TbFuneralExpenses
        /// </summary>
        public virtual TbFuneralExpenses FuneralExpenses { get; set; }
    }
}

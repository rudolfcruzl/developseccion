﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Seccion.Model.Models
{
    public class TbStates: BaseEntityModel
    {
        //public TbStates()
        //{
            //this.Municipalities = new HashSet<TbMunicipalities>();
            //this.Employee = new HashSet<TbEmployee>();
        //}

        public int TbStatesId { get; set; }
        public string NameSate { get; set; }

        public virtual ICollection<TbMunicipalities> Municipalities { get; set; }
        public virtual ICollection<TbEmployee> Employee { get; set; }
    }
}

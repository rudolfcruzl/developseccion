﻿using System;
using System.Collections.Generic;

namespace Seccion.Model.Models
{
    public class TbFuneralExpenses : BaseEntityModel
    {

        public int TbFuneralExpensesId { get; set; }
        public int TbEmployeeId { get; set; }
        public string OrderNumberGF { get; set; }
        public DateTime DateCreate { get; set; }
        public string ServiceConcept { get; set; }
        public string PaymentType { get; set; }
        public string VoucherKey { get; set; }
        public string AvalName { get; set; }
        public decimal PaymentAmount { get; set; }
        public string PeriodsPayment { get; set; }
        public decimal AbonoCatorcenalMensual { get; set; }
        public string Comments { get; set; }
        public virtual TbEmployee Employee { get; set; }
        public virtual ICollection<TbVoucher> Vouchers { get; set; }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Model.Models
{
    public class TbCard : BaseEntityModel
    {
        /// <summary>
        /// PK de la tabla TbCard
        /// </summary>
        public int TbCardId { get; set; }

        /// <summary>
        /// Titulo de la tarjeta
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Descripcion de la tarjeta
        /// </summary>
        public string Description { get; set; }

        public virtual ICollection<TbNotification> Notifications { get; set; }

        public virtual ICollection<TbFile> Files { get; set; }
    }
}

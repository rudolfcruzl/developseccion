﻿using Seccion.Model.Enums;
using Seccion.Model.IdentityModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Model.Models
{
    /// <summary>
    /// Clase (tabla) de entidades para las notificaciones de tareas.
    /// </summary>
    public class TbNotification : BaseEntityModel
    {
        /// <summary>
        /// PK de la tabla
        /// </summary>
        public int TbNotificationId { get; set; }

        /// <summary>
        /// FK de la tabla AspNetUsers
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// FK de la tabla TbCard
        /// </summary>
        public int TbCardId { get; set; }

        /// <summary>
        /// Comentario de la tarea asignada
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Estatus de la notificacion
        /// </summary>
        public StatusNotification Status { get; set; }

        //[ForeignKey("UserId")]
        public virtual ApplicationUser ApplicationUser { get; set; }

        public virtual TbCard Card { get; set; }
    }
}

﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Model.IdentityModel
{
    public class ApplicationUserRole: IdentityUserRole<int>
    {
        public override int UserId { get; set; }
        public override int RoleId { get; set; }

        public ApplicationUser User { get; set; }
        public ApplicationRole Role { get; set; }
    }
}

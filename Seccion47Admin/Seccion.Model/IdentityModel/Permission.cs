﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Model.IdentityModel
{
    public class Permission : BaseEntityModel
    {
        public int PermissionId { get; set; }

        public string Name { get; set; }
    }
}
